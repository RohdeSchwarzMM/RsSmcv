Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS:USEFul:[RATE]:MAX
	single: [SOURce<HW>]:BB:DVBS:USEFul:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS:USEFul:[RATE]:MAX
	[SOURce<HW>]:BB:DVBS:USEFul:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: