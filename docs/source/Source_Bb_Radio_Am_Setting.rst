Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:AM:SETTing:CATalog
	single: [SOURce<HW>]:BB:RADio:AM:SETTing:DELete
	single: [SOURce<HW>]:BB:RADio:AM:SETTing:LOAD
	single: [SOURce<HW>]:BB:RADio:AM:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:RADio:AM:SETTing:CATalog
	[SOURce<HW>]:BB:RADio:AM:SETTing:DELete
	[SOURce<HW>]:BB:RADio:AM:SETTing:LOAD
	[SOURce<HW>]:BB:RADio:AM:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Am.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: