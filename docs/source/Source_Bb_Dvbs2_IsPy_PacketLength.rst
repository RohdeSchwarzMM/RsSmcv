PacketLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[IS<CH>]:PACKetlength

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[IS<CH>]:PACKetlength



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.IsPy.PacketLength.PacketLengthCls
	:members:
	:undoc-members:
	:noindex: