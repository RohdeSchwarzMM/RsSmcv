Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:CHANnel:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:CHANnel:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: