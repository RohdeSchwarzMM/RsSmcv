Address
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:IGMP:[SOURce]:ADDRess

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:IGMP:[SOURce]:ADDRess



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Igmp.Source.Address.AddressCls
	:members:
	:undoc-members:
	:noindex: