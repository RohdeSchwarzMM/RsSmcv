Lls
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:LLS

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:LLS



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Lls.LlsCls
	:members:
	:undoc-members:
	:noindex: