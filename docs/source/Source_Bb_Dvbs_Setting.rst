Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS:SETTing:CATalog
	single: [SOURce<HW>]:BB:DVBS:SETTing:DELete
	single: [SOURce<HW>]:BB:DVBS:SETTing:LOAD
	single: [SOURce<HW>]:BB:DVBS:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DVBS:SETTing:CATalog
	[SOURce<HW>]:BB:DVBS:SETTing:DELete
	[SOURce<HW>]:BB:DVBS:SETTing:LOAD
	[SOURce<HW>]:BB:DVBS:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: