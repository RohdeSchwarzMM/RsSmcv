Setting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBC:[SPECial]:SETTing:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:DVBC:[SPECial]:SETTing:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbc.Special.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: