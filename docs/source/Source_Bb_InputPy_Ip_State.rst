State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.State.StateCls
	:members:
	:undoc-members:
	:noindex: