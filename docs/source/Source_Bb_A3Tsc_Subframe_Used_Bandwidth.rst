Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:USED:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:USED:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Used.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: