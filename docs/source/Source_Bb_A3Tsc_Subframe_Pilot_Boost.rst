Boost
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:PILot:BOOSt

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:PILot:BOOSt



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Pilot.Boost.BoostCls
	:members:
	:undoc-members:
	:noindex: