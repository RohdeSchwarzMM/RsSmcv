OibPlp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:OIBPlp

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:OIBPlp



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.OibPlp.OibPlpCls
	:members:
	:undoc-members:
	:noindex: