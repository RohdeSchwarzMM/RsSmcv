Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DTMB:CHANnel:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:CHANnel:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: