Bbin
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TEST<HW>:BBIN:RBERror
	single: TEST:BBIN

.. code-block:: python

	TEST<HW>:BBIN:RBERror
	TEST:BBIN



.. autoclass:: RsSmcv.Implementations.Test.Bbin.BbinCls
	:members:
	:undoc-members:
	:noindex: