IsPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:SOURce:IS<CH>

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:SOURce:IS<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Source.IsPy.IsPyCls
	:members:
	:undoc-members:
	:noindex: