Inhibit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DAB:TRIGger:[EXTernal<CH>]:INHibit

.. code-block:: python

	[SOURce<HW>]:BB:DAB:TRIGger:[EXTernal<CH>]:INHibit



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dab.Trigger.External.Inhibit.InhibitCls
	:members:
	:undoc-members:
	:noindex: