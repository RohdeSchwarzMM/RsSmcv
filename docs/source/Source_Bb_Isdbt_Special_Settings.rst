Settings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:[SPECial]:SETTings:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:[SPECial]:SETTings:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Special.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex: