G13A
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G13A:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G13A:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G13A:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G13A:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G13A:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G13A:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G13A.G13ACls
	:members:
	:undoc-members:
	:noindex: