Symbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ATSM:SYMBols:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:SYMBols:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Symbols.SymbolsCls
	:members:
	:undoc-members:
	:noindex: