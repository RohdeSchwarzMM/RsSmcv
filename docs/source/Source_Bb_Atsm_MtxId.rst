MtxId
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ATSM:MTXid:MID
	single: [SOURce<HW>]:BB:ATSM:MTXid:TID

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:MTXid:MID
	[SOURce<HW>]:BB:ATSM:MTXid:TID



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.MtxId.MtxIdCls
	:members:
	:undoc-members:
	:noindex: