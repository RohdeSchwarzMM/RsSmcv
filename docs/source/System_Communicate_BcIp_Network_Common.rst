Common
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:BCIP:NETWork:COMMon:HOSTname

.. code-block:: python

	SYSTem:COMMunicate:BCIP:NETWork:COMMon:HOSTname



.. autoclass:: RsSmcv.Implementations.System.Communicate.BcIp.Network.Common.CommonCls
	:members:
	:undoc-members:
	:noindex: