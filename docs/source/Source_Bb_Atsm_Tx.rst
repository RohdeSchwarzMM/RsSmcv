Tx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ATSM:TX:ADDRess

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:TX:ADDRess



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Tx.TxCls
	:members:
	:undoc-members:
	:noindex: