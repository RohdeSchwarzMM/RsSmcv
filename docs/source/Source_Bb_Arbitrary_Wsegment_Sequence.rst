Sequence
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:WSEGment:SEQuence:SELect

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:WSEGment:SEQuence:SELect



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Wsegment.Sequence.SequenceCls
	:members:
	:undoc-members:
	:noindex: