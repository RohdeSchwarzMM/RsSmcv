Stl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INPut:STL:INTerface

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INPut:STL:INTerface



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.InputPy.Stl.StlCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.inputPy.stl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_InputPy_Stl_ResetLog.rst