Keyboard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:KEYBoard:[STATe]

.. code-block:: python

	TEST:KEYBoard:[STATe]



.. autoclass:: RsSmcv.Implementations.Test.Keyboard.KeyboardCls
	:members:
	:undoc-members:
	:noindex: