Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:DELay:MAXimum

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:DELay:MAXimum



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.Output.Delay.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: