Darc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:DARC:DEViation
	single: [SOURce<HW>]:BB:RADio:FM:DARC:INFormation
	single: [SOURce<HW>]:BB:RADio:FM:DARC:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:DARC:DEViation
	[SOURce<HW>]:BB:RADio:FM:DARC:INFormation
	[SOURce<HW>]:BB:RADio:FM:DARC:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Darc.DarcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.darc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Darc_Bic.rst