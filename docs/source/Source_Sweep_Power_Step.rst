Step
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:SWEep:POWer:STEP:[LOGarithmic]

.. code-block:: python

	[SOURce<HW>]:SWEep:POWer:STEP:[LOGarithmic]



.. autoclass:: RsSmcv.Implementations.Source.Sweep.Power.Step.StepCls
	:members:
	:undoc-members:
	:noindex: