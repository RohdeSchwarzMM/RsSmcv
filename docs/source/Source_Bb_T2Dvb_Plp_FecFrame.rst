FecFrame
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:FECFrame

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:FECFrame



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.FecFrame.FecFrameCls
	:members:
	:undoc-members:
	:noindex: