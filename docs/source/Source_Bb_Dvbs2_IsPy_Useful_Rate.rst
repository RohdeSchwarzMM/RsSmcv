Rate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[IS<CH>]:USEFul:[RaTE]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[IS<CH>]:USEFul:[RaTE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.IsPy.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.isPy.useful.rate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_IsPy_Useful_Rate_Max.rst