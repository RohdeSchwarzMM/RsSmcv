Desc<AlternaiveFreqList>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.source.bb.radio.fm.rds.af.b.list4.desc.repcap_alternaiveFreqList_get()
	driver.source.bb.radio.fm.rds.af.b.list4.desc.repcap_alternaiveFreqList_set(repcap.AlternaiveFreqList.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST4:DESC<CH>

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST4:DESC<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.B.List4.Desc.DescCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.b.list4.desc.clone()