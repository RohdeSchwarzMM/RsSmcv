Source
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:SOURce:A
	single: [SOURce<HW>]:BB:ISDBt:SOURce:B
	single: [SOURce<HW>]:BB:ISDBt:SOURce:C

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:SOURce:A
	[SOURce<HW>]:BB:ISDBt:SOURce:B
	[SOURce<HW>]:BB:ISDBt:SOURce:C



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: