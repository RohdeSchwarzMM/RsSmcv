Immediate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger<HW>:PSWeep:[IMMediate]

.. code-block:: python

	TRIGger<HW>:PSWeep:[IMMediate]



.. autoclass:: RsSmcv.Implementations.Trigger.Psweep.Immediate.ImmediateCls
	:members:
	:undoc-members:
	:noindex: