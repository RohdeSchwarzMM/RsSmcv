Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:ID

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:ID



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Id.IdCls
	:members:
	:undoc-members:
	:noindex: