G9B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G9B:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G9B:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G9B:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G9B:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G9B:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G9B:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G9B.G9BCls
	:members:
	:undoc-members:
	:noindex: