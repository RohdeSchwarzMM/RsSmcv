Internal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:PM:INTernal:SOURce

.. code-block:: python

	[SOURce<HW>]:PM:INTernal:SOURce



.. autoclass:: RsSmcv.Implementations.Source.Pm.Internal.InternalCls
	:members:
	:undoc-members:
	:noindex: