Fft
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:FFT:MODE

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:FFT:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Fft.FftCls
	:members:
	:undoc-members:
	:noindex: