Source
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Igmp.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.inputPy.ip.igmp.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_InputPy_Ip_Igmp_Source_Address.rst
	Source_Bb_InputPy_Ip_Igmp_Source_Ping.rst
	Source_Bb_InputPy_Ip_Igmp_Source_Result.rst