Signal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:INPut:USER<CH>:SIGNal

.. code-block:: python

	[SOURce]:INPut:USER<CH>:SIGNal



.. autoclass:: RsSmcv.Implementations.Source.InputPy.User.Signal.SignalCls
	:members:
	:undoc-members:
	:noindex: