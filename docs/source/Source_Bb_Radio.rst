Radio
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.RadioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Am.rst
	Source_Bb_Radio_Fm.rst