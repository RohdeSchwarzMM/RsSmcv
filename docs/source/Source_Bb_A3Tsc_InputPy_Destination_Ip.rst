Ip
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INPut:DESTination:IP:ADDRess
	single: [SOURce<HW>]:BB:A3TSc:INPut:DESTination:IP:PORT

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INPut:DESTination:IP:ADDRess
	[SOURce<HW>]:BB:A3TSc:INPut:DESTination:IP:PORT



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.InputPy.Destination.Ip.IpCls
	:members:
	:undoc-members:
	:noindex: