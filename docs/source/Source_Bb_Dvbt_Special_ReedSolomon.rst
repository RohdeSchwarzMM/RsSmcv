ReedSolomon
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:[SPECial]:REEDsolomon:LOW
	single: [SOURce<HW>]:BB:DVBT:[SPECial]:REEDsolomon:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:[SPECial]:REEDsolomon:LOW
	[SOURce<HW>]:BB:DVBT:[SPECial]:REEDsolomon:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Special.ReedSolomon.ReedSolomonCls
	:members:
	:undoc-members:
	:noindex: