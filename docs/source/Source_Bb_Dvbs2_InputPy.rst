InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[INPut]:CMMode
	single: [SOURce<HW>]:BB:DVBS2:[INPut]:NIS

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[INPut]:CMMode
	[SOURce<HW>]:BB:DVBS2:[INPut]:NIS



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_InputPy_IsPy.rst