Bb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:CFACtor
	single: [SOURce]:BB:CONFiguration
	single: [SOURce<HW>]:BB:FOFFset
	single: [SOURce<HW>]:BB:IQGain
	single: [SOURce<HW>]:BB:PGAin
	single: [SOURce<HW>]:BB:POFFset
	single: [SOURce<HW>]:BB:ROUTe

.. code-block:: python

	[SOURce<HW>]:BB:CFACtor
	[SOURce]:BB:CONFiguration
	[SOURce<HW>]:BB:FOFFset
	[SOURce<HW>]:BB:IQGain
	[SOURce<HW>]:BB:PGAin
	[SOURce<HW>]:BB:POFFset
	[SOURce<HW>]:BB:ROUTe



.. autoclass:: RsSmcv.Implementations.Source.Bb.BbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc.rst
	Source_Bb_Arbitrary.rst
	Source_Bb_Atsm.rst
	Source_Bb_Coder.rst
	Source_Bb_Dab.rst
	Source_Bb_Drm.rst
	Source_Bb_Dtmb.rst
	Source_Bb_Dvbc.rst
	Source_Bb_Dvbs.rst
	Source_Bb_Dvbs2.rst
	Source_Bb_Dvbt.rst
	Source_Bb_General.rst
	Source_Bb_Graphics.rst
	Source_Bb_Impairment.rst
	Source_Bb_Info.rst
	Source_Bb_InputPy.rst
	Source_Bb_Isdbt.rst
	Source_Bb_J83B.rst
	Source_Bb_Lora.rst
	Source_Bb_Path.rst
	Source_Bb_Power.rst
	Source_Bb_Progress.rst
	Source_Bb_Radio.rst
	Source_Bb_T2Dvb.rst
	Source_Bb_Tdmb.rst
	Source_Bb_Trigger.rst