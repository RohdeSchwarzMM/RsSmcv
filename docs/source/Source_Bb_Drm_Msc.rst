Msc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DRM:MSC:CONStel

.. code-block:: python

	[SOURce<HW>]:BB:DRM:MSC:CONStel



.. autoclass:: RsSmcv.Implementations.Source.Bb.Drm.Msc.MscCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.drm.msc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Drm_Msc_Level.rst
	Source_Bb_Drm_Msc_Profile.rst
	Source_Bb_Drm_Msc_Rate.rst