Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:CHANnel:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:CHANnel:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: