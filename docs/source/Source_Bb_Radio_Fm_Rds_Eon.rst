Eon
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:EG
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:ILS
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:LA
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:LSN
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:PI
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:PIN
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:PS
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:PTY
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:TP
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:Ta

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:EON:EG
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:ILS
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:LA
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:LSN
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:PI
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:PIN
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:PS
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:PTY
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:TP
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:Ta



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Eon.EonCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.eon.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Eon_Af.rst