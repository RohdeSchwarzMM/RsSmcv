SuPolicy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:SUPolicy

.. code-block:: python

	SYSTem:SECurity:SUPolicy



.. autoclass:: RsSmcv.Implementations.System.Security.SuPolicy.SuPolicyCls
	:members:
	:undoc-members:
	:noindex: