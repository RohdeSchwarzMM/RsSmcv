Dvbs2
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce]:[IQCoder]:DVBS2:CONStel
	single: [SOURce]:[IQCoder]:DVBS2:FECFrame
	single: [SOURce]:[IQCoder]:DVBS2:INPut
	single: [SOURce]:[IQCoder]:DVBS2:PILots
	single: [SOURce]:[IQCoder]:DVBS2:RATE

.. code-block:: python

	[SOURce]:[IQCoder]:DVBS2:CONStel
	[SOURce]:[IQCoder]:DVBS2:FECFrame
	[SOURce]:[IQCoder]:DVBS2:INPut
	[SOURce]:[IQCoder]:DVBS2:PILots
	[SOURce]:[IQCoder]:DVBS2:RATE



.. autoclass:: RsSmcv.Implementations.Source.Iqcoder.Dvbs2.Dvbs2Cls
	:members:
	:undoc-members:
	:noindex: