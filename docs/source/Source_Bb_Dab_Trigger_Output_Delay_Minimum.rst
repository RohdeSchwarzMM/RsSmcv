Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DAB:TRIGger:OUTPut<CH>:DELay:MINimum

.. code-block:: python

	[SOURce<HW>]:BB:DAB:TRIGger:OUTPut<CH>:DELay:MINimum



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dab.Trigger.Output.Delay.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: