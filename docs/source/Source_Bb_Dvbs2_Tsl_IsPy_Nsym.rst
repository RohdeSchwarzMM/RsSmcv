Nsym
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:NSYM

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:NSYM



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.IsPy.Nsym.NsymCls
	:members:
	:undoc-members:
	:noindex: