Opf
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.OpfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.opf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Opf_Apply.rst
	Source_Bb_Radio_Fm_Rds_Opf_G10B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G11A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G11B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G12A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G12B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G13A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G13B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G15A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G1A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G1B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G3A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G3B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G4B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G5A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G5B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G6A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G6B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G7A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G7B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G8A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G8B.rst
	Source_Bb_Radio_Fm_Rds_Opf_G9A.rst
	Source_Bb_Radio_Fm_Rds_Opf_G9B.rst