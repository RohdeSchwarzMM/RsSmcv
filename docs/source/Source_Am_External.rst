External
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AM:EXTernal:COUPling

.. code-block:: python

	[SOURce<HW>]:AM:EXTernal:COUPling



.. autoclass:: RsSmcv.Implementations.Source.Am.External.ExternalCls
	:members:
	:undoc-members:
	:noindex: