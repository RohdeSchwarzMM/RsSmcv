Npreamble
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:L:NPReamble:[SYMBols]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:L:NPReamble:[SYMBols]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Lpy.Npreamble.NpreambleCls
	:members:
	:undoc-members:
	:noindex: