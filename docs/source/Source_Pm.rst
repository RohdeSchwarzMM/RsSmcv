Pm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:PM:SENSitivity
	single: [SOURce<HW>]:PM:[DEViation]

.. code-block:: python

	[SOURce<HW>]:PM:SENSitivity
	[SOURce<HW>]:PM:[DEViation]



.. autoclass:: RsSmcv.Implementations.Source.Pm.PmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.pm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Pm_External.rst
	Source_Pm_Internal.rst