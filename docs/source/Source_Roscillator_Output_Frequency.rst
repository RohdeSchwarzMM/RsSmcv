Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce]:ROSCillator:OUTPut:FREQuency:MODE
	single: [SOURce<HW>]:ROSCillator:OUTPut:FREQuency

.. code-block:: python

	[SOURce]:ROSCillator:OUTPut:FREQuency:MODE
	[SOURce<HW>]:ROSCillator:OUTPut:FREQuency



.. autoclass:: RsSmcv.Implementations.Source.Roscillator.Output.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: