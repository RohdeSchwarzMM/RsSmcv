Iip
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:IIP:PID

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:IIP:PID



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Iip.IipCls
	:members:
	:undoc-members:
	:noindex: