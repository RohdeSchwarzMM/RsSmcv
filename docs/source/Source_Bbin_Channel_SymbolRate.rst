SymbolRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BBIN:CHANnel<CH0>:SRATe

.. code-block:: python

	[SOURce<HW>]:BBIN:CHANnel<CH0>:SRATe



.. autoclass:: RsSmcv.Implementations.Source.Bbin.Channel.SymbolRate.SymbolRateCls
	:members:
	:undoc-members:
	:noindex: