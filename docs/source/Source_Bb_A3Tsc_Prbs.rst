Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: