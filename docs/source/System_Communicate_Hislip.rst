Hislip
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:HISLip:RESource

.. code-block:: python

	SYSTem:COMMunicate:HISLip:RESource



.. autoclass:: RsSmcv.Implementations.System.Communicate.Hislip.HislipCls
	:members:
	:undoc-members:
	:noindex: