Settings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:TDMB:[SPECial]:SETTings:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:[SPECial]:SETTings:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Special.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex: