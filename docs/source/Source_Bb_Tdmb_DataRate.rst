DataRate<SubChannel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.source.bb.tdmb.dataRate.repcap_subChannel_get()
	driver.source.bb.tdmb.dataRate.repcap_subChannel_set(repcap.SubChannel.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:TDMB:DATarate<CH>

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:DATarate<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.DataRate.DataRateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.tdmb.dataRate.clone()