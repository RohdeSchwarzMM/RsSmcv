Qcomponent
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:BB:IMPairment:IQOutput<CH>:LEAKage:Q

.. code-block:: python

	[SOURce]:BB:IMPairment:IQOutput<CH>:LEAKage:Q



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.IqOutput.Leakage.Qcomponent.QcomponentCls
	:members:
	:undoc-members:
	:noindex: