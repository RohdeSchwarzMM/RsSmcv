Transition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:GENeral:PULM:TRANsition:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:PULM:TRANsition:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Pulm.Transition.TransitionCls
	:members:
	:undoc-members:
	:noindex: