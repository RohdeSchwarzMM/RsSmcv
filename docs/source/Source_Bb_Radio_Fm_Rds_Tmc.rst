Tmc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:READy
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:READy
	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Tmc.TmcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.tmc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Tmc_Apply.rst
	Source_Bb_Radio_Fm_Rds_Tmc_G3A.rst
	Source_Bb_Radio_Fm_Rds_Tmc_G8A.rst