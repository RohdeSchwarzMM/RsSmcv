PciExpress
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:PCIexpress:RESource

.. code-block:: python

	SYSTem:COMMunicate:PCIexpress:RESource



.. autoclass:: RsSmcv.Implementations.System.Communicate.PciExpress.PciExpressCls
	:members:
	:undoc-members:
	:noindex: