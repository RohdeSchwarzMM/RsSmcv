State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:CORRection:SPDevice:STATe

.. code-block:: python

	SENSe<CH>:[POWer]:CORRection:SPDevice:STATe



.. autoclass:: RsSmcv.Implementations.Sense.Power.Correction.SpDevice.State.StateCls
	:members:
	:undoc-members:
	:noindex: