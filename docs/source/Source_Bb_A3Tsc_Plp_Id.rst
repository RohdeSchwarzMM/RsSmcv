Id
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:ID

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:ID



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Id.IdCls
	:members:
	:undoc-members:
	:noindex: