Max
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:USEFul:[RATE]:MAX

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:USEFul:[RATE]:MAX



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Useful.Rate.Max.MaxCls
	:members:
	:undoc-members:
	:noindex: