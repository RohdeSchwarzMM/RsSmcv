Ape2
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:EEW:APE2

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:EEW:APE2



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Eew.Ape2.Ape2Cls
	:members:
	:undoc-members:
	:noindex: