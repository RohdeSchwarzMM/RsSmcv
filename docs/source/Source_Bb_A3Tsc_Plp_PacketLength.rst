PacketLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:PACKetlength

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:PACKetlength



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.PacketLength.PacketLengthCls
	:members:
	:undoc-members:
	:noindex: