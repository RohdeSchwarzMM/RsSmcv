Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TSGen:CONFigure:PRBS:[SEQuence]

.. code-block:: python

	TSGen:CONFigure:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.TsGen.Configure.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: