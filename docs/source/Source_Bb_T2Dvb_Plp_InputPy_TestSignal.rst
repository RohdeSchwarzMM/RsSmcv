TestSignal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:INPut:TESTsignal

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:INPut:TESTsignal



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.InputPy.TestSignal.TestSignalCls
	:members:
	:undoc-members:
	:noindex: