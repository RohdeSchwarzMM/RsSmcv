Specification
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:SPECification:PARameter
	single: SYSTem:SPECification

.. code-block:: python

	SYSTem:SPECification:PARameter
	SYSTem:SPECification



.. autoclass:: RsSmcv.Implementations.System.Specification.SpecificationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.specification.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Specification_Identification.rst
	System_Specification_Version.rst