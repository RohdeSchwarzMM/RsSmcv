Frame
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:FRAMe:DURation

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:FRAMe:DURation



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Frame.FrameCls
	:members:
	:undoc-members:
	:noindex: