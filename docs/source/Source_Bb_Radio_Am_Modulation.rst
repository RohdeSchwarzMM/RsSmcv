Modulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:AM:MODulation:DEPTh

.. code-block:: python

	[SOURce<HW>]:BB:RADio:AM:MODulation:DEPTh



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Am.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex: