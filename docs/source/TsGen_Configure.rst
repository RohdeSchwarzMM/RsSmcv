Configure
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TSGen:CONFigure:COMMand
	single: TSGen:CONFigure:PAYLoad
	single: TSGen:CONFigure:PID
	single: TSGen:CONFigure:PIDTestpack
	single: TSGen:CONFigure:PLAYfile
	single: TSGen:CONFigure:PLENgth
	single: TSGen:CONFigure:STOPdata
	single: TSGen:CONFigure:STUFfing
	single: TSGen:CONFigure:TSPacket
	single: TSGen:CONFigure:TSRate

.. code-block:: python

	TSGen:CONFigure:COMMand
	TSGen:CONFigure:PAYLoad
	TSGen:CONFigure:PID
	TSGen:CONFigure:PIDTestpack
	TSGen:CONFigure:PLAYfile
	TSGen:CONFigure:PLENgth
	TSGen:CONFigure:STOPdata
	TSGen:CONFigure:STUFfing
	TSGen:CONFigure:TSPacket
	TSGen:CONFigure:TSRate



.. autoclass:: RsSmcv.Implementations.TsGen.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tsGen.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	TsGen_Configure_Prbs.rst
	TsGen_Configure_Seamless.rst
	TsGen_Configure_Seek.rst