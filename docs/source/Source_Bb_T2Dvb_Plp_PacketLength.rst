PacketLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:PACKetlength

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:PACKetlength



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.PacketLength.PacketLengthCls
	:members:
	:undoc-members:
	:noindex: