RepCaps
=========

HwInstance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_hwInstance_set(repcap.HwInstance.InstA)
	# Range:
	InstA .. InstH
	# All values (8x):
	InstA | InstB | InstC | InstD | InstE | InstF | InstG | InstH

AlternaiveFreqList
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.AlternaiveFreqList.Nr1
	# Range:
	Nr1 .. Nr12
	# All values (12x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12

BitNumberNull
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BitNumberNull.Nr0
	# Range:
	Nr0 .. Nr15
	# All values (16x):
	Nr0 | Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7
	Nr8 | Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15

BlockIdCode
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.BlockIdCode.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

Carrier
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Carrier.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

Channel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Channel.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

ChannelNull
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ChannelNull.Nr0
	# Range:
	Nr0 .. Nr63
	# All values (64x):
	Nr0 | Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7
	Nr8 | Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15
	Nr16 | Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23
	Nr24 | Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31
	Nr32 | Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39
	Nr40 | Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47
	Nr48 | Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55
	Nr56 | Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63

ErrorCount
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ErrorCount.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

External
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.External.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

GroupTypeVariant
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.GroupTypeVariant.Nr1
	# Values (2x):
	Nr1 | Nr2

Index
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Index.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

InputIx
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.InputIx.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

InputStream
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.InputStream.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

IpVersion
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IpVersion.Nr4
	# Values (2x):
	Nr4 | Nr6

IqConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IqConnector.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

Level
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Level.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

Output
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Output.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

Path
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Path.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

PhysicalLayerPipe
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.PhysicalLayerPipe.Nr1
	# Range:
	Nr1 .. Nr64
	# All values (64x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64

Profile
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Profile.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

Stream
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Stream.Nr1
	# Range:
	Nr1 .. Nr16
	# All values (16x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16

SubChannel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SubChannel.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

Subframe
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Subframe.Nr1
	# Range:
	Nr1 .. Nr32
	# All values (32x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32

TimeSlice
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.TimeSlice.Nr1
	# Range:
	Nr1 .. Nr8
	# All values (8x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8

UserIx
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UserIx.Nr1
	# Range:
	Nr1 .. Nr48
	# All values (48x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48

