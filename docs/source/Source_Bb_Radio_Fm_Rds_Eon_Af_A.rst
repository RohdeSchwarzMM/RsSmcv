A
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:AF:A:NUMBer

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:EON:AF:A:NUMBer



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Eon.Af.A.ACls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.eon.af.a.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Eon_Af_A_Frequency.rst