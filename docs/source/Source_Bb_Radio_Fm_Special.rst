Special
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Special_Pilot.rst
	Source_Bb_Radio_Fm_Special_Rds.rst
	Source_Bb_Radio_Fm_Special_Settings.rst