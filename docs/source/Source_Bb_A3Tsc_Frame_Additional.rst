Additional
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:ADDitional:[SAMPles]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:FRAMe:ADDitional:[SAMPles]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Frame.Additional.AdditionalCls
	:members:
	:undoc-members:
	:noindex: