Af
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:METHod

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:AF:METHod



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.AfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Af_A.rst
	Source_Bb_Radio_Fm_Rds_Af_B.rst