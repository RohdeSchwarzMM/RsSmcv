Pilot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:PILot:DX

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:PILot:DX



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Pilot.PilotCls
	:members:
	:undoc-members:
	:noindex: