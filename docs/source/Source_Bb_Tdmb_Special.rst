Special
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.tdmb.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Tdmb_Special_Settings.rst
	Source_Bb_Tdmb_Special_TestSignal.rst
	Source_Bb_Tdmb_Special_Transmission.rst