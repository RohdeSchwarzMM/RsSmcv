T2Mi
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:ANALyzer
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:INTerface
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MEASuremode
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:PID
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:SID

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:ANALyzer
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:INTerface
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MEASuremode
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:PID
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:SID



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.InputPy.T2Mi.T2MiCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.inputPy.t2Mi.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_InputPy_T2Mi_Max.rst
	Source_Bb_T2Dvb_InputPy_T2Mi_Min.rst
	Source_Bb_T2Dvb_InputPy_T2Mi_ResetLog.rst