Pilot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:[SPECial]:PILot:PHASe
	single: [SOURce<HW>]:BB:RADio:FM:[SPECial]:PILot:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:[SPECial]:PILot:PHASe
	[SOURce<HW>]:BB:RADio:FM:[SPECial]:PILot:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Special.Pilot.PilotCls
	:members:
	:undoc-members:
	:noindex: