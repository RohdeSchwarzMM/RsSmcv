Fint
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:TIL:FINT

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:TIL:FINT



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Til.Fint.FintCls
	:members:
	:undoc-members:
	:noindex: