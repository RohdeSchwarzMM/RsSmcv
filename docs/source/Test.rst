Test
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TEST:EIQMode
	single: TEST:LEVel
	single: TEST:NRPTrigger
	single: TEST:PRESet

.. code-block:: python

	TEST:EIQMode
	TEST:LEVel
	TEST:NRPTrigger
	TEST:PRESet



.. autoclass:: RsSmcv.Implementations.Test.TestCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Test_All.rst
	Test_Bb.rst
	Test_Bbin.rst
	Test_BbOut.rst
	Test_Connector.rst
	Test_Hs.rst
	Test_Keyboard.rst
	Test_Pixel.rst
	Test_Remote.rst
	Test_Res.rst
	Test_Serror.rst
	Test_Sw.rst
	Test_Write.rst