Rate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:RATE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:RATE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: