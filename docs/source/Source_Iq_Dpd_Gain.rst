Gain
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:IQ:DPD:GAIN:PRE

.. code-block:: python

	[SOURce<HW>]:IQ:DPD:GAIN:PRE



.. autoclass:: RsSmcv.Implementations.Source.Iq.Dpd.Gain.GainCls
	:members:
	:undoc-members:
	:noindex: