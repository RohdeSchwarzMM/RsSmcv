Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:[SPECial]:TRANsmission:MODE:SELECT
	single: [SOURce<HW>]:BB:TDMB:[SPECial]:TRANsmission:MODE

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:[SPECial]:TRANsmission:MODE:SELECT
	[SOURce<HW>]:BB:TDMB:[SPECial]:TRANsmission:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Special.Transmission.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: