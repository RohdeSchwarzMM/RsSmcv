Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:RATE:LOW
	single: [SOURce<HW>]:BB:DVBT:RATE:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:RATE:LOW
	[SOURce<HW>]:BB:DVBT:RATE:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: