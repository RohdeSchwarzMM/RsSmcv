Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DTMB:USEFul:[RATE]:MAX
	single: [SOURce<HW>]:BB:DTMB:USEFul:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:USEFul:[RATE]:MAX
	[SOURce<HW>]:BB:DTMB:USEFul:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: