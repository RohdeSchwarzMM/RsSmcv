Id
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:ID:CELL
	single: [SOURce<HW>]:BB:T2DVb:ID:NETWork
	single: [SOURce<HW>]:BB:T2DVb:ID:T2SYstem

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:ID:CELL
	[SOURce<HW>]:BB:T2DVb:ID:NETWork
	[SOURce<HW>]:BB:T2DVb:ID:T2SYstem



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Id.IdCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.id.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Id_Txid.rst