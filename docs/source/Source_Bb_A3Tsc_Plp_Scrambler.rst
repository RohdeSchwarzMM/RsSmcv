Scrambler
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:SCRambler

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:SCRambler



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Scrambler.ScramblerCls
	:members:
	:undoc-members:
	:noindex: