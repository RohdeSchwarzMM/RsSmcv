AudGen
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:AUDGen:FRQ1
	single: [SOURce<HW>]:BB:RADio:FM:AUDGen:FRQ2
	single: [SOURce<HW>]:BB:RADio:FM:AUDGen:LEV1
	single: [SOURce<HW>]:BB:RADio:FM:AUDGen:LEV2

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:AUDGen:FRQ1
	[SOURce<HW>]:BB:RADio:FM:AUDGen:FRQ2
	[SOURce<HW>]:BB:RADio:FM:AUDGen:LEV1
	[SOURce<HW>]:BB:RADio:FM:AUDGen:LEV2



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.AudGen.AudGenCls
	:members:
	:undoc-members:
	:noindex: