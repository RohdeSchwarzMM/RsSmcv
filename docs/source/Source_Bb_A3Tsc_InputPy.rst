InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INPut:CCHeck
	single: [SOURce<HW>]:BB:A3TSc:INPut:NPLP
	single: [SOURce<HW>]:BB:A3TSc:INPut:PROTocol
	single: [SOURce<HW>]:BB:A3TSc:INPut:STATus
	single: [SOURce<HW>]:BB:A3TSc:INPut:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INPut:CCHeck
	[SOURce<HW>]:BB:A3TSc:INPut:NPLP
	[SOURce<HW>]:BB:A3TSc:INPut:PROTocol
	[SOURce<HW>]:BB:A3TSc:INPut:STATus
	[SOURce<HW>]:BB:A3TSc:INPut:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_InputPy_Destination.rst
	Source_Bb_A3Tsc_InputPy_Stl.rst