DataRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[INPut]:[IS<CH>]:DATarate

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[INPut]:[IS<CH>]:DATarate



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.InputPy.IsPy.DataRate.DataRateCls
	:members:
	:undoc-members:
	:noindex: