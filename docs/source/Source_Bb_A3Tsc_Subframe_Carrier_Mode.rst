Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:CARRier:MODE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:CARRier:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Carrier.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: