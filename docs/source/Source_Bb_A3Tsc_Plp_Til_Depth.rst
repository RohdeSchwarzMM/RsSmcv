Depth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:DEPTh

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:DEPTh



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Til.Depth.DepthCls
	:members:
	:undoc-members:
	:noindex: