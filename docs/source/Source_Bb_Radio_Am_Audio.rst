Audio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:AM:AUDio:AF

.. code-block:: python

	[SOURce<HW>]:BB:RADio:AM:AUDio:AF



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Am.Audio.AudioCls
	:members:
	:undoc-members:
	:noindex: