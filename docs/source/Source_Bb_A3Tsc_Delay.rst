Delay
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:DELay:DEViation
	single: [SOURce<HW>]:BB:A3TSc:DELay:DISPatch
	single: [SOURce<HW>]:BB:A3TSc:DELay:DYNamic
	single: [SOURce<HW>]:BB:A3TSc:DELay:MAXImum
	single: [SOURce<HW>]:BB:A3TSc:DELay:NETWork
	single: [SOURce<HW>]:BB:A3TSc:DELay:PROCess
	single: [SOURce<HW>]:BB:A3TSc:DELay:SFNMode
	single: [SOURce<HW>]:BB:A3TSc:DELay:STATic
	single: [SOURce<HW>]:BB:A3TSc:DELay:TOTal

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:DELay:DEViation
	[SOURce<HW>]:BB:A3TSc:DELay:DISPatch
	[SOURce<HW>]:BB:A3TSc:DELay:DYNamic
	[SOURce<HW>]:BB:A3TSc:DELay:MAXImum
	[SOURce<HW>]:BB:A3TSc:DELay:NETWork
	[SOURce<HW>]:BB:A3TSc:DELay:PROCess
	[SOURce<HW>]:BB:A3TSc:DELay:SFNMode
	[SOURce<HW>]:BB:A3TSc:DELay:STATic
	[SOURce<HW>]:BB:A3TSc:DELay:TOTal



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.delay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Delay_Mute.rst
	Source_Bb_A3Tsc_Delay_Tsp.rst