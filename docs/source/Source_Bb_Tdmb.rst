Tdmb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:ETIinput
	single: [SOURce<HW>]:BB:TDMB:MID
	single: [SOURce<HW>]:BB:TDMB:NET
	single: [SOURce<HW>]:BB:TDMB:NST
	single: [SOURce<HW>]:BB:TDMB:PRESet
	single: [SOURce<HW>]:BB:TDMB:SOURce
	single: [SOURce<HW>]:BB:TDMB:STATe

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:ETIinput
	[SOURce<HW>]:BB:TDMB:MID
	[SOURce<HW>]:BB:TDMB:NET
	[SOURce<HW>]:BB:TDMB:NST
	[SOURce<HW>]:BB:TDMB:PRESet
	[SOURce<HW>]:BB:TDMB:SOURce
	[SOURce<HW>]:BB:TDMB:STATe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.TdmbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.tdmb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Tdmb_DataRate.rst
	Source_Bb_Tdmb_Delay.rst
	Source_Bb_Tdmb_InputPy.rst
	Source_Bb_Tdmb_Prbs.rst
	Source_Bb_Tdmb_Protection.rst
	Source_Bb_Tdmb_Scid.rst
	Source_Bb_Tdmb_Setting.rst
	Source_Bb_Tdmb_Special.rst
	Source_Bb_Tdmb_Tii.rst