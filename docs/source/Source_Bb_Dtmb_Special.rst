Special
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DTMB:[SPECial]:SIPNormal

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:[SPECial]:SIPNormal



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dtmb.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dtmb_Special_Settings.rst