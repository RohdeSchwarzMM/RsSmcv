Nplp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:PLP:NPLP

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:PLP:NPLP



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Plp.Nplp.NplpCls
	:members:
	:undoc-members:
	:noindex: