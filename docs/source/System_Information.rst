Information
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:INFormation:SR
	single: SYSTem:INFormation

.. code-block:: python

	SYSTem:INFormation:SR
	SYSTem:INFormation



.. autoclass:: RsSmcv.Implementations.System.Information.InformationCls
	:members:
	:undoc-members:
	:noindex: