Cell
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:CELL:ID

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:CELL:ID



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Cell.CellCls
	:members:
	:undoc-members:
	:noindex: