Detail
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:L:DETail:BYTes
	single: [SOURce<HW>]:BB:A3TSc:INFO:L:DETail:CELLs

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:L:DETail:BYTes
	[SOURce<HW>]:BB:A3TSc:INFO:L:DETail:CELLs



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Lpy.Detail.DetailCls
	:members:
	:undoc-members:
	:noindex: