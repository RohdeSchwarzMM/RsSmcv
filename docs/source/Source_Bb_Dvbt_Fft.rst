Fft
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:FFT:MODE

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:FFT:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Fft.FftCls
	:members:
	:undoc-members:
	:noindex: