Network
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ATSM:NETWork:ID

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:NETWork:ID



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex: