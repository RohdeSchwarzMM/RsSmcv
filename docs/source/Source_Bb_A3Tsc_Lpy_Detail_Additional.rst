Additional
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:L:DETail:ADDitional:[PARity]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:L:DETail:ADDitional:[PARity]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Lpy.Detail.Additional.AdditionalCls
	:members:
	:undoc-members:
	:noindex: