Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:SETTing:CATalog
	single: [SOURce<HW>]:BB:T2DVb:SETTing:DELete
	single: [SOURce<HW>]:BB:T2DVb:SETTing:LOAD
	single: [SOURce<HW>]:BB:T2DVb:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:SETTing:CATalog
	[SOURce<HW>]:BB:T2DVb:SETTing:DELete
	[SOURce<HW>]:BB:T2DVb:SETTing:LOAD
	[SOURce<HW>]:BB:T2DVb:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: