Tsl<TimeSlice>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.bb.dvbs2.tsl.repcap_timeSlice_get()
	driver.source.bb.dvbs2.tsl.repcap_timeSlice_set(repcap.TimeSlice.Nr1)





.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.TslCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.tsl.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_Tsl_IsPy.rst