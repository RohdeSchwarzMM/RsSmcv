Direct
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:DIRect

.. code-block:: python

	SENSe<CH>:[POWer]:DIRect



.. autoclass:: RsSmcv.Implementations.Sense.Power.Direct.DirectCls
	:members:
	:undoc-members:
	:noindex: