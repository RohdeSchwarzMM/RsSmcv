Group
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:GROup:SEQuence

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:GROup:SEQuence



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Group.GroupCls
	:members:
	:undoc-members:
	:noindex: