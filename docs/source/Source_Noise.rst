Noise
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:NOISe:[STATe]

.. code-block:: python

	[SOURce]:NOISe:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Noise.NoiseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.noise.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Noise_Bandwidth.rst
	Source_Noise_Level.rst