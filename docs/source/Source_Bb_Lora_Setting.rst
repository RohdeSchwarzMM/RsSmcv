Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:LORA:SETTing:CATalog
	single: [SOURce<HW>]:BB:LORA:SETTing:DELete
	single: [SOURce<HW>]:BB:LORA:SETTing:LOAD

.. code-block:: python

	[SOURce<HW>]:BB:LORA:SETTing:CATalog
	[SOURce<HW>]:BB:LORA:SETTing:DELete
	[SOURce<HW>]:BB:LORA:SETTing:LOAD



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.lora.setting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Lora_Setting_Store.rst