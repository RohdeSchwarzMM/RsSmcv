Max
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T1
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T2
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T3
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T4

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T1
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T2
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T3
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MAX:T4



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.InputPy.T2Mi.Max.MaxCls
	:members:
	:undoc-members:
	:noindex: