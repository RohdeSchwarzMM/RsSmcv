Connector
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TEST:CONNector:AUXio
	single: TEST:CONNector:BNC

.. code-block:: python

	TEST:CONNector:AUXio
	TEST:CONNector:BNC



.. autoclass:: RsSmcv.Implementations.Test.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex: