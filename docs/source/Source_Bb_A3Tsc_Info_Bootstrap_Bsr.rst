Bsr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:BSR:COEFficient

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:BSR:COEFficient



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Bsr.BsrCls
	:members:
	:undoc-members:
	:noindex: