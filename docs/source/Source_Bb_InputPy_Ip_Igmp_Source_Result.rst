Result
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:IGMP:[SOURce]:RESult

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:IGMP:[SOURce]:RESult



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Igmp.Source.Result.ResultCls
	:members:
	:undoc-members:
	:noindex: