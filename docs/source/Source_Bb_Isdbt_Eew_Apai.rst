Apai
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:EEW:APAI

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:EEW:APAI



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Eew.Apai.ApaiCls
	:members:
	:undoc-members:
	:noindex: