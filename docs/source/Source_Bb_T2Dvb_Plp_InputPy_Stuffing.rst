Stuffing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:INPut:STUFfing

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:INPut:STUFfing



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.InputPy.Stuffing.StuffingCls
	:members:
	:undoc-members:
	:noindex: