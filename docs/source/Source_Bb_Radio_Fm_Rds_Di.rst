Di
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:DI:ARTificial
	single: [SOURce<HW>]:BB:RADio:FM:RDS:DI:COMPressed
	single: [SOURce<HW>]:BB:RADio:FM:RDS:DI:DYNamic
	single: [SOURce<HW>]:BB:RADio:FM:RDS:DI:STEReo

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:DI:ARTificial
	[SOURce<HW>]:BB:RADio:FM:RDS:DI:COMPressed
	[SOURce<HW>]:BB:RADio:FM:RDS:DI:DYNamic
	[SOURce<HW>]:BB:RADio:FM:RDS:DI:STEReo



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Di.DiCls
	:members:
	:undoc-members:
	:noindex: