Igmp
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Igmp.IgmpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.inputPy.ip.igmp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_InputPy_Ip_Igmp_Source.rst