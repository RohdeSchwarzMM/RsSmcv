G7B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G7B:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G7B:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G7B:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G7B:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G7B:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G7B:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G7B.G7BCls
	:members:
	:undoc-members:
	:noindex: