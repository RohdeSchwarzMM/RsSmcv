TpsReserved
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:TPSReserved:STATE
	single: [SOURce<HW>]:BB:DVBT:TPSReserved:VALue

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:TPSReserved:STATE
	[SOURce<HW>]:BB:DVBT:TPSReserved:VALue



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.TpsReserved.TpsReservedCls
	:members:
	:undoc-members:
	:noindex: