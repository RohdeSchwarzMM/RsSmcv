External
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:PULM:TRIGger:EXTernal:IMPedance
	single: [SOURce<HW>]:PULM:TRIGger:EXTernal:SLOPe

.. code-block:: python

	[SOURce<HW>]:PULM:TRIGger:EXTernal:IMPedance
	[SOURce<HW>]:PULM:TRIGger:EXTernal:SLOPe



.. autoclass:: RsSmcv.Implementations.Source.Pulm.Trigger.External.ExternalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.pulm.trigger.external.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Pulm_Trigger_External_Gate.rst