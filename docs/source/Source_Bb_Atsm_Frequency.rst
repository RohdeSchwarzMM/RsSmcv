Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ATSM:FREQuency:VSBFrequency

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:FREQuency:VSBFrequency



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: