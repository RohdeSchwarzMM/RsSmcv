InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS:INPut:FORMat
	single: [SOURce<HW>]:BB:DVBS:INPut:TSCHannel
	single: [SOURce<HW>]:BB:DVBS:[INPut]:DATarate
	single: [SOURce<HW>]:BB:DVBS:INPut

.. code-block:: python

	[SOURce<HW>]:BB:DVBS:INPut:FORMat
	[SOURce<HW>]:BB:DVBS:INPut:TSCHannel
	[SOURce<HW>]:BB:DVBS:[INPut]:DATarate
	[SOURce<HW>]:BB:DVBS:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: