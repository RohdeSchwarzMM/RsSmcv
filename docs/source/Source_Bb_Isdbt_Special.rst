Special
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:[SPECial]:ACData2
	single: [SOURce<HW>]:BB:ISDBt:[SPECial]:REEDsolomon
	single: [SOURce<HW>]:BB:ISDBt:[SPECial]:TXParam

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:[SPECial]:ACData2
	[SOURce<HW>]:BB:ISDBt:[SPECial]:REEDsolomon
	[SOURce<HW>]:BB:ISDBt:[SPECial]:TXParam



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Isdbt_Special_Alert.rst
	Source_Bb_Isdbt_Special_Settings.rst
	Source_Bb_Isdbt_Special_Tmcc.rst