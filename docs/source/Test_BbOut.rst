BbOut
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST<HW>:BBOut:LRATe

.. code-block:: python

	TEST<HW>:BBOut:LRATe



.. autoclass:: RsSmcv.Implementations.Test.BbOut.BbOutCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.bbOut.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Test_BbOut_Ttest.rst