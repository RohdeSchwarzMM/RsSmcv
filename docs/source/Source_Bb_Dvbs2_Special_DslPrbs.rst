DslPrbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[SPECial]:DSLPrbs:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[SPECial]:DSLPrbs:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Special.DslPrbs.DslPrbsCls
	:members:
	:undoc-members:
	:noindex: