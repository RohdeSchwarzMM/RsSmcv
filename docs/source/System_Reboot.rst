Reboot
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:REBoot

.. code-block:: python

	SYSTem:REBoot



.. autoclass:: RsSmcv.Implementations.System.Reboot.RebootCls
	:members:
	:undoc-members:
	:noindex: