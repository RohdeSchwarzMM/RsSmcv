Write
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:WRITe:RESult

.. code-block:: python

	TEST:WRITe:RESult



.. autoclass:: RsSmcv.Implementations.Test.Write.WriteCls
	:members:
	:undoc-members:
	:noindex: