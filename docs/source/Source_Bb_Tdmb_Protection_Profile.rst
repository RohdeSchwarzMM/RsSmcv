Profile<Profile>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.source.bb.tdmb.protection.profile.repcap_profile_get()
	driver.source.bb.tdmb.protection.profile.repcap_profile_set(repcap.Profile.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:TDMB:PROTection:PROFile<CH>

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:PROTection:PROFile<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Protection.Profile.ProfileCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.tdmb.protection.profile.clone()