Direction
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:INPut:USER<CH>:DIRection

.. code-block:: python

	[SOURce]:INPut:USER<CH>:DIRection



.. autoclass:: RsSmcv.Implementations.Source.InputPy.User.Direction.DirectionCls
	:members:
	:undoc-members:
	:noindex: