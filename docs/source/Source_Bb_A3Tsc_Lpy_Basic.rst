Basic
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:L:BASic:FECType
	single: [SOURce<HW>]:BB:A3TSc:L:BASic:VERSion

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:L:BASic:FECType
	[SOURce<HW>]:BB:A3TSc:L:BASic:VERSion



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Lpy.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: