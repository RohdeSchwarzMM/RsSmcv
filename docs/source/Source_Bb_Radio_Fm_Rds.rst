Rds
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:CT
	single: [SOURce<HW>]:BB:RADio:FM:RDS:CTOFfset
	single: [SOURce<HW>]:BB:RADio:FM:RDS:DEViation
	single: [SOURce<HW>]:BB:RADio:FM:RDS:MS
	single: [SOURce<HW>]:BB:RADio:FM:RDS:PI
	single: [SOURce<HW>]:BB:RADio:FM:RDS:PS
	single: [SOURce<HW>]:BB:RADio:FM:RDS:PTY
	single: [SOURce<HW>]:BB:RADio:FM:RDS:PTYN
	single: [SOURce<HW>]:BB:RADio:FM:RDS:RT
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TA
	single: [SOURce<HW>]:BB:RADio:FM:RDS:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:CT
	[SOURce<HW>]:BB:RADio:FM:RDS:CTOFfset
	[SOURce<HW>]:BB:RADio:FM:RDS:DEViation
	[SOURce<HW>]:BB:RADio:FM:RDS:MS
	[SOURce<HW>]:BB:RADio:FM:RDS:PI
	[SOURce<HW>]:BB:RADio:FM:RDS:PS
	[SOURce<HW>]:BB:RADio:FM:RDS:PTY
	[SOURce<HW>]:BB:RADio:FM:RDS:PTYN
	[SOURce<HW>]:BB:RADio:FM:RDS:RT
	[SOURce<HW>]:BB:RADio:FM:RDS:TA
	[SOURce<HW>]:BB:RADio:FM:RDS:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.RdsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Af.rst
	Source_Bb_Radio_Fm_Rds_Di.rst
	Source_Bb_Radio_Fm_Rds_Eon.rst
	Source_Bb_Radio_Fm_Rds_Group.rst
	Source_Bb_Radio_Fm_Rds_Opf.rst
	Source_Bb_Radio_Fm_Rds_Tmc.rst
	Source_Bb_Radio_Fm_Rds_Tp.rst