BcIp
----------------------------------------





.. autoclass:: RsSmcv.Implementations.System.Communicate.BcIp.BcIpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.bcIp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_BcIp_Network.rst