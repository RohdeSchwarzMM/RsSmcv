Useful
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Useful.UsefulCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dtmb.useful.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dtmb_Useful_Rate.rst