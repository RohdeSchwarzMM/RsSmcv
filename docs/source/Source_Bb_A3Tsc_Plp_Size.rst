Size
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:SIZE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:SIZE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Size.SizeCls
	:members:
	:undoc-members:
	:noindex: