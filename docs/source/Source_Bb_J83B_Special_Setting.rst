Setting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:J83B:[SPECial]:SETTing:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:J83B:[SPECial]:SETTing:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.Special.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: