InputPy
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.plp.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Plp_InputPy_DataRate.rst
	Source_Bb_T2Dvb_Plp_InputPy_FormatPy.rst
	Source_Bb_T2Dvb_Plp_InputPy_Stuffing.rst
	Source_Bb_T2Dvb_Plp_InputPy_TestSignal.rst