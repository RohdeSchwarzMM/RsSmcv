Bootstrap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SPECial:BOOTstrap:EAS
	single: [SOURce<HW>]:BB:A3TSc:SPECial:BOOTstrap:MINor

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SPECial:BOOTstrap:EAS
	[SOURce<HW>]:BB:A3TSc:SPECial:BOOTstrap:MINor



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Special.Bootstrap.BootstrapCls
	:members:
	:undoc-members:
	:noindex: