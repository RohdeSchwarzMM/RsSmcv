Info
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:INFO:DP
	single: [SOURce<HW>]:BB:T2DVb:INFO:DPUSed
	single: [SOURce<HW>]:BB:T2DVb:INFO:POSBits
	single: [SOURce<HW>]:BB:T2DVb:INFO:POSCells
	single: [SOURce<HW>]:BB:T2DVb:INFO:PREBits
	single: [SOURce<HW>]:BB:T2DVb:INFO:PRECells
	single: [SOURce<HW>]:BB:T2DVb:INFO:TF
	single: [SOURce<HW>]:BB:T2DVb:INFO:TFEF
	single: [SOURce<HW>]:BB:T2DVb:INFO:TP1
	single: [SOURce<HW>]:BB:T2DVb:INFO:TP2
	single: [SOURce<HW>]:BB:T2DVb:INFO:TS
	single: [SOURce<HW>]:BB:T2DVb:INFO:TSF

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:INFO:DP
	[SOURce<HW>]:BB:T2DVb:INFO:DPUSed
	[SOURce<HW>]:BB:T2DVb:INFO:POSBits
	[SOURce<HW>]:BB:T2DVb:INFO:POSCells
	[SOURce<HW>]:BB:T2DVb:INFO:PREBits
	[SOURce<HW>]:BB:T2DVb:INFO:PRECells
	[SOURce<HW>]:BB:T2DVb:INFO:TF
	[SOURce<HW>]:BB:T2DVb:INFO:TFEF
	[SOURce<HW>]:BB:T2DVb:INFO:TP1
	[SOURce<HW>]:BB:T2DVb:INFO:TP2
	[SOURce<HW>]:BB:T2DVb:INFO:TS
	[SOURce<HW>]:BB:T2DVb:INFO:TSF



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Info.InfoCls
	:members:
	:undoc-members:
	:noindex: