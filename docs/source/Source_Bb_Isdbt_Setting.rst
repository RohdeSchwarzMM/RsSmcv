Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:SETTing:CATalog
	single: [SOURce<HW>]:BB:ISDBt:SETTing:DELete
	single: [SOURce<HW>]:BB:ISDBt:SETTing:LOAD
	single: [SOURce<HW>]:BB:ISDBt:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:SETTing:CATalog
	[SOURce<HW>]:BB:ISDBt:SETTing:DELete
	[SOURce<HW>]:BB:ISDBt:SETTing:LOAD
	[SOURce<HW>]:BB:ISDBt:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: