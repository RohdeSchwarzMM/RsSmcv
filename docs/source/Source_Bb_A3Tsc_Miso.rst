Miso
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:MISo:IDX
	single: [SOURce<HW>]:BB:A3TSc:MISo:NTX

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:MISo:IDX
	[SOURce<HW>]:BB:A3TSc:MISo:NTX



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Miso.MisoCls
	:members:
	:undoc-members:
	:noindex: