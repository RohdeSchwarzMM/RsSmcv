ApLayer
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:AM:APLayer:ATT

.. code-block:: python

	[SOURce<HW>]:BB:RADio:AM:APLayer:ATT



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Am.ApLayer.ApLayerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.am.apLayer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Am_ApLayer_Library.rst