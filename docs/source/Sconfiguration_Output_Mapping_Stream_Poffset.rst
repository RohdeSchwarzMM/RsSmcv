Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SCONfiguration:OUTPut:MAPPing:STReam<ST>:POFFset

.. code-block:: python

	SCONfiguration:OUTPut:MAPPing:STReam<ST>:POFFset



.. autoclass:: RsSmcv.Implementations.Sconfiguration.Output.Mapping.Stream.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: