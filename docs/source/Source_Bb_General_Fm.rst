Fm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:GENeral:FM:DEViation
	single: [SOURce<HW>]:BB:GENeral:FM:FREQuency
	single: [SOURce<HW>]:BB:GENeral:FM:PERiod
	single: [SOURce<HW>]:BB:GENeral:FM:SHAPe
	single: [SOURce<HW>]:BB:GENeral:FM:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:FM:DEViation
	[SOURce<HW>]:BB:GENeral:FM:FREQuency
	[SOURce<HW>]:BB:GENeral:FM:PERiod
	[SOURce<HW>]:BB:GENeral:FM:SHAPe
	[SOURce<HW>]:BB:GENeral:FM:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Fm.FmCls
	:members:
	:undoc-members:
	:noindex: