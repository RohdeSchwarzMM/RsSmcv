Drm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DRM:BANDwidth
	single: [SOURce<HW>]:BB:DRM:FILename
	single: [SOURce<HW>]:BB:DRM:INTerleaver
	single: [SOURce<HW>]:BB:DRM:LABel
	single: [SOURce<HW>]:BB:DRM:MODE
	single: [SOURce<HW>]:BB:DRM:NUMData
	single: [SOURce<HW>]:BB:DRM:NUMaudio
	single: [SOURce<HW>]:BB:DRM:PORT
	single: [SOURce<HW>]:BB:DRM:PRESet
	single: [SOURce<HW>]:BB:DRM:SOURce
	single: [SOURce<HW>]:BB:DRM:STATe
	single: [SOURce<HW>]:BB:DRM:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:DRM:BANDwidth
	[SOURce<HW>]:BB:DRM:FILename
	[SOURce<HW>]:BB:DRM:INTerleaver
	[SOURce<HW>]:BB:DRM:LABel
	[SOURce<HW>]:BB:DRM:MODE
	[SOURce<HW>]:BB:DRM:NUMData
	[SOURce<HW>]:BB:DRM:NUMaudio
	[SOURce<HW>]:BB:DRM:PORT
	[SOURce<HW>]:BB:DRM:PRESet
	[SOURce<HW>]:BB:DRM:SOURce
	[SOURce<HW>]:BB:DRM:STATe
	[SOURce<HW>]:BB:DRM:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Drm.DrmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.drm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Drm_Msc.rst
	Source_Bb_Drm_Sdc.rst
	Source_Bb_Drm_Setting.rst