Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: READ<CH>:[POWer]

.. code-block:: python

	READ<CH>:[POWer]



.. autoclass:: RsSmcv.Implementations.Read.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: