AlpType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:ALPType

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:ALPType



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.AlpType.AlpTypeCls
	:members:
	:undoc-members:
	:noindex: