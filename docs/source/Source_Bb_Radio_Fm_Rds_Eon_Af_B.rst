B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:AF:B:NUMBer
	single: [SOURce<HW>]:BB:RADio:FM:RDS:EON:AF:B:TFRequency

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:EON:AF:B:NUMBer
	[SOURce<HW>]:BB:RADio:FM:RDS:EON:AF:B:TFRequency



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Eon.Af.B.BCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.eon.af.b.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Eon_Af_B_Frequency.rst