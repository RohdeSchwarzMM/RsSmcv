FecFrame
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:FECFrame

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:FECFrame



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.IsPy.FecFrame.FecFrameCls
	:members:
	:undoc-members:
	:noindex: