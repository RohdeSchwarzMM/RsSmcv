Rate<Profile>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr16
	rc = driver.source.bb.drm.msc.rate.repcap_profile_get()
	driver.source.bb.drm.msc.rate.repcap_profile_set(repcap.Profile.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DRM:MSC:RATE<CH>

.. code-block:: python

	[SOURce<HW>]:BB:DRM:MSC:RATE<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Drm.Msc.Rate.RateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.drm.msc.rate.clone()