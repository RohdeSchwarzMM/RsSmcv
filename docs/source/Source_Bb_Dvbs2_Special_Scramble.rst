Scramble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[SPECial]:SCRamble:SEQuence
	single: [SOURce<HW>]:BB:DVBS2:[SPECial]:SCRamble:STATe

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[SPECial]:SCRamble:SEQuence
	[SOURce<HW>]:BB:DVBS2:[SPECial]:SCRamble:STATe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Special.Scramble.ScrambleCls
	:members:
	:undoc-members:
	:noindex: