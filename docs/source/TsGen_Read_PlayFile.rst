PlayFile
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TSGen:READ:PLAYfile:LENGth

.. code-block:: python

	TSGen:READ:PLAYfile:LENGth



.. autoclass:: RsSmcv.Implementations.TsGen.Read.PlayFile.PlayFileCls
	:members:
	:undoc-members:
	:noindex: