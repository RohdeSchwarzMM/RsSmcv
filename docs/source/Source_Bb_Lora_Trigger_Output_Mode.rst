Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:MODE

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.Output.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: