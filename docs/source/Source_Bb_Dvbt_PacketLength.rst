PacketLength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:PACKetlength:LOW
	single: [SOURce<HW>]:BB:DVBT:PACKetlength:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:PACKetlength:LOW
	[SOURce<HW>]:BB:DVBT:PACKetlength:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.PacketLength.PacketLengthCls
	:members:
	:undoc-members:
	:noindex: