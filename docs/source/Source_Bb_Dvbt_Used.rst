Used
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:USED:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:USED:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Used.UsedCls
	:members:
	:undoc-members:
	:noindex: