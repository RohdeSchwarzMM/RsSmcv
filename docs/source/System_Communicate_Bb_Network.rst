Network
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:BB<HW>:NETWork:PORT

.. code-block:: python

	SYSTem:COMMunicate:BB<HW>:NETWork:PORT



.. autoclass:: RsSmcv.Implementations.System.Communicate.Bb.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex: