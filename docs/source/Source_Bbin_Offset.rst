Offset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BBIN:OFFSet:I
	single: [SOURce<HW>]:BBIN:OFFSet:Q

.. code-block:: python

	[SOURce<HW>]:BBIN:OFFSet:I
	[SOURce<HW>]:BBIN:OFFSet:Q



.. autoclass:: RsSmcv.Implementations.Source.Bbin.Offset.OffsetCls
	:members:
	:undoc-members:
	:noindex: