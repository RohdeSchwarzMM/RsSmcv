InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ATSM:INPut:FORMat
	single: [SOURce<HW>]:BB:ATSM:INPut:TSCHannel
	single: [SOURce<HW>]:BB:ATSM:[INPut]:DATarate
	single: [SOURce<HW>]:BB:ATSM:INPut

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:INPut:FORMat
	[SOURce<HW>]:BB:ATSM:INPut:TSCHannel
	[SOURce<HW>]:BB:ATSM:[INPut]:DATarate
	[SOURce<HW>]:BB:ATSM:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: