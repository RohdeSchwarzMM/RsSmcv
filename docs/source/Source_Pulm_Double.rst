Double
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:PULM:DOUBle:STATe
	single: [SOURce<HW>]:PULM:DOUBle:WIDTh

.. code-block:: python

	[SOURce<HW>]:PULM:DOUBle:STATe
	[SOURce<HW>]:PULM:DOUBle:WIDTh



.. autoclass:: RsSmcv.Implementations.Source.Pulm.Double.DoubleCls
	:members:
	:undoc-members:
	:noindex: