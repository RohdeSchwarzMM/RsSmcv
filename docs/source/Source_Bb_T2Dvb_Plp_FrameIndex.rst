FrameIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:FRAMeindex

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:FRAMeindex



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.FrameIndex.FrameIndexCls
	:members:
	:undoc-members:
	:noindex: