Basic
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:L:BASic:BYTes
	single: [SOURce<HW>]:BB:A3TSc:INFO:L:BASic:CELLs

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:L:BASic:BYTes
	[SOURce<HW>]:BB:A3TSc:INFO:L:BASic:CELLs



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Lpy.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: