Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:SOURce

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:SOURce



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_Source_IsPy.rst