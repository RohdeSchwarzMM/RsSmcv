State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:RPC:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:RPC:[STATe]



.. autoclass:: RsSmcv.Implementations.System.Security.Network.Rpc.State.StateCls
	:members:
	:undoc-members:
	:noindex: