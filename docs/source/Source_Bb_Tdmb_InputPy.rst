InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:INPut:ETIChannel
	single: [SOURce<HW>]:BB:TDMB:INPut:FORMat
	single: [SOURce<HW>]:BB:TDMB:INPut

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:INPut:ETIChannel
	[SOURce<HW>]:BB:TDMB:INPut:FORMat
	[SOURce<HW>]:BB:TDMB:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: