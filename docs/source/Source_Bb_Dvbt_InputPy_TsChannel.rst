TsChannel
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:INPut:TSCHannel:LOW
	single: [SOURce<HW>]:BB:DVBT:INPut:TSCHannel:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:INPut:TSCHannel:LOW
	[SOURce<HW>]:BB:DVBT:INPut:TSCHannel:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.InputPy.TsChannel.TsChannelCls
	:members:
	:undoc-members:
	:noindex: