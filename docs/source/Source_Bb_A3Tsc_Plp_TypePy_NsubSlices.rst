NsubSlices
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TYPE:NSUBslices

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TYPE:NSUBslices



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.TypePy.NsubSlices.NsubSlicesCls
	:members:
	:undoc-members:
	:noindex: