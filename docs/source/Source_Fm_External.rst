External
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FM:EXTernal:COUPling
	single: [SOURce<HW>]:FM:EXTernal:DEViation

.. code-block:: python

	[SOURce<HW>]:FM:EXTernal:COUPling
	[SOURce<HW>]:FM:EXTernal:DEViation



.. autoclass:: RsSmcv.Implementations.Source.Fm.External.ExternalCls
	:members:
	:undoc-members:
	:noindex: