Special
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBC:[SPECial]:REEDsolomon

.. code-block:: python

	[SOURce<HW>]:BB:DVBC:[SPECial]:REEDsolomon



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbc.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbc.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbc_Special_Setting.rst