Alias
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:ALIas

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:ALIas



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Alias.AliasCls
	:members:
	:undoc-members:
	:noindex: