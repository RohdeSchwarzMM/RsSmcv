Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:TIL:LENGth

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:TIL:LENGth



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Til.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: