Port
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:PORT

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:PORT



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Port.PortCls
	:members:
	:undoc-members:
	:noindex: