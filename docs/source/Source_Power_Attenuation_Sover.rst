Sover
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:POWer:ATTenuation:SOVer:[OFFSet]

.. code-block:: python

	[SOURce<HW>]:POWer:ATTenuation:SOVer:[OFFSet]



.. autoclass:: RsSmcv.Implementations.Source.Power.Attenuation.Sover.SoverCls
	:members:
	:undoc-members:
	:noindex: