Level<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.bb.tdmb.protection.level.repcap_index_get()
	driver.source.bb.tdmb.protection.level.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:TDMB:PROTection:LEVel<CH>

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:PROTection:LEVel<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Protection.Level.LevelCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.tdmb.protection.level.clone()