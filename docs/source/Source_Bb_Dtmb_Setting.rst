Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DTMB:SETTing:CATalog
	single: [SOURce<HW>]:BB:DTMB:SETTing:DELete
	single: [SOURce<HW>]:BB:DTMB:SETTing:LOAD
	single: [SOURce<HW>]:BB:DTMB:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:SETTing:CATalog
	[SOURce<HW>]:BB:DTMB:SETTing:DELete
	[SOURce<HW>]:BB:DTMB:SETTing:LOAD
	[SOURce<HW>]:BB:DTMB:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: