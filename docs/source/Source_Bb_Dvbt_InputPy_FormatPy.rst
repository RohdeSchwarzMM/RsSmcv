FormatPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:INPut:FORMat:LOW
	single: [SOURce<HW>]:BB:DVBT:INPut:FORMat

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:INPut:FORMat:LOW
	[SOURce<HW>]:BB:DVBT:INPut:FORMat



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.InputPy.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex: