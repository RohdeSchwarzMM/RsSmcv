CmType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:CMTYpe

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:CMTYpe



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.CmType.CmTypeCls
	:members:
	:undoc-members:
	:noindex: