A3Tsc
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:BSID
	single: [SOURce<HW>]:BB:A3TSc:IPPacket
	single: [SOURce<HW>]:BB:A3TSc:LLS
	single: [SOURce<HW>]:BB:A3TSc:NETWorkmode
	single: [SOURce<HW>]:BB:A3TSc:NRF
	single: [SOURce<HW>]:BB:A3TSc:PAPR
	single: [SOURce<HW>]:BB:A3TSc:PAYLoad
	single: [SOURce<HW>]:BB:A3TSc:PID
	single: [SOURce<HW>]:BB:A3TSc:PIDTestpack
	single: [SOURce<HW>]:BB:A3TSc:PRESet
	single: [SOURce<HW>]:BB:A3TSc:SOURce
	single: [SOURce<HW>]:BB:A3TSc:STATe
	single: [SOURce<HW>]:BB:A3TSc:TIME
	single: [SOURce<HW>]:BB:A3TSc:TSPacket

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:BSID
	[SOURce<HW>]:BB:A3TSc:IPPacket
	[SOURce<HW>]:BB:A3TSc:LLS
	[SOURce<HW>]:BB:A3TSc:NETWorkmode
	[SOURce<HW>]:BB:A3TSc:NRF
	[SOURce<HW>]:BB:A3TSc:PAPR
	[SOURce<HW>]:BB:A3TSc:PAYLoad
	[SOURce<HW>]:BB:A3TSc:PID
	[SOURce<HW>]:BB:A3TSc:PIDTestpack
	[SOURce<HW>]:BB:A3TSc:PRESet
	[SOURce<HW>]:BB:A3TSc:SOURce
	[SOURce<HW>]:BB:A3TSc:STATe
	[SOURce<HW>]:BB:A3TSc:TIME
	[SOURce<HW>]:BB:A3TSc:TSPacket



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.A3TscCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Channel.rst
	Source_Bb_A3Tsc_Delay.rst
	Source_Bb_A3Tsc_Frame.rst
	Source_Bb_A3Tsc_Info.rst
	Source_Bb_A3Tsc_InputPy.rst
	Source_Bb_A3Tsc_Lpy.rst
	Source_Bb_A3Tsc_Miso.rst
	Source_Bb_A3Tsc_Plp.rst
	Source_Bb_A3Tsc_Prbs.rst
	Source_Bb_A3Tsc_Return.rst
	Source_Bb_A3Tsc_Setting.rst
	Source_Bb_A3Tsc_Special.rst
	Source_Bb_A3Tsc_Subframe.rst
	Source_Bb_A3Tsc_Txid.rst