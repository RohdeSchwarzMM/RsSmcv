Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:DELay:MINimum

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:DELay:MINimum



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Trigger.Output.Delay.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: