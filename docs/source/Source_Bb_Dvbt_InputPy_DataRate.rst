DataRate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:[INPut]:DATarate:LOW
	single: [SOURce<HW>]:BB:DVBT:[INPut]:DATarate:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:[INPut]:DATarate:LOW
	[SOURce<HW>]:BB:DVBT:[INPut]:DATarate:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.InputPy.DataRate.DataRateCls
	:members:
	:undoc-members:
	:noindex: