Bb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:BB:CONNection

.. code-block:: python

	TEST:BB:CONNection



.. autoclass:: RsSmcv.Implementations.Test.Bb.BbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.bb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Test_Bb_Generator.rst