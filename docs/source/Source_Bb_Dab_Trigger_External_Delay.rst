Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DAB:TRIGger:[EXTernal<CH>]:DELay

.. code-block:: python

	[SOURce<HW>]:BB:DAB:TRIGger:[EXTernal<CH>]:DELay



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dab.Trigger.External.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: