Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SETTing:CATalog
	single: [SOURce<HW>]:BB:A3TSc:SETTing:DELete
	single: [SOURce<HW>]:BB:A3TSc:SETTing:LOAD
	single: [SOURce<HW>]:BB:A3TSc:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SETTing:CATalog
	[SOURce<HW>]:BB:A3TSc:SETTing:DELete
	[SOURce<HW>]:BB:A3TSc:SETTing:LOAD
	[SOURce<HW>]:BB:A3TSc:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: