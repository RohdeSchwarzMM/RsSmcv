Extended
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:EXTended

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:EXTended



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Til.Extended.ExtendedCls
	:members:
	:undoc-members:
	:noindex: