Ape1
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:EEW:APE1

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:EEW:APE1



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Eew.Ape1.Ape1Cls
	:members:
	:undoc-members:
	:noindex: