Store
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:STORe:FAST
	single: [SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:STORe:FAST
	[SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Mcarrier.Setting.Store.StoreCls
	:members:
	:undoc-members:
	:noindex: