InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:INPut:FORMat
	single: [SOURce<HW>]:BB:T2DVb:INPut:NPLP
	single: [SOURce<HW>]:BB:T2DVb:INPut:TSCHannel
	single: [SOURce<HW>]:BB:T2DVb:INPut

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:INPut:FORMat
	[SOURce<HW>]:BB:T2DVb:INPut:NPLP
	[SOURce<HW>]:BB:T2DVb:INPut:TSCHannel
	[SOURce<HW>]:BB:T2DVb:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_InputPy_T2Mi.rst