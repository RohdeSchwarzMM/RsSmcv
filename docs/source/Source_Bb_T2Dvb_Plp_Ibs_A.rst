A
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:IBS:A

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:IBS:A



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Ibs.A.ACls
	:members:
	:undoc-members:
	:noindex: