Noise
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AWGN:POWer:NOISe:TOTal
	single: [SOURce<HW>]:AWGN:POWer:NOISe

.. code-block:: python

	[SOURce<HW>]:AWGN:POWer:NOISe:TOTal
	[SOURce<HW>]:AWGN:POWer:NOISe



.. autoclass:: RsSmcv.Implementations.Source.Awgn.Power.Noise.NoiseCls
	:members:
	:undoc-members:
	:noindex: