Layer
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Layer.LayerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.plp.layer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Plp_Layer_Layer.rst
	Source_Bb_A3Tsc_Plp_Layer_Level.rst