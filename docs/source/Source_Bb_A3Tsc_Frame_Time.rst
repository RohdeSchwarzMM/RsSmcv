Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:TIME:[OFFSet]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:FRAMe:TIME:[OFFSet]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Frame.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: