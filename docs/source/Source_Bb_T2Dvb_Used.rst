Used
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:USED:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:USED:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Used.UsedCls
	:members:
	:undoc-members:
	:noindex: