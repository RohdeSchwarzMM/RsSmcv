Const
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TEST:BB:GENerator:CONSt:I
	single: TEST:BB:GENerator:CONSt:Q

.. code-block:: python

	TEST:BB:GENerator:CONSt:I
	TEST:BB:GENerator:CONSt:Q



.. autoclass:: RsSmcv.Implementations.Test.Bb.Generator.Const.ConstCls
	:members:
	:undoc-members:
	:noindex: