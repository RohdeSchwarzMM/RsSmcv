DataRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:[INPut]:DATarate

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:[INPut]:DATarate



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.InputPy.DataRate.DataRateCls
	:members:
	:undoc-members:
	:noindex: