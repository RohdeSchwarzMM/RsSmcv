Segments
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:SEGMents:A
	single: [SOURce<HW>]:BB:ISDBt:SEGMents:B
	single: [SOURce<HW>]:BB:ISDBt:SEGMents:C

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:SEGMents:A
	[SOURce<HW>]:BB:ISDBt:SEGMents:B
	[SOURce<HW>]:BB:ISDBt:SEGMents:C



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Segments.SegmentsCls
	:members:
	:undoc-members:
	:noindex: