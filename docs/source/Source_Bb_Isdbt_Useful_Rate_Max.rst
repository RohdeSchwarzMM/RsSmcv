Max
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:MAX:A
	single: [SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:MAX:B
	single: [SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:MAX:C

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:MAX:A
	[SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:MAX:B
	[SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:MAX:C



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Useful.Rate.Max.MaxCls
	:members:
	:undoc-members:
	:noindex: