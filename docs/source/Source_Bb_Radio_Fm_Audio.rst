Audio
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:AF1
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:AF2
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:DEViation
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:MODE
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:NDEViation
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:PREemphasis
	single: [SOURce<HW>]:BB:RADio:FM:AUDio:SOURce

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:AUDio:AF1
	[SOURce<HW>]:BB:RADio:FM:AUDio:AF2
	[SOURce<HW>]:BB:RADio:FM:AUDio:DEViation
	[SOURce<HW>]:BB:RADio:FM:AUDio:MODE
	[SOURce<HW>]:BB:RADio:FM:AUDio:NDEViation
	[SOURce<HW>]:BB:RADio:FM:AUDio:PREemphasis
	[SOURce<HW>]:BB:RADio:FM:AUDio:SOURce



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Audio.AudioCls
	:members:
	:undoc-members:
	:noindex: