Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBC:SETTing:CATalog
	single: [SOURce<HW>]:BB:DVBC:SETTing:DELete
	single: [SOURce<HW>]:BB:DVBC:SETTing:LOAD
	single: [SOURce<HW>]:BB:DVBC:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DVBC:SETTing:CATalog
	[SOURce<HW>]:BB:DVBC:SETTing:DELete
	[SOURce<HW>]:BB:DVBC:SETTing:LOAD
	[SOURce<HW>]:BB:DVBC:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbc.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: