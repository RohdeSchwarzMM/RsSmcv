External
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:PM:EXTernal:COUPling
	single: [SOURce<HW>]:PM:EXTernal:DEViation

.. code-block:: python

	[SOURce<HW>]:PM:EXTernal:COUPling
	[SOURce<HW>]:PM:EXTernal:DEViation



.. autoclass:: RsSmcv.Implementations.Source.Pm.External.ExternalCls
	:members:
	:undoc-members:
	:noindex: