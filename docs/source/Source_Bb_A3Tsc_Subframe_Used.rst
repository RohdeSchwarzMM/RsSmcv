Used
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Used.UsedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.subframe.used.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Subframe_Used_Bandwidth.rst