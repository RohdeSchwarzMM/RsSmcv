Blocks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:BLOCks

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:BLOCks



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Blocks.BlocksCls
	:members:
	:undoc-members:
	:noindex: