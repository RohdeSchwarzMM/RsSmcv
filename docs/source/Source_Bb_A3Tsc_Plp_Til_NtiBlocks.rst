NtiBlocks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:NTIBlocks

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:NTIBlocks



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Til.NtiBlocks.NtiBlocksCls
	:members:
	:undoc-members:
	:noindex: