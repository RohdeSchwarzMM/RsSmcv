T2Dvb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:LDATa
	single: [SOURce<HW>]:BB:T2DVb:LF
	single: [SOURce<HW>]:BB:T2DVb:NAUX
	single: [SOURce<HW>]:BB:T2DVb:NETWorkmode
	single: [SOURce<HW>]:BB:T2DVb:NSUB
	single: [SOURce<HW>]:BB:T2DVb:NT2Frames
	single: [SOURce<HW>]:BB:T2DVb:PAPR
	single: [SOURce<HW>]:BB:T2DVb:PAYLoad
	single: [SOURce<HW>]:BB:T2DVb:PID
	single: [SOURce<HW>]:BB:T2DVb:PIDTestpack
	single: [SOURce<HW>]:BB:T2DVb:PILot
	single: [SOURce<HW>]:BB:T2DVb:PRESet
	single: [SOURce<HW>]:BB:T2DVb:PROFile
	single: [SOURce<HW>]:BB:T2DVb:SOURce
	single: [SOURce<HW>]:BB:T2DVb:STATe
	single: [SOURce<HW>]:BB:T2DVb:TFS
	single: [SOURce<HW>]:BB:T2DVb:TSPacket
	single: [SOURce<HW>]:BB:T2DVb:TXSYs

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:LDATa
	[SOURce<HW>]:BB:T2DVb:LF
	[SOURce<HW>]:BB:T2DVb:NAUX
	[SOURce<HW>]:BB:T2DVb:NETWorkmode
	[SOURce<HW>]:BB:T2DVb:NSUB
	[SOURce<HW>]:BB:T2DVb:NT2Frames
	[SOURce<HW>]:BB:T2DVb:PAPR
	[SOURce<HW>]:BB:T2DVb:PAYLoad
	[SOURce<HW>]:BB:T2DVb:PID
	[SOURce<HW>]:BB:T2DVb:PIDTestpack
	[SOURce<HW>]:BB:T2DVb:PILot
	[SOURce<HW>]:BB:T2DVb:PRESet
	[SOURce<HW>]:BB:T2DVb:PROFile
	[SOURce<HW>]:BB:T2DVb:SOURce
	[SOURce<HW>]:BB:T2DVb:STATe
	[SOURce<HW>]:BB:T2DVb:TFS
	[SOURce<HW>]:BB:T2DVb:TSPacket
	[SOURce<HW>]:BB:T2DVb:TXSYs



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.T2DvbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Bandwidth.rst
	Source_Bb_T2Dvb_Channel.rst
	Source_Bb_T2Dvb_Delay.rst
	Source_Bb_T2Dvb_Fef.rst
	Source_Bb_T2Dvb_Fft.rst
	Source_Bb_T2Dvb_Guard.rst
	Source_Bb_T2Dvb_Id.rst
	Source_Bb_T2Dvb_Info.rst
	Source_Bb_T2Dvb_InputPy.rst
	Source_Bb_T2Dvb_Lpy.rst
	Source_Bb_T2Dvb_Miso.rst
	Source_Bb_T2Dvb_Plp.rst
	Source_Bb_T2Dvb_Prbs.rst
	Source_Bb_T2Dvb_Setting.rst
	Source_Bb_T2Dvb_Used.rst