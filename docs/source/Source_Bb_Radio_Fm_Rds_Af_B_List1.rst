List1
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST1:NUMBer
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST1:TFRequency

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST1:NUMBer
	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST1:TFRequency



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.B.List1.List1Cls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.b.list1.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Af_B_List1_Desc.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List1_Frequency.rst