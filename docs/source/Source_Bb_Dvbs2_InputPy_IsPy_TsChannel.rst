TsChannel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:INPut:[IS<CH>]:TSCHannel

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:INPut:[IS<CH>]:TSCHannel



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.InputPy.IsPy.TsChannel.TsChannelCls
	:members:
	:undoc-members:
	:noindex: