List2
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST2:NUMBer
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST2:TFRequency

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST2:NUMBer
	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST2:TFRequency



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.B.List2.List2Cls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.b.list2.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Af_B_List2_Desc.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List2_Frequency.rst