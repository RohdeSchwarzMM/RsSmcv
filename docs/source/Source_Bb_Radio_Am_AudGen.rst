AudGen
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:AM:AUDGen:FRQ
	single: [SOURce<HW>]:BB:RADio:AM:AUDGen:LEV

.. code-block:: python

	[SOURce<HW>]:BB:RADio:AM:AUDGen:FRQ
	[SOURce<HW>]:BB:RADio:AM:AUDGen:LEV



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Am.AudGen.AudGenCls
	:members:
	:undoc-members:
	:noindex: