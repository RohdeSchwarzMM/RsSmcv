Video
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:GENeral:PULM:VIDeo:POLarity

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:PULM:VIDeo:POLarity



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Pulm.Video.VideoCls
	:members:
	:undoc-members:
	:noindex: