Plp<PhysicalLayerPipe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.bb.t2Dvb.plp.repcap_physicalLayerPipe_get()
	driver.source.bb.t2Dvb.plp.repcap_physicalLayerPipe_set(repcap.PhysicalLayerPipe.Nr1)





.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.PlpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.plp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Plp_Blocks.rst
	Source_Bb_T2Dvb_Plp_CmType.rst
	Source_Bb_T2Dvb_Plp_Constel.rst
	Source_Bb_T2Dvb_Plp_Crotation.rst
	Source_Bb_T2Dvb_Plp_FecFrame.rst
	Source_Bb_T2Dvb_Plp_FrameIndex.rst
	Source_Bb_T2Dvb_Plp_Group.rst
	Source_Bb_T2Dvb_Plp_Ibs.rst
	Source_Bb_T2Dvb_Plp_Id.rst
	Source_Bb_T2Dvb_Plp_InputPy.rst
	Source_Bb_T2Dvb_Plp_Issy.rst
	Source_Bb_T2Dvb_Plp_MaxBlocks.rst
	Source_Bb_T2Dvb_Plp_Npd.rst
	Source_Bb_T2Dvb_Plp_OibPlp.rst
	Source_Bb_T2Dvb_Plp_PacketLength.rst
	Source_Bb_T2Dvb_Plp_PadFlag.rst
	Source_Bb_T2Dvb_Plp_Rate.rst
	Source_Bb_T2Dvb_Plp_StaFlag.rst
	Source_Bb_T2Dvb_Plp_Til.rst
	Source_Bb_T2Dvb_Plp_TypePy.rst
	Source_Bb_T2Dvb_Plp_Useful.rst