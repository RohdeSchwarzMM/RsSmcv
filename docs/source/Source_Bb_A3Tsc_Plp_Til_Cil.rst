Cil
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:CIL

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:CIL



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Til.Cil.CilCls
	:members:
	:undoc-members:
	:noindex: