NidPlp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:PLP:NIDPlp

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:PLP:NIDPlp



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Plp.NidPlp.NidPlpCls
	:members:
	:undoc-members:
	:noindex: