Special
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:J83B:[SPECial]:REEDsolomon

.. code-block:: python

	[SOURce<HW>]:BB:J83B:[SPECial]:REEDsolomon



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.j83B.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_J83B_Special_Setting.rst