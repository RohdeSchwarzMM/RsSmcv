Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:CHANnel:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:CHANnel:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: