Frequency<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.test.bb.generator.frequency.repcap_index_get()
	driver.test.bb.generator.frequency.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: TEST:BB:GENerator:FREQuency<CH>

.. code-block:: python

	TEST:BB:GENerator:FREQuency<CH>



.. autoclass:: RsSmcv.Implementations.Test.Bb.Generator.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.test.bb.generator.frequency.clone()