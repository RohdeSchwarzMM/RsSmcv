InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DTMB:INPut:FORMat
	single: [SOURce<HW>]:BB:DTMB:INPut:TSCHannel
	single: [SOURce<HW>]:BB:DTMB:[INPut]:DATarate
	single: [SOURce<HW>]:BB:DTMB:INPut

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:INPut:FORMat
	[SOURce<HW>]:BB:DTMB:INPut:TSCHannel
	[SOURce<HW>]:BB:DTMB:[INPut]:DATarate
	[SOURce<HW>]:BB:DTMB:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: