Dtmb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:[IQCoder]:DTMB:INPut

.. code-block:: python

	[SOURce]:[IQCoder]:DTMB:INPut



.. autoclass:: RsSmcv.Implementations.Source.Iqcoder.Dtmb.DtmbCls
	:members:
	:undoc-members:
	:noindex: