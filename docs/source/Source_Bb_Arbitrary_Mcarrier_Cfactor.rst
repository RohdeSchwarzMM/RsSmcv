Cfactor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:MCARrier:CFACtor:MODE

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:MCARrier:CFACtor:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Mcarrier.Cfactor.CfactorCls
	:members:
	:undoc-members:
	:noindex: