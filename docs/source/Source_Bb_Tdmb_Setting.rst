Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:SETTing:CATalog
	single: [SOURce<HW>]:BB:TDMB:SETTing:DELete
	single: [SOURce<HW>]:BB:TDMB:SETTing:LOAD
	single: [SOURce<HW>]:BB:TDMB:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:SETTing:CATalog
	[SOURce<HW>]:BB:TDMB:SETTing:DELete
	[SOURce<HW>]:BB:TDMB:SETTing:LOAD
	[SOURce<HW>]:BB:TDMB:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: