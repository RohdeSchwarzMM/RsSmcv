Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:J83B:SETTing:CATalog
	single: [SOURce<HW>]:BB:J83B:SETTing:DELete
	single: [SOURce<HW>]:BB:J83B:SETTing:LOAD
	single: [SOURce<HW>]:BB:J83B:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:J83B:SETTing:CATalog
	[SOURce<HW>]:BB:J83B:SETTing:DELete
	[SOURce<HW>]:BB:J83B:SETTing:LOAD
	[SOURce<HW>]:BB:J83B:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: