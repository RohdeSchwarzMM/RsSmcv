Delay
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:DELay:DEViation
	single: [SOURce<HW>]:BB:T2DVb:DELay:DYNamic
	single: [SOURce<HW>]:BB:T2DVb:DELay:MUTep1
	single: [SOURce<HW>]:BB:T2DVb:DELay:PROCess
	single: [SOURce<HW>]:BB:T2DVb:DELay:STATic
	single: [SOURce<HW>]:BB:T2DVb:DELay:TOTal

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:DELay:DEViation
	[SOURce<HW>]:BB:T2DVb:DELay:DYNamic
	[SOURce<HW>]:BB:T2DVb:DELay:MUTep1
	[SOURce<HW>]:BB:T2DVb:DELay:PROCess
	[SOURce<HW>]:BB:T2DVb:DELay:STATic
	[SOURce<HW>]:BB:T2DVb:DELay:TOTal



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.delay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Delay_Tsp.rst