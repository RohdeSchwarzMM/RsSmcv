Setting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS:[SPECial]:SETTing:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS:[SPECial]:SETTing:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs.Special.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: