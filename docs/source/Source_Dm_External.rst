External
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Dm.External.ExternalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.dm.external.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Dm_External_Polarity.rst