Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:CATalog
	single: [SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:LOAD

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:CATalog
	[SOURce<HW>]:BB:ARBitrary:MCARrier:SETTing:LOAD



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Mcarrier.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.arbitrary.mcarrier.setting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Arbitrary_Mcarrier_Setting_Store.rst