Leakage
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:IMPairment:LEAKage:I
	single: [SOURce<HW>]:BB:IMPairment:LEAKage:Q

.. code-block:: python

	[SOURce<HW>]:BB:IMPairment:LEAKage:I
	[SOURce<HW>]:BB:IMPairment:LEAKage:Q



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.Leakage.LeakageCls
	:members:
	:undoc-members:
	:noindex: