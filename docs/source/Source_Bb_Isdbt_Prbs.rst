Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: