Constel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:CONStel

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:CONStel



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Constel.ConstelCls
	:members:
	:undoc-members:
	:noindex: