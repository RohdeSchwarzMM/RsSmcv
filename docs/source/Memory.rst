Memory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: MEMory:HFRee

.. code-block:: python

	MEMory:HFRee



.. autoclass:: RsSmcv.Implementations.Memory.MemoryCls
	:members:
	:undoc-members:
	:noindex: