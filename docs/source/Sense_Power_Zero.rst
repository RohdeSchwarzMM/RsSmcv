Zero
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:ZERO

.. code-block:: python

	SENSe<CH>:[POWer]:ZERO



.. autoclass:: RsSmcv.Implementations.Sense.Power.Zero.ZeroCls
	:members:
	:undoc-members:
	:noindex: