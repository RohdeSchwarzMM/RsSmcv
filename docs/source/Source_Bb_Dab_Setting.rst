Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DAB:SETTing:CATalog
	single: [SOURce<HW>]:BB:DAB:SETTing:DELete
	single: [SOURce<HW>]:BB:DAB:SETTing:LOAD

.. code-block:: python

	[SOURce<HW>]:BB:DAB:SETTing:CATalog
	[SOURce<HW>]:BB:DAB:SETTing:DELete
	[SOURce<HW>]:BB:DAB:SETTing:LOAD



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dab.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dab.setting.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dab_Setting_Store.rst