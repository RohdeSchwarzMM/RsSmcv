Amplitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DISPlay:ANNotation:AMPLitude

.. code-block:: python

	DISPlay:ANNotation:AMPLitude



.. autoclass:: RsSmcv.Implementations.Display.Annotation.Amplitude.AmplitudeCls
	:members:
	:undoc-members:
	:noindex: