Max
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:USEFul:[RATe]:MAX

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:USEFul:[RATe]:MAX



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Useful.Rate.Max.MaxCls
	:members:
	:undoc-members:
	:noindex: