Snumber
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe<CH>:[POWer]:SNUMber

.. code-block:: python

	SENSe<CH>:[POWer]:SNUMber



.. autoclass:: RsSmcv.Implementations.Sense.Power.Snumber.SnumberCls
	:members:
	:undoc-members:
	:noindex: