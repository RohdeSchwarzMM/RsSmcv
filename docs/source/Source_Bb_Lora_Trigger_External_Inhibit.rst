Inhibit
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:[EXTernal<CH>]:INHibit

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:[EXTernal<CH>]:INHibit



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.External.Inhibit.InhibitCls
	:members:
	:undoc-members:
	:noindex: