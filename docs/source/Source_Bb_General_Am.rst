Am
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:GENeral:AM:DEPTh
	single: [SOURce<HW>]:BB:GENeral:AM:FREQuency
	single: [SOURce<HW>]:BB:GENeral:AM:PERiod
	single: [SOURce<HW>]:BB:GENeral:AM:SHAPe
	single: [SOURce<HW>]:BB:GENeral:AM:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:AM:DEPTh
	[SOURce<HW>]:BB:GENeral:AM:FREQuency
	[SOURce<HW>]:BB:GENeral:AM:PERiod
	[SOURce<HW>]:BB:GENeral:AM:SHAPe
	[SOURce<HW>]:BB:GENeral:AM:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Am.AmCls
	:members:
	:undoc-members:
	:noindex: