Angle
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:BB:IMPairment:IQOutput<CH>:QUADrature:[ANGLe]

.. code-block:: python

	[SOURce]:BB:IMPairment:IQOutput<CH>:QUADrature:[ANGLe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.IqOutput.Quadrature.Angle.AngleCls
	:members:
	:undoc-members:
	:noindex: