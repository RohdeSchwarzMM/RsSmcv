Special
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[SPECial]:GOLDcode

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[SPECial]:GOLDcode



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_Special_DslPrbs.rst
	Source_Bb_Dvbs2_Special_Scramble.rst
	Source_Bb_Dvbs2_Special_Setting.rst