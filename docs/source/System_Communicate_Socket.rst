Socket
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:SOCKet:PORT
	single: SYSTem:COMMunicate:SOCKet:RESource

.. code-block:: python

	SYSTem:COMMunicate:SOCKet:PORT
	SYSTem:COMMunicate:SOCKet:RESource



.. autoclass:: RsSmcv.Implementations.System.Communicate.Socket.SocketCls
	:members:
	:undoc-members:
	:noindex: