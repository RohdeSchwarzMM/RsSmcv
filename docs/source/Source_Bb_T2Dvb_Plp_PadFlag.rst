PadFlag
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:PADFlag

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:PADFlag



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.PadFlag.PadFlagCls
	:members:
	:undoc-members:
	:noindex: