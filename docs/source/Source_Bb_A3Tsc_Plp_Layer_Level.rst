Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:LAYer:LEVel

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:LAYer:LEVel



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Layer.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: