Fft
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:FFT:MODE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:FFT:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Fft.FftCls
	:members:
	:undoc-members:
	:noindex: