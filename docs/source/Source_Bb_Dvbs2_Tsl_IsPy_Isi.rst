Isi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:ISI

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:ISI



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.IsPy.Isi.IsiCls
	:members:
	:undoc-members:
	:noindex: