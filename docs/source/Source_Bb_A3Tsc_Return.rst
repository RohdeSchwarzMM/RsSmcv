Return
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:RETurn:[CHANnel]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:RETurn:[CHANnel]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Return.ReturnCls
	:members:
	:undoc-members:
	:noindex: