FecType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:FECType

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:FECType



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.FecType.FecTypeCls
	:members:
	:undoc-members:
	:noindex: