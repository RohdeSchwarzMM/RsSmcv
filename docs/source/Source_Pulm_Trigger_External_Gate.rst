Gate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:PULM:TRIGger:EXTernal:GATE:POLarity

.. code-block:: python

	[SOURce<HW>]:PULM:TRIGger:EXTernal:GATE:POLarity



.. autoclass:: RsSmcv.Implementations.Source.Pulm.Trigger.External.Gate.GateCls
	:members:
	:undoc-members:
	:noindex: