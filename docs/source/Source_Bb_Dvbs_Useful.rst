Useful
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs.Useful.UsefulCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs.useful.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs_Useful_Rate.rst