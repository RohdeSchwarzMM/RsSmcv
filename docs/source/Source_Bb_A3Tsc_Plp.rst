Plp<PhysicalLayerPipe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.bb.a3Tsc.plp.repcap_physicalLayerPipe_get()
	driver.source.bb.a3Tsc.plp.repcap_physicalLayerPipe_set(repcap.PhysicalLayerPipe.Nr1)





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.PlpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.plp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Plp_AlpType.rst
	Source_Bb_A3Tsc_Plp_BbfCounter.rst
	Source_Bb_A3Tsc_Plp_BbfPadding.rst
	Source_Bb_A3Tsc_Plp_Constel.rst
	Source_Bb_A3Tsc_Plp_FecType.rst
	Source_Bb_A3Tsc_Plp_Id.rst
	Source_Bb_A3Tsc_Plp_InputPy.rst
	Source_Bb_A3Tsc_Plp_Layer.rst
	Source_Bb_A3Tsc_Plp_Lls.rst
	Source_Bb_A3Tsc_Plp_PacketLength.rst
	Source_Bb_A3Tsc_Plp_Rate.rst
	Source_Bb_A3Tsc_Plp_Scrambler.rst
	Source_Bb_A3Tsc_Plp_Size.rst
	Source_Bb_A3Tsc_Plp_Til.rst
	Source_Bb_A3Tsc_Plp_TypePy.rst
	Source_Bb_A3Tsc_Plp_Useful.rst