Npd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:NPD

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:NPD



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Npd.NpdCls
	:members:
	:undoc-members:
	:noindex: