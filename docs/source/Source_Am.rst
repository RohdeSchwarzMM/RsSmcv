Am
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AM:SENSitivity

.. code-block:: python

	[SOURce<HW>]:AM:SENSitivity



.. autoclass:: RsSmcv.Implementations.Source.Am.AmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.am.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Am_Bband.rst
	Source_Am_External.rst