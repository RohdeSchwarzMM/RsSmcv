Level
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:WSEGment:CONFigure:LEVel:[MODE]

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:WSEGment:CONFigure:LEVel:[MODE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Wsegment.Configure.Level.LevelCls
	:members:
	:undoc-members:
	:noindex: