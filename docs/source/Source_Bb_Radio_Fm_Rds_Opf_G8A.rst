G8A
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G8A:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G8A:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G8A:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G8A:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G8A:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G8A:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G8A.G8ACls
	:members:
	:undoc-members:
	:noindex: