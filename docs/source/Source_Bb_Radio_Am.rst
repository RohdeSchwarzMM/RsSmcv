Am
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:AM:DEPTh
	single: [SOURce<HW>]:BB:RADio:AM:INPut
	single: [SOURce<HW>]:BB:RADio:AM:PRESet
	single: [SOURce<HW>]:BB:RADio:AM:SOURce
	single: [SOURce<HW>]:BB:RADio:AM:STATe

.. code-block:: python

	[SOURce<HW>]:BB:RADio:AM:DEPTh
	[SOURce<HW>]:BB:RADio:AM:INPut
	[SOURce<HW>]:BB:RADio:AM:PRESet
	[SOURce<HW>]:BB:RADio:AM:SOURce
	[SOURce<HW>]:BB:RADio:AM:STATe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Am.AmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.am.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Am_ApLayer.rst
	Source_Bb_Radio_Am_AudGen.rst
	Source_Bb_Radio_Am_Audio.rst
	Source_Bb_Radio_Am_Modulation.rst
	Source_Bb_Radio_Am_Setting.rst