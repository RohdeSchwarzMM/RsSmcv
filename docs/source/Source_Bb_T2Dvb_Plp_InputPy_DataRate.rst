DataRate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:[INPut]:DATarate

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:[INPut]:DATarate



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.InputPy.DataRate.DataRateCls
	:members:
	:undoc-members:
	:noindex: