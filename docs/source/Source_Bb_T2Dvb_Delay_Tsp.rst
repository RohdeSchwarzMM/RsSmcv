Tsp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:DELay:TSP:DATE
	single: [SOURce<HW>]:BB:T2DVb:DELay:TSP:MODE
	single: [SOURce<HW>]:BB:T2DVb:DELay:TSP:OFFSet
	single: [SOURce<HW>]:BB:T2DVb:DELay:TSP:SEConds
	single: [SOURce<HW>]:BB:T2DVb:DELay:TSP:TIME

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:DELay:TSP:DATE
	[SOURce<HW>]:BB:T2DVb:DELay:TSP:MODE
	[SOURce<HW>]:BB:T2DVb:DELay:TSP:OFFSet
	[SOURce<HW>]:BB:T2DVb:DELay:TSP:SEConds
	[SOURce<HW>]:BB:T2DVb:DELay:TSP:TIME



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Delay.Tsp.TspCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.delay.tsp.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Delay_Tsp_Update.rst