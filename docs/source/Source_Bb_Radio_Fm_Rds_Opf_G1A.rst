G1A
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G1A:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G1A:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G1A:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G1A:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G1A:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G1A:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G1A.G1ACls
	:members:
	:undoc-members:
	:noindex: