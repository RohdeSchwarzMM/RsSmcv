Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ATSM:USEFul:[RATE]:MAX
	single: [SOURce<HW>]:BB:ATSM:USEFul:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:USEFul:[RATE]:MAX
	[SOURce<HW>]:BB:ATSM:USEFul:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: