TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: