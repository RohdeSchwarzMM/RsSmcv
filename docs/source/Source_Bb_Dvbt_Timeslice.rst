Timeslice
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:TIMeslice:LOW
	single: [SOURce<HW>]:BB:DVBT:TIMeslice:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:TIMeslice:LOW
	[SOURce<HW>]:BB:DVBT:TIMeslice:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Timeslice.TimesliceCls
	:members:
	:undoc-members:
	:noindex: