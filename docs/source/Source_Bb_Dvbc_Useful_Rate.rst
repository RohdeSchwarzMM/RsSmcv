Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBC:USEFul:[RATE]:MAX
	single: [SOURce<HW>]:BB:DVBC:USEFul:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:DVBC:USEFul:[RATE]:MAX
	[SOURce<HW>]:BB:DVBC:USEFul:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbc.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: