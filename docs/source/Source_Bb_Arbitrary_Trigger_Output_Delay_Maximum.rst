Maximum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:DELay:MAXimum

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:DELay:MAXimum



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Trigger.Output.Delay.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: