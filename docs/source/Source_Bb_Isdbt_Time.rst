Time
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Time.TimeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.time.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Isdbt_Time_Interleaving.rst