Fm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:INPut
	single: [SOURce<HW>]:BB:RADio:FM:MODE
	single: [SOURce<HW>]:BB:RADio:FM:PRESet
	single: [SOURce<HW>]:BB:RADio:FM:STATe

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:INPut
	[SOURce<HW>]:BB:RADio:FM:MODE
	[SOURce<HW>]:BB:RADio:FM:PRESet
	[SOURce<HW>]:BB:RADio:FM:STATe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.FmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_ApLayer.rst
	Source_Bb_Radio_Fm_AudGen.rst
	Source_Bb_Radio_Fm_Audio.rst
	Source_Bb_Radio_Fm_Darc.rst
	Source_Bb_Radio_Fm_Pilot.rst
	Source_Bb_Radio_Fm_Rds.rst
	Source_Bb_Radio_Fm_Setting.rst
	Source_Bb_Radio_Fm_Special.rst