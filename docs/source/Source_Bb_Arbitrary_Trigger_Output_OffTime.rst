OffTime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:OFFTime

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:OFFTime



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Trigger.Output.OffTime.OffTimeCls
	:members:
	:undoc-members:
	:noindex: