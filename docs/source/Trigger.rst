Trigger<InputIx>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.trigger.repcap_inputIx_get()
	driver.trigger.repcap_inputIx_set(repcap.InputIx.Nr1)





.. autoclass:: RsSmcv.Implementations.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_FreqSweep.rst
	Trigger_Psweep.rst
	Trigger_Sweep.rst