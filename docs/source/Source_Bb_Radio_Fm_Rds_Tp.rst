Tp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TP:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:TP:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Tp.TpCls
	:members:
	:undoc-members:
	:noindex: