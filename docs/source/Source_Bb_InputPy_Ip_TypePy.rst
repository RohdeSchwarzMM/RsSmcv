TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: