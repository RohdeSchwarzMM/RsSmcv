Pulm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:PULM:DELay
	single: [SOURce<HW>]:PULM:POLarity
	single: [SOURce<HW>]:PULM:SOURce
	single: [SOURce<HW>]:PULM:STATe

.. code-block:: python

	[SOURce<HW>]:PULM:DELay
	[SOURce<HW>]:PULM:POLarity
	[SOURce<HW>]:PULM:SOURce
	[SOURce<HW>]:PULM:STATe



.. autoclass:: RsSmcv.Implementations.Source.Pulm.PulmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.pulm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Pulm_Double.rst
	Source_Pulm_Trigger.rst