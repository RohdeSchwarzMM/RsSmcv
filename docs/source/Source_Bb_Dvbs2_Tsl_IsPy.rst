IsPy<InputStream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.bb.dvbs2.tsl.isPy.repcap_inputStream_get()
	driver.source.bb.dvbs2.tsl.isPy.repcap_inputStream_set(repcap.InputStream.Nr1)





.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.IsPy.IsPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.tsl.isPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_Tsl_IsPy_FecFrame.rst
	Source_Bb_Dvbs2_Tsl_IsPy_Isi.rst
	Source_Bb_Dvbs2_Tsl_IsPy_ModCod.rst
	Source_Bb_Dvbs2_Tsl_IsPy_Nsym.rst
	Source_Bb_Dvbs2_Tsl_IsPy_Pilots.rst
	Source_Bb_Dvbs2_Tsl_IsPy_Tsn.rst