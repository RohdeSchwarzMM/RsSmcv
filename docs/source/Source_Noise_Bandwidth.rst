Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:NOISe:BWIDth:STATe

.. code-block:: python

	[SOURce<HW>]:NOISe:BWIDth:STATe



.. autoclass:: RsSmcv.Implementations.Source.Noise.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: