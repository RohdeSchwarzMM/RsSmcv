First
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:SBS:FIRSt

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:SBS:FIRSt



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Sbs.First.FirstCls
	:members:
	:undoc-members:
	:noindex: