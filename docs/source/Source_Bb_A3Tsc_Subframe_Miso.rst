Miso
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:MISO

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:MISO



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Miso.MisoCls
	:members:
	:undoc-members:
	:noindex: