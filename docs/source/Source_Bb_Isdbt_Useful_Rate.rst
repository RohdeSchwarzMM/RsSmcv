Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:A
	single: [SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:B
	single: [SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:C

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:A
	[SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:B
	[SOURce<HW>]:BB:ISDBt:USEFul:[RATE]:C



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.useful.rate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Isdbt_Useful_Rate_Max.rst