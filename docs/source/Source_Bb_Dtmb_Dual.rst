Dual
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DTMB:DUAL:PILot

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:DUAL:PILot



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Dual.DualCls
	:members:
	:undoc-members:
	:noindex: