Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DTMB:TIME:[INTerleaver]

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:TIME:[INTerleaver]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: