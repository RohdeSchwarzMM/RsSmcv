Setting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:[SPECial]:SETTing:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:[SPECial]:SETTing:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Special.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: