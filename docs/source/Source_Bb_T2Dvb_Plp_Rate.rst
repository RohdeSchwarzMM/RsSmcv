Rate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:RATE

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:RATE



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: