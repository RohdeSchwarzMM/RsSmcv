Txid
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:TXId:ADDRess
	single: [SOURce<HW>]:BB:A3TSc:TXId:LEVel
	single: [SOURce<HW>]:BB:A3TSc:TXId:MODE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:TXId:ADDRess
	[SOURce<HW>]:BB:A3TSc:TXId:LEVel
	[SOURce<HW>]:BB:A3TSc:TXId:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Txid.TxidCls
	:members:
	:undoc-members:
	:noindex: