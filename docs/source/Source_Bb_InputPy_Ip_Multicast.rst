Multicast
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Multicast.MulticastCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.inputPy.ip.multicast.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_InputPy_Ip_Multicast_Address.rst