Interval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:GUARd:INTerval

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:GUARd:INTerval



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Guard.Interval.IntervalCls
	:members:
	:undoc-members:
	:noindex: