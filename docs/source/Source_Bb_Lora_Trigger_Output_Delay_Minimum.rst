Minimum
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:DELay:MINimum

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:DELay:MINimum



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.Output.Delay.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: