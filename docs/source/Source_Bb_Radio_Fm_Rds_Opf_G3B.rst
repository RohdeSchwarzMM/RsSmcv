G3B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G3B:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G3B:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G3B:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G3B:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G3B:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G3B:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G3B.G3BCls
	:members:
	:undoc-members:
	:noindex: