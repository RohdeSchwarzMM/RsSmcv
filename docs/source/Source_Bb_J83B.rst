J83B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:J83B:CONStel
	single: [SOURce<HW>]:BB:J83B:PACKetlength
	single: [SOURce<HW>]:BB:J83B:PAYLoad
	single: [SOURce<HW>]:BB:J83B:PID
	single: [SOURce<HW>]:BB:J83B:PIDTestpack
	single: [SOURce<HW>]:BB:J83B:PRBS
	single: [SOURce<HW>]:BB:J83B:PRESet
	single: [SOURce<HW>]:BB:J83B:ROLLoff
	single: [SOURce<HW>]:BB:J83B:SOURce
	single: [SOURce<HW>]:BB:J83B:STATe
	single: [SOURce<HW>]:BB:J83B:STUFfing
	single: [SOURce<HW>]:BB:J83B:SYMBols
	single: [SOURce<HW>]:BB:J83B:TESTsignal
	single: [SOURce<HW>]:BB:J83B:TSPacket

.. code-block:: python

	[SOURce<HW>]:BB:J83B:CONStel
	[SOURce<HW>]:BB:J83B:PACKetlength
	[SOURce<HW>]:BB:J83B:PAYLoad
	[SOURce<HW>]:BB:J83B:PID
	[SOURce<HW>]:BB:J83B:PIDTestpack
	[SOURce<HW>]:BB:J83B:PRBS
	[SOURce<HW>]:BB:J83B:PRESet
	[SOURce<HW>]:BB:J83B:ROLLoff
	[SOURce<HW>]:BB:J83B:SOURce
	[SOURce<HW>]:BB:J83B:STATe
	[SOURce<HW>]:BB:J83B:STUFfing
	[SOURce<HW>]:BB:J83B:SYMBols
	[SOURce<HW>]:BB:J83B:TESTsignal
	[SOURce<HW>]:BB:J83B:TSPacket



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.J83BCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.j83B.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_J83B_InputPy.rst
	Source_Bb_J83B_Interleaver.rst
	Source_Bb_J83B_Setting.rst
	Source_Bb_J83B_Special.rst
	Source_Bb_J83B_Useful.rst