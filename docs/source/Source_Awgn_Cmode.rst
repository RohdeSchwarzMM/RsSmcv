Cmode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:AWGN:CMODe:[STATe]

.. code-block:: python

	[SOURce<HW>]:AWGN:CMODe:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Awgn.Cmode.CmodeCls
	:members:
	:undoc-members:
	:noindex: