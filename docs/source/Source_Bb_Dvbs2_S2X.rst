S2X
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:S2X:CHB
	single: [SOURce<HW>]:BB:DVBS2:S2X:SF
	single: [SOURce<HW>]:BB:DVBS2:S2X

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:S2X:CHB
	[SOURce<HW>]:BB:DVBS2:S2X:SF
	[SOURce<HW>]:BB:DVBS2:S2X



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.S2X.S2XCls
	:members:
	:undoc-members:
	:noindex: