Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:SETTing:CATalog
	single: [SOURce<HW>]:BB:DVBT:SETTing:DELete
	single: [SOURce<HW>]:BB:DVBT:SETTing:LOAD
	single: [SOURce<HW>]:BB:DVBT:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:SETTing:CATalog
	[SOURce<HW>]:BB:DVBT:SETTing:DELete
	[SOURce<HW>]:BB:DVBT:SETTing:LOAD
	[SOURce<HW>]:BB:DVBT:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: