Configuration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: DIAGnostic<HW>:POINt:CONFiguration

.. code-block:: python

	DIAGnostic<HW>:POINt:CONFiguration



.. autoclass:: RsSmcv.Implementations.Diagnostic.Point.Configuration.ConfigurationCls
	:members:
	:undoc-members:
	:noindex: