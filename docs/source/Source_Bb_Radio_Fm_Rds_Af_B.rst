B
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.B.BCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.b.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Af_B_List1.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List2.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List3.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List4.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List5.rst