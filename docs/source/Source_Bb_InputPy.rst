InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:INPut:FORMat
	single: [SOURce<HW>]:BB:INPut:TSCHannel
	single: [SOURce<HW>]:BB:INPut

.. code-block:: python

	[SOURce<HW>]:BB:INPut:FORMat
	[SOURce<HW>]:BB:INPut:TSCHannel
	[SOURce<HW>]:BB:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_InputPy_Ip.rst