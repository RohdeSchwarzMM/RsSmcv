Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:RATE:A
	single: [SOURce<HW>]:BB:ISDBt:RATE:B
	single: [SOURce<HW>]:BB:ISDBt:RATE:C

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:RATE:A
	[SOURce<HW>]:BB:ISDBt:RATE:B
	[SOURce<HW>]:BB:ISDBt:RATE:C



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: