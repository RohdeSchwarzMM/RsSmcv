Settings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:[SPECial]:SETTings:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:[SPECial]:SETTings:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Special.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex: