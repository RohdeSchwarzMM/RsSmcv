Impairment
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:IMPairment:DELay
	single: [SOURce<HW>]:BB:IMPairment:STATe

.. code-block:: python

	[SOURce<HW>]:BB:IMPairment:DELay
	[SOURce<HW>]:BB:IMPairment:STATe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.ImpairmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.impairment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Impairment_IqOutput.rst
	Source_Bb_Impairment_IqRatio.rst
	Source_Bb_Impairment_Leakage.rst
	Source_Bb_Impairment_Optimization.rst
	Source_Bb_Impairment_Quadrature.rst