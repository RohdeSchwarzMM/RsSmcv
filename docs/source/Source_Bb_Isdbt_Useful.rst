Useful
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Useful.UsefulCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.useful.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Isdbt_Useful_Rate.rst