Fil
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:FIL

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:FIL



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Fil.FilCls
	:members:
	:undoc-members:
	:noindex: