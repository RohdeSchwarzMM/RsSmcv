Ndata
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:NDATa

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:NDATa



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Ndata.NdataCls
	:members:
	:undoc-members:
	:noindex: