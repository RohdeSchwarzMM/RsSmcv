TestSignal
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:[SPECial]:TESTsignal:SCID
	single: [SOURce<HW>]:BB:TDMB:[SPECial]:TESTsignal:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:[SPECial]:TESTsignal:SCID
	[SOURce<HW>]:BB:TDMB:[SPECial]:TESTsignal:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Special.TestSignal.TestSignalCls
	:members:
	:undoc-members:
	:noindex: