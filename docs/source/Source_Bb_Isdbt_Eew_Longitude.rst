Longitude<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.bb.isdbt.eew.longitude.repcap_index_get()
	driver.source.bb.isdbt.eew.longitude.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:EEW:LONGitude<CH>

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:EEW:LONGitude<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Eew.Longitude.LongitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.eew.longitude.clone()