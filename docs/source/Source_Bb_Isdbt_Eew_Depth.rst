Depth<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.bb.isdbt.eew.depth.repcap_index_get()
	driver.source.bb.isdbt.eew.depth.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:EEW:DEPTh<CH>

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:EEW:DEPTh<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Eew.Depth.DepthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.eew.depth.clone()