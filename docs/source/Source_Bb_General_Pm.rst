Pm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:GENeral:PM:DEViation
	single: [SOURce<HW>]:BB:GENeral:PM:FREQuency
	single: [SOURce<HW>]:BB:GENeral:PM:PERiod
	single: [SOURce<HW>]:BB:GENeral:PM:SHAPe
	single: [SOURce<HW>]:BB:GENeral:PM:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:PM:DEViation
	[SOURce<HW>]:BB:GENeral:PM:FREQuency
	[SOURce<HW>]:BB:GENeral:PM:PERiod
	[SOURce<HW>]:BB:GENeral:PM:SHAPe
	[SOURce<HW>]:BB:GENeral:PM:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Pm.PmCls
	:members:
	:undoc-members:
	:noindex: