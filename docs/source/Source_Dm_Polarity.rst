Polarity<Channel>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.dm.polarity.repcap_channel_get()
	driver.source.dm.polarity.repcap_channel_set(repcap.Channel.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:DM:POLarity<CH>

.. code-block:: python

	[SOURce<HW>]:DM:POLarity<CH>



.. autoclass:: RsSmcv.Implementations.Source.Dm.Polarity.PolarityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.dm.polarity.clone()