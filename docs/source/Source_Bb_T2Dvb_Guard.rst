Guard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:GUARd:INTerval

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:GUARd:INTerval



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Guard.GuardCls
	:members:
	:undoc-members:
	:noindex: