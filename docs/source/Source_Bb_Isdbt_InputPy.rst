InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:INPut:FORMat
	single: [SOURce<HW>]:BB:ISDBt:INPut:TSCHannel
	single: [SOURce<HW>]:BB:ISDBt:[INPut]:DATarate
	single: [SOURce<HW>]:BB:ISDBt:INPut

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:INPut:FORMat
	[SOURce<HW>]:BB:ISDBt:INPut:TSCHannel
	[SOURce<HW>]:BB:ISDBt:[INPut]:DATarate
	[SOURce<HW>]:BB:ISDBt:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: