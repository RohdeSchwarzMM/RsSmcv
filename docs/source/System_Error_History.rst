History
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:ERRor:HISTory:CLEar
	single: SYSTem:ERRor:HISTory

.. code-block:: python

	SYSTem:ERRor:HISTory:CLEar
	SYSTem:ERRor:HISTory



.. autoclass:: RsSmcv.Implementations.System.Error.History.HistoryCls
	:members:
	:undoc-members:
	:noindex: