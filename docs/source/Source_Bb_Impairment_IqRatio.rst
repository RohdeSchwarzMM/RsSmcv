IqRatio
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:IMPairment:IQRatio:[MAGNitude]

.. code-block:: python

	[SOURce<HW>]:BB:IMPairment:IQRatio:[MAGNitude]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.IqRatio.IqRatioCls
	:members:
	:undoc-members:
	:noindex: