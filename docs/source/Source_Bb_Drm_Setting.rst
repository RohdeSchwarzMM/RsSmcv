Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DRM:SETTing:CATalog
	single: [SOURce<HW>]:BB:DRM:SETTing:DELete
	single: [SOURce<HW>]:BB:DRM:SETTing:LOAD
	single: [SOURce<HW>]:BB:DRM:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DRM:SETTing:CATalog
	[SOURce<HW>]:BB:DRM:SETTing:DELete
	[SOURce<HW>]:BB:DRM:SETTing:LOAD
	[SOURce<HW>]:BB:DRM:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Drm.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: