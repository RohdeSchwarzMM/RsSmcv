Quadrature
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:IMPairment:QUADrature:[ANGLe]

.. code-block:: python

	[SOURce<HW>]:BB:IMPairment:QUADrature:[ANGLe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.Quadrature.QuadratureCls
	:members:
	:undoc-members:
	:noindex: