TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TYPE:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TYPE:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.TypePy.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: