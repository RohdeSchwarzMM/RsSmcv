Carrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:L:CARRier:MODE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:L:CARRier:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Lpy.Carrier.CarrierCls
	:members:
	:undoc-members:
	:noindex: