Eew
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:EEW:AREAinfo
	single: [SOURce<HW>]:BB:ISDBt:EEW:EEW
	single: [SOURce<HW>]:BB:ISDBt:EEW:NUMepicenter
	single: [SOURce<HW>]:BB:ISDBt:EEW:SIGNaltype

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:EEW:AREAinfo
	[SOURce<HW>]:BB:ISDBt:EEW:EEW
	[SOURce<HW>]:BB:ISDBt:EEW:NUMepicenter
	[SOURce<HW>]:BB:ISDBt:EEW:SIGNaltype



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Eew.EewCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.isdbt.eew.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Isdbt_Eew_Apai.rst
	Source_Bb_Isdbt_Eew_Ape1.rst
	Source_Bb_Isdbt_Eew_Ape2.rst
	Source_Bb_Isdbt_Eew_Depth.rst
	Source_Bb_Isdbt_Eew_InfoType.rst
	Source_Bb_Isdbt_Eew_Latitude.rst
	Source_Bb_Isdbt_Eew_Longitude.rst
	Source_Bb_Isdbt_Eew_Occurence.rst
	Source_Bb_Isdbt_Eew_WarnId.rst