Pilots
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:PILots

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:PILots



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.IsPy.Pilots.PilotsCls
	:members:
	:undoc-members:
	:noindex: