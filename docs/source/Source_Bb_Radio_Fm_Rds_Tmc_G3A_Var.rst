Var<GroupTypeVariant>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.source.bb.radio.fm.rds.tmc.g3A.var.repcap_groupTypeVariant_get()
	driver.source.bb.radio.fm.rds.tmc.g3A.var.repcap_groupTypeVariant_set(repcap.GroupTypeVariant.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:G3A:VAR<CH>

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:G3A:VAR<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Tmc.G3A.Var.VarCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.tmc.g3A.var.clone()