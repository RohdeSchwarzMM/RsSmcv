Inter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:INTer

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:INTer



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Til.Inter.InterCls
	:members:
	:undoc-members:
	:noindex: