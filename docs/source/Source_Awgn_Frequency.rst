Frequency
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AWGN:FREQuency:RESult
	single: [SOURce<HW>]:AWGN:FREQuency:TARGet

.. code-block:: python

	[SOURce<HW>]:AWGN:FREQuency:RESult
	[SOURce<HW>]:AWGN:FREQuency:TARGet



.. autoclass:: RsSmcv.Implementations.Source.Awgn.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: