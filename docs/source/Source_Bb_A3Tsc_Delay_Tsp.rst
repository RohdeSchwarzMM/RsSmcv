Tsp
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:DELay:TSP:LTT
	single: [SOURce<HW>]:BB:A3TSc:DELay:TSP:LTU
	single: [SOURce<HW>]:BB:A3TSc:DELay:TSP:TOET
	single: [SOURce<HW>]:BB:A3TSc:DELay:TSP:UTO

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:DELay:TSP:LTT
	[SOURce<HW>]:BB:A3TSc:DELay:TSP:LTU
	[SOURce<HW>]:BB:A3TSc:DELay:TSP:TOET
	[SOURce<HW>]:BB:A3TSc:DELay:TSP:UTO



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Delay.Tsp.TspCls
	:members:
	:undoc-members:
	:noindex: