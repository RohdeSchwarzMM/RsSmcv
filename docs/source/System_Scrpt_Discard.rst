Discard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SCRPt:DISCard

.. code-block:: python

	SYSTem:SCRPt:DISCard



.. autoclass:: RsSmcv.Implementations.System.Scrpt.Discard.DiscardCls
	:members:
	:undoc-members:
	:noindex: