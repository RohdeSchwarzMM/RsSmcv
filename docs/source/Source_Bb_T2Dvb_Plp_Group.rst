Group
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:GROup

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:GROup



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Group.GroupCls
	:members:
	:undoc-members:
	:noindex: