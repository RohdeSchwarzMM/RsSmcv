Ip<IpVersion>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr4 .. Nr6
	rc = driver.source.bb.inputPy.ip.repcap_ipVersion_get()
	driver.source.bb.inputPy.ip.repcap_ipVersion_set(repcap.IpVersion.Nr4)





.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.IpCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.inputPy.ip.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_InputPy_Ip_Alias.rst
	Source_Bb_InputPy_Ip_Igmp.rst
	Source_Bb_InputPy_Ip_Multicast.rst
	Source_Bb_InputPy_Ip_Port.rst
	Source_Bb_InputPy_Ip_State.rst
	Source_Bb_InputPy_Ip_TypePy.rst