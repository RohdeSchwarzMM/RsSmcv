Guard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:GUARd:INTerval

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:GUARd:INTerval



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Guard.GuardCls
	:members:
	:undoc-members:
	:noindex: