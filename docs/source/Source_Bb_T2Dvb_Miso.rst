Miso
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:MISO:MODE
	single: [SOURce<HW>]:BB:T2DVb:MISO:[GROup]

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:MISO:MODE
	[SOURce<HW>]:BB:T2DVb:MISO:[GROup]



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Miso.MisoCls
	:members:
	:undoc-members:
	:noindex: