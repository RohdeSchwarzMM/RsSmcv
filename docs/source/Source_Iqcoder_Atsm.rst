Atsm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:[IQCoder]:ATSM:INPut

.. code-block:: python

	[SOURce]:[IQCoder]:ATSM:INPut



.. autoclass:: RsSmcv.Implementations.Source.Iqcoder.Atsm.AtsmCls
	:members:
	:undoc-members:
	:noindex: