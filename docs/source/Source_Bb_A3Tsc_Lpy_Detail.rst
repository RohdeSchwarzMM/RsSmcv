Detail
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:L:DETail:FECType
	single: [SOURce<HW>]:BB:A3TSc:L:DETail:VERSion

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:L:DETail:FECType
	[SOURce<HW>]:BB:A3TSc:L:DETail:VERSion



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Lpy.Detail.DetailCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.lpy.detail.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Lpy_Detail_Additional.rst