Source
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:SOURce:LOW
	single: [SOURce<HW>]:BB:DVBT:SOURce:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:SOURce:LOW
	[SOURce<HW>]:BB:DVBT:SOURce:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: