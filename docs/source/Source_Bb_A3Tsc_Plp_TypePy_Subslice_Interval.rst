Interval
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TYPE:SUBSlice:[INTerval]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TYPE:SUBSlice:[INTerval]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.TypePy.Subslice.Interval.IntervalCls
	:members:
	:undoc-members:
	:noindex: