Path
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:BB:PATH:COUNt

.. code-block:: python

	[SOURce]:BB:PATH:COUNt



.. autoclass:: RsSmcv.Implementations.Source.Bb.Path.PathCls
	:members:
	:undoc-members:
	:noindex: