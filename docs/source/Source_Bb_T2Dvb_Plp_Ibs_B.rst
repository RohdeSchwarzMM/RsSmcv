B
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:IBS:B

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:IBS:B



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Ibs.B.BCls
	:members:
	:undoc-members:
	:noindex: