List3
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST3:NUMBer
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST3:TFRequency

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST3:NUMBer
	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST3:TFRequency



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.B.List3.List3Cls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.b.list3.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Af_B_List3_Desc.rst
	Source_Bb_Radio_Fm_Rds_Af_B_List3_Frequency.rst