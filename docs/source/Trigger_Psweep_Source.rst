Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger<HW>:PSWeep:SOURce

.. code-block:: python

	TRIGger<HW>:PSWeep:SOURce



.. autoclass:: RsSmcv.Implementations.Trigger.Psweep.Source.SourceCls
	:members:
	:undoc-members:
	:noindex: