Dvbs2
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:ANNM
	single: [SOURce<HW>]:BB:DVBS2:NTSL
	single: [SOURce<HW>]:BB:DVBS2:PAYLoad
	single: [SOURce<HW>]:BB:DVBS2:PID
	single: [SOURce<HW>]:BB:DVBS2:PIDTestpack
	single: [SOURce<HW>]:BB:DVBS2:PRESet
	single: [SOURce<HW>]:BB:DVBS2:ROLLoff
	single: [SOURce<HW>]:BB:DVBS2:STATe
	single: [SOURce<HW>]:BB:DVBS2:TSPacket

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:ANNM
	[SOURce<HW>]:BB:DVBS2:NTSL
	[SOURce<HW>]:BB:DVBS2:PAYLoad
	[SOURce<HW>]:BB:DVBS2:PID
	[SOURce<HW>]:BB:DVBS2:PIDTestpack
	[SOURce<HW>]:BB:DVBS2:PRESet
	[SOURce<HW>]:BB:DVBS2:ROLLoff
	[SOURce<HW>]:BB:DVBS2:STATe
	[SOURce<HW>]:BB:DVBS2:TSPacket



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Dvbs2Cls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_InputPy.rst
	Source_Bb_Dvbs2_IsPy.rst
	Source_Bb_Dvbs2_Prbs.rst
	Source_Bb_Dvbs2_S2X.rst
	Source_Bb_Dvbs2_Setting.rst
	Source_Bb_Dvbs2_Source.rst
	Source_Bb_Dvbs2_Special.rst
	Source_Bb_Dvbs2_Symbols.rst
	Source_Bb_Dvbs2_Tsl.rst