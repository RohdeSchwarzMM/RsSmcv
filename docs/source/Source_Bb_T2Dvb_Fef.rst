Fef
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:FEF:INTerval
	single: [SOURce<HW>]:BB:T2DVb:FEF:LENGth
	single: [SOURce<HW>]:BB:T2DVb:FEF:PAYLoad
	single: [SOURce<HW>]:BB:T2DVb:FEF:TYPE
	single: [SOURce<HW>]:BB:T2DVb:FEF

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:FEF:INTerval
	[SOURce<HW>]:BB:T2DVb:FEF:LENGth
	[SOURce<HW>]:BB:T2DVb:FEF:PAYLoad
	[SOURce<HW>]:BB:T2DVb:FEF:TYPE
	[SOURce<HW>]:BB:T2DVb:FEF



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Fef.FefCls
	:members:
	:undoc-members:
	:noindex: