Max
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:USEFul:[RATE]:MAX:LOW
	single: [SOURce<HW>]:BB:DVBT:USEFul:[RATE]:MAX:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:USEFul:[RATE]:MAX:LOW
	[SOURce<HW>]:BB:DVBT:USEFul:[RATE]:MAX:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Useful.Rate.Max.MaxCls
	:members:
	:undoc-members:
	:noindex: