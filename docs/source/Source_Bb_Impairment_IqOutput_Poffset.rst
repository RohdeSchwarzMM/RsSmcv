Poffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:BB:IMPairment:IQOutput<CH>:POFFset

.. code-block:: python

	[SOURce]:BB:IMPairment:IQOutput<CH>:POFFset



.. autoclass:: RsSmcv.Implementations.Source.Bb.Impairment.IqOutput.Poffset.PoffsetCls
	:members:
	:undoc-members:
	:noindex: