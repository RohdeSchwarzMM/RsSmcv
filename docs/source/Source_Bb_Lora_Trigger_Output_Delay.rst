Delay
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:DELay
	single: [SOURce<HW>]:BB:LORA:TRIGger:OUTPut:DELay:FIXed

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:DELay
	[SOURce<HW>]:BB:LORA:TRIGger:OUTPut:DELay:FIXed



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.Output.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.lora.trigger.output.delay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Lora_Trigger_Output_Delay_Maximum.rst
	Source_Bb_Lora_Trigger_Output_Delay_Minimum.rst