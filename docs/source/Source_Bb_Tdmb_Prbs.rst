Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:TDMB:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: