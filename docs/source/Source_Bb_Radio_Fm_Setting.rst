Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:SETTing:CATalog
	single: [SOURce<HW>]:BB:RADio:FM:SETTing:DELete
	single: [SOURce<HW>]:BB:RADio:FM:SETTing:LOAD
	single: [SOURce<HW>]:BB:RADio:FM:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:SETTing:CATalog
	[SOURce<HW>]:BB:RADio:FM:SETTing:DELete
	[SOURce<HW>]:BB:RADio:FM:SETTing:LOAD
	[SOURce<HW>]:BB:RADio:FM:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: