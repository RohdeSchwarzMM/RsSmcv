Factory
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALibration:DATA:FACTory:DATE

.. code-block:: python

	CALibration:DATA:FACTory:DATE



.. autoclass:: RsSmcv.Implementations.Calibration.Data.Factory.FactoryCls
	:members:
	:undoc-members:
	:noindex: