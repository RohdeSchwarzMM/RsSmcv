Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: