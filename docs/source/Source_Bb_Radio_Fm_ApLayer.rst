ApLayer
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:APLayer:ATT1
	single: [SOURce<HW>]:BB:RADio:FM:APLayer:ATT2

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:APLayer:ATT1
	[SOURce<HW>]:BB:RADio:FM:APLayer:ATT2



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.ApLayer.ApLayerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.apLayer.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_ApLayer_Library.rst