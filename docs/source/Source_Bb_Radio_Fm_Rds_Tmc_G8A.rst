G8A
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:BLOCk3a
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:BLOCk4
	single: [SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:NUMBer

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:BLOCk3a
	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:BLOCk4
	[SOURce<HW>]:BB:RADio:FM:RDS:TMC:G8A:NUMBer



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Tmc.G8A.G8ACls
	:members:
	:undoc-members:
	:noindex: