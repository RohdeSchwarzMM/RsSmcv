Pilot
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Pilot.PilotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.subframe.pilot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Subframe_Pilot_Boost.rst
	Source_Bb_A3Tsc_Subframe_Pilot_Siso.rst