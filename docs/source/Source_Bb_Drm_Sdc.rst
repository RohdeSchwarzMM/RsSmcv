Sdc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DRM:SDC:CONStel

.. code-block:: python

	[SOURce<HW>]:BB:DRM:SDC:CONStel



.. autoclass:: RsSmcv.Implementations.Source.Bb.Drm.Sdc.SdcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.drm.sdc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Drm_Sdc_Level.rst
	Source_Bb_Drm_Sdc_Profile.rst
	Source_Bb_Drm_Sdc_Rate.rst