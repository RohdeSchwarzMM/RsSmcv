Subframe<Subframe>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr32
	rc = driver.source.bb.a3Tsc.subframe.repcap_subframe_get()
	driver.source.bb.a3Tsc.subframe.repcap_subframe_set(repcap.Subframe.Nr1)





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.SubframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.subframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Subframe_Carrier.rst
	Source_Bb_A3Tsc_Subframe_Duration.rst
	Source_Bb_A3Tsc_Subframe_Fft.rst
	Source_Bb_A3Tsc_Subframe_Fil.rst
	Source_Bb_A3Tsc_Subframe_Guard.rst
	Source_Bb_A3Tsc_Subframe_Mimo.rst
	Source_Bb_A3Tsc_Subframe_Miso.rst
	Source_Bb_A3Tsc_Subframe_Ndata.rst
	Source_Bb_A3Tsc_Subframe_Pilot.rst
	Source_Bb_A3Tsc_Subframe_Plp.rst
	Source_Bb_A3Tsc_Subframe_Sbs.rst
	Source_Bb_A3Tsc_Subframe_Used.rst