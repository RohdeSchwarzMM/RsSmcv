Bootstrap
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:BANDwidth
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:DURation
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:EAS
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:MAJor
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:MINor

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:BANDwidth
	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:DURation
	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:EAS
	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:MAJor
	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:MINor



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.BootstrapCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.info.bootstrap.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Info_Bootstrap_Basic.rst
	Source_Bb_A3Tsc_Info_Bootstrap_Bsr.rst
	Source_Bb_A3Tsc_Info_Bootstrap_Fft.rst
	Source_Bb_A3Tsc_Info_Bootstrap_Guard.rst
	Source_Bb_A3Tsc_Info_Bootstrap_Pilot.rst
	Source_Bb_A3Tsc_Info_Bootstrap_Preamble.rst
	Source_Bb_A3Tsc_Info_Bootstrap_Time.rst