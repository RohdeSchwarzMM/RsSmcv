Stuffing
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[IS<CH>]:STUFfing

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[IS<CH>]:STUFfing



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.IsPy.Stuffing.StuffingCls
	:members:
	:undoc-members:
	:noindex: