IpAddress
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:BCIP:NETWork:IPADdress:MODE
	single: SYSTem:COMMunicate:BCIP:NETWork:IPADdress

.. code-block:: python

	SYSTem:COMMunicate:BCIP:NETWork:IPADdress:MODE
	SYSTem:COMMunicate:BCIP:NETWork:IPADdress



.. autoclass:: RsSmcv.Implementations.System.Communicate.BcIp.Network.IpAddress.IpAddressCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.bcIp.network.ipAddress.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_BcIp_Network_IpAddress_Subnet.rst