Symbols
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:SYMBols:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:SYMBols:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Symbols.SymbolsCls
	:members:
	:undoc-members:
	:noindex: