InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBC:INPut:FORMat
	single: [SOURce<HW>]:BB:DVBC:INPut:TSCHannel
	single: [SOURce<HW>]:BB:DVBC:[INPut]:DATarate
	single: [SOURce<HW>]:BB:DVBC:INPut

.. code-block:: python

	[SOURce<HW>]:BB:DVBC:INPut:FORMat
	[SOURce<HW>]:BB:DVBC:INPut:TSCHannel
	[SOURce<HW>]:BB:DVBC:[INPut]:DATarate
	[SOURce<HW>]:BB:DVBC:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbc.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: