Network
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SYSTem:COMMunicate:BCIP:NETWork:MACaddress
	single: SYSTem:COMMunicate:BCIP:NETWork:PROTocol
	single: SYSTem:COMMunicate:BCIP:NETWork:STATus

.. code-block:: python

	SYSTem:COMMunicate:BCIP:NETWork:MACaddress
	SYSTem:COMMunicate:BCIP:NETWork:PROTocol
	SYSTem:COMMunicate:BCIP:NETWork:STATus



.. autoclass:: RsSmcv.Implementations.System.Communicate.BcIp.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.system.communicate.bcIp.network.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	System_Communicate_BcIp_Network_Common.rst
	System_Communicate_BcIp_Network_IpAddress.rst
	System_Communicate_BcIp_Network_Restart.rst