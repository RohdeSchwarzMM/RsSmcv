Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:J83B:USEFul:[RATE]:MAX
	single: [SOURce<HW>]:BB:J83B:USEFul:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:J83B:USEFul:[RATE]:MAX
	[SOURce<HW>]:BB:J83B:USEFul:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex: