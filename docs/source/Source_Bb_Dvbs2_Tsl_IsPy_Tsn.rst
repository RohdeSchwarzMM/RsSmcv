Tsn
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:TSN

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:TSL<ST>:IS<CH>:TSN



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Tsl.IsPy.Tsn.TsnCls
	:members:
	:undoc-members:
	:noindex: