MpeFec
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:MPEFec:LOW
	single: [SOURce<HW>]:BB:DVBT:MPEFec:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:MPEFec:LOW
	[SOURce<HW>]:BB:DVBT:MPEFec:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.MpeFec.MpeFecCls
	:members:
	:undoc-members:
	:noindex: