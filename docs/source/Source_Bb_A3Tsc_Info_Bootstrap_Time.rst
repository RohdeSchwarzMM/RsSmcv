Time
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:TIME:NEXT

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:TIME:NEXT



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Time.TimeCls
	:members:
	:undoc-members:
	:noindex: