Txid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:ID:TXID:AVAil

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:ID:TXID:AVAil



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Id.Txid.TxidCls
	:members:
	:undoc-members:
	:noindex: