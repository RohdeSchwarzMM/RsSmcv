Subnet
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:COMMunicate:BCIP:NETWork:IPADdress:SUBNet:MASK

.. code-block:: python

	SYSTem:COMMunicate:BCIP:NETWork:IPADdress:SUBNet:MASK



.. autoclass:: RsSmcv.Implementations.System.Communicate.BcIp.Network.IpAddress.Subnet.SubnetCls
	:members:
	:undoc-members:
	:noindex: