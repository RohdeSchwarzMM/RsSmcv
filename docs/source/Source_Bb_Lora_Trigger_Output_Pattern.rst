Pattern
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:PATTern

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:OUTPut<CH>:PATTern



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.Output.Pattern.PatternCls
	:members:
	:undoc-members:
	:noindex: