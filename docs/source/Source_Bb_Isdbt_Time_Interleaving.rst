Interleaving
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:TIME:[INTerleaving]:A
	single: [SOURce<HW>]:BB:ISDBt:TIME:[INTerleaving]:B
	single: [SOURce<HW>]:BB:ISDBt:TIME:[INTerleaving]:C

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:TIME:[INTerleaving]:A
	[SOURce<HW>]:BB:ISDBt:TIME:[INTerleaving]:B
	[SOURce<HW>]:BB:ISDBt:TIME:[INTerleaving]:C



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Time.Interleaving.InterleavingCls
	:members:
	:undoc-members:
	:noindex: