Max
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[IS<CH>]:USEFul:[RATE]:MAX

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[IS<CH>]:USEFul:[RATE]:MAX



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.IsPy.Useful.Rate.Max.MaxCls
	:members:
	:undoc-members:
	:noindex: