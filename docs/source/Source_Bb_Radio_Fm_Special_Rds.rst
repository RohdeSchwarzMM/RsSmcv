Rds
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:[SPECial]:RDS:PHASe

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:[SPECial]:RDS:PHASe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Special.Rds.RdsCls
	:members:
	:undoc-members:
	:noindex: