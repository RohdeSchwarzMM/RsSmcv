Fm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:FM:SENSitivity
	single: [SOURce<HW>]:FM:[DEViation]

.. code-block:: python

	[SOURce<HW>]:FM:SENSitivity
	[SOURce<HW>]:FM:[DEViation]



.. autoclass:: RsSmcv.Implementations.Source.Fm.FmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.fm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Fm_External.rst
	Source_Fm_Internal.rst