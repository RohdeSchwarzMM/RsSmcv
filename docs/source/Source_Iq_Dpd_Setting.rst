Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:IQ:DPD:SETTing:CATalog
	single: [SOURce<HW>]:IQ:DPD:SETTing:LOAD
	single: [SOURce<HW>]:IQ:DPD:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:IQ:DPD:SETTing:CATalog
	[SOURce<HW>]:IQ:DPD:SETTing:LOAD
	[SOURce<HW>]:IQ:DPD:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Iq.Dpd.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: