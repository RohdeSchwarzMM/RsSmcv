Trigger
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:TRIGger:RMODe

.. code-block:: python

	[SOURce<HW>]:BB:TRIGger:RMODe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Trigger.TriggerCls
	:members:
	:undoc-members:
	:noindex: