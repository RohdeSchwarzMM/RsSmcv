Read
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TSGen:READ:FMEMory
	single: TSGen:READ:ORIGtsrate

.. code-block:: python

	TSGen:READ:FMEMory
	TSGen:READ:ORIGtsrate



.. autoclass:: RsSmcv.Implementations.TsGen.Read.ReadCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.tsGen.read.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	TsGen_Read_PlayFile.rst