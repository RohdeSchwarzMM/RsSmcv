Lpy
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Lpy.LpyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.info.lpy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Info_Lpy_Basic.rst
	Source_Bb_A3Tsc_Info_Lpy_Detail.rst