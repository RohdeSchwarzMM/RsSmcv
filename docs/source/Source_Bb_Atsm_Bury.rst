Bury
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ATSM:BURY:RATio

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:BURY:RATio



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Bury.BuryCls
	:members:
	:undoc-members:
	:noindex: