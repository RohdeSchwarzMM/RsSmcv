Rate
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:USEFul:[RATE]:LOW
	single: [SOURce<HW>]:BB:DVBT:USEFul:[RATE]:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:USEFul:[RATE]:LOW
	[SOURce<HW>]:BB:DVBT:USEFul:[RATE]:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbt.useful.rate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbt_Useful_Rate_Max.rst