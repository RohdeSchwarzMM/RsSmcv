Rate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:USEFul:[RATE]

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:USEFul:[RATE]



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Useful.Rate.RateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.plp.useful.rate.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Plp_Useful_Rate_Max.rst