Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: