Ttest
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST<HW>:BBOut:TTESt:[STATe]

.. code-block:: python

	TEST<HW>:BBOut:TTESt:[STATe]



.. autoclass:: RsSmcv.Implementations.Test.BbOut.Ttest.TtestCls
	:members:
	:undoc-members:
	:noindex: