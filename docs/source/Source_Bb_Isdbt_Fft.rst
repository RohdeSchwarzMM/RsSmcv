Fft
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:FFT:MODE

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:FFT:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Fft.FftCls
	:members:
	:undoc-members:
	:noindex: