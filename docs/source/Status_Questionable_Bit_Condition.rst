Condition
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: STATus:QUEStionable:BIT<BITNR>:CONDition

.. code-block:: python

	STATus:QUEStionable:BIT<BITNR>:CONDition



.. autoclass:: RsSmcv.Implementations.Status.Questionable.Bit.Condition.ConditionCls
	:members:
	:undoc-members:
	:noindex: