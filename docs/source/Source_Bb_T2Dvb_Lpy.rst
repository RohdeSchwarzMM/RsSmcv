Lpy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:L:CONStel
	single: [SOURce<HW>]:BB:T2DVb:L:EXTension
	single: [SOURce<HW>]:BB:T2DVb:L:REPetition
	single: [SOURce<HW>]:BB:T2DVb:L:SCRambled
	single: [SOURce<HW>]:BB:T2DVb:L:T2Baselite
	single: [SOURce<HW>]:BB:T2DVb:L:T2Version

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:L:CONStel
	[SOURce<HW>]:BB:T2DVb:L:EXTension
	[SOURce<HW>]:BB:T2DVb:L:REPetition
	[SOURce<HW>]:BB:T2DVb:L:SCRambled
	[SOURce<HW>]:BB:T2DVb:L:T2Baselite
	[SOURce<HW>]:BB:T2DVb:L:T2Version



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Lpy.LpyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.t2Dvb.lpy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_T2Dvb_Lpy_RfSignalling.rst