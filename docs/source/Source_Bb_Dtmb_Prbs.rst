Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DTMB:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: