InputPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP:INPut:TESTsignal

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP:INPut:TESTsignal



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.plp.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Plp_InputPy_DataRate.rst