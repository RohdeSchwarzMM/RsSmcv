Special
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS:[SPECial]:REEDsolomon

.. code-block:: python

	[SOURce<HW>]:BB:DVBS:[SPECial]:REEDsolomon



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs_Special_Setting.rst