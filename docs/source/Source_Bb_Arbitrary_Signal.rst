Signal
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:SIGNal:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:SIGNal:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Signal.SignalCls
	:members:
	:undoc-members:
	:noindex: