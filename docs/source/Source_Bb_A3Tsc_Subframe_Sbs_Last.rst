Last
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:SBS:LAST

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:SBS:LAST



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Sbs.Last.LastCls
	:members:
	:undoc-members:
	:noindex: