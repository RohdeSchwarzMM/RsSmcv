G10B
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G10B:BLOCk2
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G10B:BLOCk3
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:G10B:BLOCk4

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G10B:BLOCk2
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G10B:BLOCk3
	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:G10B:BLOCk4



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.G10B.G10BCls
	:members:
	:undoc-members:
	:noindex: