Setting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:[SPECial]:SETTing:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:[SPECial]:SETTing:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Special.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: