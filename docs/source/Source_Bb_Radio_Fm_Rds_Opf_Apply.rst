Apply
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:OPF:APPLy

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:OPF:APPLy



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Opf.Apply.ApplyCls
	:members:
	:undoc-members:
	:noindex: