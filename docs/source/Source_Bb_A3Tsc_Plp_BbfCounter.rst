BbfCounter
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:BBFCounter

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:BBFCounter



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.BbfCounter.BbfCounterCls
	:members:
	:undoc-members:
	:noindex: