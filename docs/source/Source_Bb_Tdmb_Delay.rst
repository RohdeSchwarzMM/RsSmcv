Delay
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:DELay:COMPensation
	single: [SOURce<HW>]:BB:TDMB:DELay:DELay
	single: [SOURce<HW>]:BB:TDMB:DELay:DEViation
	single: [SOURce<HW>]:BB:TDMB:DELay:NETWork
	single: [SOURce<HW>]:BB:TDMB:DELay:PROCess
	single: [SOURce<HW>]:BB:TDMB:DELay:STATic
	single: [SOURce<HW>]:BB:TDMB:DELay:TOTal

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:DELay:COMPensation
	[SOURce<HW>]:BB:TDMB:DELay:DELay
	[SOURce<HW>]:BB:TDMB:DELay:DEViation
	[SOURce<HW>]:BB:TDMB:DELay:NETWork
	[SOURce<HW>]:BB:TDMB:DELay:PROCess
	[SOURce<HW>]:BB:TDMB:DELay:STATic
	[SOURce<HW>]:BB:TDMB:DELay:TOTal



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: