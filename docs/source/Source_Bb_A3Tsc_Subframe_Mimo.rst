Mimo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:MIMO

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:MIMO



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex: