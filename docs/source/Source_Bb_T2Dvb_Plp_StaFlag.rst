StaFlag
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:STAFlag

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:STAFlag



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.StaFlag.StaFlagCls
	:members:
	:undoc-members:
	:noindex: