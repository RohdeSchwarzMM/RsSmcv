IsPy<InputStream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.bb.dvbs2.isPy.repcap_inputStream_get()
	driver.source.bb.dvbs2.isPy.repcap_inputStream_set(repcap.InputStream.Nr1)





.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.IsPy.IsPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.isPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_IsPy_PacketLength.rst
	Source_Bb_Dvbs2_IsPy_Stuffing.rst
	Source_Bb_Dvbs2_IsPy_TestSignal.rst
	Source_Bb_Dvbs2_IsPy_Useful.rst