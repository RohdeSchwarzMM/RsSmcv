Bband
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:AM:BBANd:SENSitivity
	single: [SOURce<HW>]:AM:BBANd:[STATe]

.. code-block:: python

	[SOURce<HW>]:AM:BBANd:SENSitivity
	[SOURce<HW>]:AM:BBANd:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Am.Bband.BbandCls
	:members:
	:undoc-members:
	:noindex: