Protection
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Protection.ProtectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.tdmb.protection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Tdmb_Protection_Level.rst
	Source_Bb_Tdmb_Protection_Profile.rst