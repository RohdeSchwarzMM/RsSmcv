Common
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:IQ:OUTPut:DIGital:SRATe:COMMon:STATe

.. code-block:: python

	[SOURce]:IQ:OUTPut:DIGital:SRATe:COMMon:STATe



.. autoclass:: RsSmcv.Implementations.Source.Iq.Output.Digital.SymbolRate.Common.CommonCls
	:members:
	:undoc-members:
	:noindex: