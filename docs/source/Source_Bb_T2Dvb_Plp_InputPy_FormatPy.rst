FormatPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:INPut:FORMat

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:INPut:FORMat



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.InputPy.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex: