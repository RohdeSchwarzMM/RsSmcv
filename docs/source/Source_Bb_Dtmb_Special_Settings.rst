Settings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DTMB:[SPECial]:SETTings:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:[SPECial]:SETTings:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.Special.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex: