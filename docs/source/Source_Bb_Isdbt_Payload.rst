Payload
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:ISDBt:PAYLoad:A

.. code-block:: python

	[SOURce<HW>]:BB:ISDBt:PAYLoad:A



.. autoclass:: RsSmcv.Implementations.Source.Bb.Isdbt.Payload.PayloadCls
	:members:
	:undoc-members:
	:noindex: