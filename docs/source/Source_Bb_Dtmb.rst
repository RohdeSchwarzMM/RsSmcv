Dtmb
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DTMB:CONStel
	single: [SOURce<HW>]:BB:DTMB:FRAMes
	single: [SOURce<HW>]:BB:DTMB:GIC
	single: [SOURce<HW>]:BB:DTMB:GUARd
	single: [SOURce<HW>]:BB:DTMB:PACKetlength
	single: [SOURce<HW>]:BB:DTMB:PAYLoad
	single: [SOURce<HW>]:BB:DTMB:PID
	single: [SOURce<HW>]:BB:DTMB:PIDTestpack
	single: [SOURce<HW>]:BB:DTMB:PRESet
	single: [SOURce<HW>]:BB:DTMB:RATE
	single: [SOURce<HW>]:BB:DTMB:SINGle
	single: [SOURce<HW>]:BB:DTMB:SOURce
	single: [SOURce<HW>]:BB:DTMB:STATe
	single: [SOURce<HW>]:BB:DTMB:STUFfing
	single: [SOURce<HW>]:BB:DTMB:TESTsignal
	single: [SOURce<HW>]:BB:DTMB:TSPacket

.. code-block:: python

	[SOURce<HW>]:BB:DTMB:CONStel
	[SOURce<HW>]:BB:DTMB:FRAMes
	[SOURce<HW>]:BB:DTMB:GIC
	[SOURce<HW>]:BB:DTMB:GUARd
	[SOURce<HW>]:BB:DTMB:PACKetlength
	[SOURce<HW>]:BB:DTMB:PAYLoad
	[SOURce<HW>]:BB:DTMB:PID
	[SOURce<HW>]:BB:DTMB:PIDTestpack
	[SOURce<HW>]:BB:DTMB:PRESet
	[SOURce<HW>]:BB:DTMB:RATE
	[SOURce<HW>]:BB:DTMB:SINGle
	[SOURce<HW>]:BB:DTMB:SOURce
	[SOURce<HW>]:BB:DTMB:STATe
	[SOURce<HW>]:BB:DTMB:STUFfing
	[SOURce<HW>]:BB:DTMB:TESTsignal
	[SOURce<HW>]:BB:DTMB:TSPacket



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dtmb.DtmbCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dtmb.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dtmb_Channel.rst
	Source_Bb_Dtmb_Dual.rst
	Source_Bb_Dtmb_InputPy.rst
	Source_Bb_Dtmb_Prbs.rst
	Source_Bb_Dtmb_Setting.rst
	Source_Bb_Dtmb_Special.rst
	Source_Bb_Dtmb_Time.rst
	Source_Bb_Dtmb_Useful.rst