Mode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:FFT:MODE

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:FFT:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Fft.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: