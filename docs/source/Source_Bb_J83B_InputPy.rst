InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:J83B:INPut:FORMat
	single: [SOURce<HW>]:BB:J83B:INPut:TSCHannel
	single: [SOURce<HW>]:BB:J83B:[INPut]:DATarate
	single: [SOURce<HW>]:BB:J83B:INPut

.. code-block:: python

	[SOURce<HW>]:BB:J83B:INPut:FORMat
	[SOURce<HW>]:BB:J83B:INPut:TSCHannel
	[SOURce<HW>]:BB:J83B:[INPut]:DATarate
	[SOURce<HW>]:BB:J83B:INPut



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: