Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ATSM:SETTing:CATalog
	single: [SOURce<HW>]:BB:ATSM:SETTing:DELete
	single: [SOURce<HW>]:BB:ATSM:SETTing:LOAD
	single: [SOURce<HW>]:BB:ATSM:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:ATSM:SETTing:CATalog
	[SOURce<HW>]:BB:ATSM:SETTing:DELete
	[SOURce<HW>]:BB:ATSM:SETTing:LOAD
	[SOURce<HW>]:BB:ATSM:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Atsm.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: