Channel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:CHANnel:[BANDwidth]

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:CHANnel:[BANDwidth]



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Channel.ChannelCls
	:members:
	:undoc-members:
	:noindex: