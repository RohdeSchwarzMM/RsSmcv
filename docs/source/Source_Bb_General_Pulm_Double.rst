Double
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:GENeral:PULM:DOUBle:DELay
	single: [SOURce<HW>]:BB:GENeral:PULM:DOUBle:WIDTh

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:PULM:DOUBle:DELay
	[SOURce<HW>]:BB:GENeral:PULM:DOUBle:WIDTh



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Pulm.Double.DoubleCls
	:members:
	:undoc-members:
	:noindex: