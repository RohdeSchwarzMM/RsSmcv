Generic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:GENeric:MSG

.. code-block:: python

	SYSTem:GENeric:MSG



.. autoclass:: RsSmcv.Implementations.System.Generic.GenericCls
	:members:
	:undoc-members:
	:noindex: