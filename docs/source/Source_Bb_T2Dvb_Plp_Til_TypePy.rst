TypePy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:TIL:TYPE

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:TIL:TYPE



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Til.TypePy.TypePyCls
	:members:
	:undoc-members:
	:noindex: