Hs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TEST:HS

.. code-block:: python

	TEST:HS



.. autoclass:: RsSmcv.Implementations.Test.Hs.HsCls
	:members:
	:undoc-members:
	:noindex: