Guard
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:GUARd:INTerval

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:GUARd:INTerval



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Guard.GuardCls
	:members:
	:undoc-members:
	:noindex: