Asetting
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BBIN:DIGital:ASETting:STATe

.. code-block:: python

	[SOURce<HW>]:BBIN:DIGital:ASETting:STATe



.. autoclass:: RsSmcv.Implementations.Source.Bbin.Digital.Asetting.AsettingCls
	:members:
	:undoc-members:
	:noindex: