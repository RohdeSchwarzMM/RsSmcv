BbfPadding
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:BBFPadding

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:BBFPadding



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.BbfPadding.BbfPaddingCls
	:members:
	:undoc-members:
	:noindex: