Source
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SOURce<HW>:PRESet

.. code-block:: python

	SOURce<HW>:PRESet



.. autoclass:: RsSmcv.Implementations.Source.SourceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Am.rst
	Source_Awgn.rst
	Source_Bb.rst
	Source_Bbin.rst
	Source_Correction.rst
	Source_Dm.rst
	Source_Fm.rst
	Source_Frequency.rst
	Source_InputPy.rst
	Source_Iq.rst
	Source_Iqcoder.rst
	Source_ListPy.rst
	Source_Modulation.rst
	Source_Noise.rst
	Source_Path.rst
	Source_Phase.rst
	Source_Pm.rst
	Source_Power.rst
	Source_Pulm.rst
	Source_Roscillator.rst
	Source_Sweep.rst