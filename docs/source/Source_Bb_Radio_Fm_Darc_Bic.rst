Bic<BlockIdCode>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.source.bb.radio.fm.darc.bic.repcap_blockIdCode_get()
	driver.source.bb.radio.fm.darc.bic.repcap_blockIdCode_set(repcap.BlockIdCode.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:DARC:BIC<CH>

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:DARC:BIC<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Darc.Bic.BicCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.darc.bic.clone()