Enums
=========

AcDc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AcDc.AC
	# All values (2x):
	AC | DC

All
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.All.NONE
	# All values (2x):
	NONE | TTSP

AmSourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AmSourceInt.LF1
	# All values (6x):
	LF1 | LF12 | LF1Noise | LF2 | LF2Noise | NOISe

AnalogDigital
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AnalogDigital.ANALog
	# All values (2x):
	ANALog | DIGital

ArbLevMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbLevMode.HIGHest
	# All values (2x):
	HIGHest | UNCHanged

ArbMultCarrCresMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbMultCarrCresMode.MAX
	# All values (3x):
	MAX | MIN | OFF

ArbMultCarrLevRef
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbMultCarrLevRef.PEAK
	# All values (2x):
	PEAK | RMS

ArbMultCarrSigDurMod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbMultCarrSigDurMod.LCM
	# All values (4x):
	LCM | LONG | SHORt | USER

ArbMultCarrSpacMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbMultCarrSpacMode.ARBitrary
	# All values (2x):
	ARBitrary | EQUidistant

ArbSegmNextSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbSegmNextSource.INTernal
	# All values (2x):
	INTernal | NSEGM1

ArbSignType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbSignType.AWGN
	# All values (4x):
	AWGN | CIQ | RECT | SINE

ArbTrigSegmModeNoEhop
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbTrigSegmModeNoEhop.NEXT
	# All values (4x):
	NEXT | NSEam | SAME | SEQuencer

ArbWaveSegmClocMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbWaveSegmClocMode.HIGHest
	# All values (3x):
	HIGHest | UNCHanged | USER

ArbWaveSegmMarkMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbWaveSegmMarkMode.IGNore
	# All values (2x):
	IGNore | TAKE

ArbWaveSegmPowMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbWaveSegmPowMode.ERMS
	# All values (2x):
	ERMS | UNCHanged

ArbWaveSegmRest
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ArbWaveSegmRest.MRK1
	# All values (5x):
	MRK1 | MRK2 | MRK3 | MRK4 | OFF

Atsc30Coderate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Atsc30Coderate.R10_15
	# Last value:
	value = enums.Atsc30Coderate.R9_15
	# All values (12x):
	R10_15 | R11_15 | R12_15 | R13_15 | R2_15 | R3_15 | R4_15 | R5_15
	R6_15 | R7_15 | R8_15 | R9_15

Atsc30Constellation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30Constellation.T1024
	# All values (6x):
	T1024 | T16 | T256 | T4 | T4096 | T64

Atsc30Depth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30Depth.D1024
	# All values (6x):
	D1024 | D1254 | D1448 | D512 | D724 | D887

Atsc30EmergencyAlertSignaling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30EmergencyAlertSignaling.NOEMergency
	# All values (4x):
	NOEMergency | SET1 | SET2 | SET3

Atsc30FecType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30FecType.B16K
	# All values (6x):
	B16K | B64K | C16K | C64K | O16K | O64K

Atsc30FftSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30FftSize.M16K
	# All values (3x):
	M16K | M32K | M8K

Atsc30FrameInfoBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30FrameInfoBandwidth.BW_6
	# All values (4x):
	BW_6 | BW_7 | BW_8 | BW8G

Atsc30FrameLengthMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30FrameLengthMode.SYMBol
	# All values (2x):
	SYMBol | TIME

Atsc30GuardInterval
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Atsc30GuardInterval.G1024
	# Last value:
	value = enums.Atsc30GuardInterval.G768
	# All values (12x):
	G1024 | G1536 | G192 | G2048 | G2432 | G3072 | G3648 | G384
	G4096 | G4864 | G512 | G768

Atsc30InputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30InputSignalTestSignal.TIPP
	# All values (2x):
	TIPP | TTSP

Atsc30InputType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30InputType.IP
	# All values (2x):
	IP | TS

Atsc30L1DetailAdditionalParityMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30L1DetailAdditionalParityMode.K1
	# All values (3x):
	K1 | K2 | OFF

Atsc30Layer
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30Layer.CORE
	# All values (2x):
	CORE | ENHanced

Atsc30LdmInjectionLayer
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Atsc30LdmInjectionLayer.L00
	# Last value:
	value = enums.Atsc30LdmInjectionLayer.L90
	# All values (31x):
	L00 | L05 | L10 | L100 | L110 | L120 | L130 | L140
	L15 | L150 | L160 | L170 | L180 | L190 | L20 | L200
	L210 | L220 | L230 | L240 | L25 | L250 | L30 | L35
	L40 | L45 | L50 | L60 | L70 | L80 | L90

Atsc30LowLevelSignaling
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30LowLevelSignaling.ABSent
	# All values (2x):
	ABSent | PRESent

Atsc30MinTimeToNext
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Atsc30MinTimeToNext.N100
	# Last value:
	value = enums.Atsc30MinTimeToNext.NOTapplicable
	# All values (33x):
	N100 | N1000 | N1100 | N1200 | N1400 | N150 | N1500 | N1600
	N1700 | N1900 | N200 | N2100 | N2300 | N250 | N2500 | N2700
	N2900 | N300 | N3300 | N350 | N3700 | N400 | N4100 | N4500
	N4900 | N50 | N500 | N5300 | N600 | N700 | N800 | N900
	NOTapplicable

Atsc30Miso
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30Miso.C256
	# All values (3x):
	C256 | C64 | OFF

Atsc30PilotPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30PilotPattern.D12
	# All values (8x):
	D12 | D16 | D24 | D3 | D32 | D4 | D6 | D8

Atsc30PilotPatternSiso
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Atsc30PilotPatternSiso.SP12_2
	# Last value:
	value = enums.Atsc30PilotPatternSiso.SP8_4
	# All values (16x):
	SP12_2 | SP12_4 | SP16_2 | SP16_4 | SP24_2 | SP24_4 | SP3_2 | SP3_4
	SP32_2 | SP32_4 | SP4_2 | SP4_4 | SP6_2 | SP6_4 | SP8_2 | SP8_4

Atsc30Protocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30Protocol.AUTO
	# All values (3x):
	AUTO | RTP | UDP

Atsc30TestIppAcket
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30TestIppAcket.HUDP
	# All values (1x):
	HUDP

Atsc30TimeInfo
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30TimeInfo.MSEC
	# All values (4x):
	MSEC | NSEC | OFF | USEC

Atsc30TimeInfoL1BasicFecType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30TimeInfoL1BasicFecType.MOD1
	# All values (7x):
	MOD1 | MOD2 | MOD3 | MOD4 | MOD5 | MOD6 | MOD7

Atsc30TimeInterleaverMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30TimeInterleaverMode.CTI
	# All values (3x):
	CTI | HTI | OFF

Atsc30TxIdInjectionLevel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Atsc30TxIdInjectionLevel.L120
	# Last value:
	value = enums.Atsc30TxIdInjectionLevel.OFF
	# All values (14x):
	L120 | L150 | L180 | L210 | L240 | L270 | L300 | L330
	L360 | L390 | L420 | L450 | L90 | OFF

Atsc30TxIdMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30TxIdMode.AUTo
	# All values (3x):
	AUTo | MANual | OFF

Atsc30Type
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Atsc30Type.DISPersed
	# All values (2x):
	DISPersed | NONDispersed

AtscmhBuryRatio
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AtscmhBuryRatio.DB21
	# All values (7x):
	DB21 | DB24 | DB27 | DB30 | DB33 | DB36 | DB39

AtscmhCodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AtscmhCodingConstel.VSB8
	# All values (1x):
	VSB8

AtscmhCodingInputSignalPacketLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AtscmhCodingInputSignalPacketLength.INValid
	# All values (3x):
	INValid | P188 | P208

AtscmhCodingRolloff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AtscmhCodingRolloff.R115
	# All values (1x):
	R115

AtscmhGeneralVsbFrequency
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AtscmhGeneralVsbFrequency.CENTer
	# All values (2x):
	CENTer | PILot

AtscmhInputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AtscmhInputSignalTestSignal.PBEM
	# All values (4x):
	PBEM | PBET | PBIN | TTSP

AudioBcFmDarcInformation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioBcFmDarcInformation.DATa
	# All values (3x):
	DATa | OFF | PRBS

AudioBcFmInputSignalAfMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioBcFmInputSignalAfMode.LEFT
	# All values (5x):
	LEFT | RELeft | REMLeft | RIGHt | RNELeft

AudioBcFmModulationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioBcFmModulationMode.MONO
	# All values (2x):
	MONO | STEReo

AudioBcFmModulationPreemphasis
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioBcFmModulationPreemphasis.D50us
	# All values (3x):
	D50us | D75us | OFF

AudioBcInputSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AudioBcInputSignal.AGENerator
	# All values (4x):
	AGENerator | APLayer | EXTernal | OFF

AutoManStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManStep.AUTO
	# All values (3x):
	AUTO | MANual | STEP

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

AutoStep
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoStep.AUTO
	# All values (2x):
	AUTO | STEP

AutoUser
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoUser.AUTO
	# All values (2x):
	AUTO | USER

BasebandModShape
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BasebandModShape.SINE
	# All values (1x):
	SINE

BasebandPulseMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BasebandPulseMode.DOUBle
	# All values (2x):
	DOUBle | SINGle

BasebandPulseTransType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BasebandPulseTransType.FAST
	# All values (2x):
	FAST | SMOothed

BbCodMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbCodMode.BBIN
	# All values (2x):
	BBIN | CODer

BbConfig
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbConfig.NORMal
	# All values (2x):
	NORMal | PACKet

BbDigInpBb
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BbDigInpBb.A
	# Last value:
	value = enums.BbDigInpBb.NONE
	# All values (9x):
	A | B | C | D | E | F | G | H
	NONE

BbImpOptMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbImpOptMode.FAST
	# All values (4x):
	FAST | QHIGh | QHTable | UCORrection

BbinInterfaceMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbinInterfaceMode.DIGital
	# All values (2x):
	DIGital | HSDin

BbinSampRateModeb
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BbinSampRateModeb.HSDin
	# All values (2x):
	HSDin | USER

BboutClocSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BboutClocSour.DIN
	# All values (3x):
	DIN | DOUT | USER

BcInputSignalSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BcInputSignalSource.SPDif
	# All values (1x):
	SPDif

BicmFecFrame
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BicmFecFrame.NORMal
	# All values (2x):
	NORMal | SHORt

ByteOrder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ByteOrder.NORMal
	# All values (2x):
	NORMal | SWAPped

CalDataMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataMode.CUSTomer
	# All values (2x):
	CUSTomer | FACTory

CalDataUpdate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalDataUpdate.BBFRC
	# All values (6x):
	BBFRC | FREQuency | IALL | LEVel | LEVForced | RFFRC

CalPowAttMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CalPowAttMode.NEW
	# All values (2x):
	NEW | OLD

CfrAlgo
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CfrAlgo.CLFiltering
	# All values (2x):
	CLFiltering | PCANcellation

CfrFiltMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CfrFiltMode.ENHanced
	# All values (2x):
	ENHanced | SIMPle

ClockModeUnits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClockModeUnits.MSAMple
	# All values (2x):
	MSAMple | SAMPle

ClockSourceB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClockSourceB.AINTernal
	# All values (3x):
	AINTernal | EXTernal | INTernal

ClocOutpMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClocOutpMode.BIT
	# All values (2x):
	BIT | SYMBol

ClocSourBb
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClocSourBb.AINTernal
	# All values (4x):
	AINTernal | COUPled | EXTernal | INTernal

ClocSyncModeSgt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ClocSyncModeSgt.DIIN
	# All values (4x):
	DIIN | NONE | PRIMary | SECondary

CodingChannelBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingChannelBandwidth.BW_6
	# All values (3x):
	BW_6 | BW_7 | BW_8

CodingCoderate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingCoderate.R1_2
	# All values (5x):
	R1_2 | R2_3 | R3_4 | R5_6 | R7_8

CodingGuardInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingGuardInterval.G1_16
	# All values (4x):
	G1_16 | G1_32 | G1_4 | G1_8

CodingInputFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputFormat.ASI
	# All values (2x):
	ASI | SMPTE

CodingInputSignalInputA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalInputA.ASI1
	# All values (8x):
	ASI1 | ASI2 | IP | SMP1 | SMP2 | TS | TS1 | TS2

CodingInputSignalInputAsi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalInputAsi.ASI1
	# All values (4x):
	ASI1 | ASI2 | ASIFront | ASIRear

CodingInputSignalInputB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalInputB.ASIFront
	# All values (6x):
	ASIFront | ASIRear | IP | SPIFront | SPIRear | TS

CodingInputSignalInputSfe
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalInputSfe.ASI1
	# All values (8x):
	ASI1 | ASI2 | ASIFront | ASIRear | SMP1 | SMP2 | SMPFront | SMPRear

CodingInputSignalPacketLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalPacketLength.INValid
	# All values (2x):
	INValid | P188

CodingInputSignalSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalSource.EXTernal
	# All values (3x):
	EXTernal | TESTsignal | TSPLayer

CodingInputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingInputSignalTestSignal.TTSP
	# All values (1x):
	TTSP

CodingIpType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingIpType.MULTicast
	# All values (2x):
	MULTicast | UNIcast

CodingIsdbtCodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingIsdbtCodingConstel.C_16QAM
	# All values (4x):
	C_16QAM | C_64QAM | C_DQPSK | C_QPSK

CodingIsdbtMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingIsdbtMode.M1_2K
	# All values (3x):
	M1_2K | M2_4K | M3_8K

CodingPacketLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingPacketLength.INV
	# All values (4x):
	INV | P188 | P204 | P208

CodingPortions
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingPortions.CCC
	# All values (7x):
	CCC | DCC | DDC | DDD | PCC | PDC | PDD

CodingTimeInterleaving
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingTimeInterleaving._0
	# All values (7x):
	_0 | _1 | _16 | _2 | _32 | _4 | _8

Colour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Colour.GREen
	# All values (4x):
	GREen | NONE | RED | YELLow

ConnDirection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnDirection.INPut
	# All values (3x):
	INPut | OUTPut | UNUSed

Count
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Count._1
	# All values (2x):
	_1 | _2

DabDataSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DabDataSour.ALL0
	# All values (5x):
	ALL0 | ALL1 | ETI | PN15 | PN23

DabTxMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DabTxMode.I
	# All values (4x):
	I | II | III | IV

DetAtt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DetAtt.HIGH
	# All values (5x):
	HIGH | LOW | MED | OFF | OVR

DevExpFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DevExpFormat.CGPRedefined
	# All values (4x):
	CGPRedefined | CGUSer | SCPI | XML

DexchExtension
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchExtension.CSV
	# All values (2x):
	CSV | TXT

DexchMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchMode.EXPort
	# All values (2x):
	EXPort | IMPort

DexchSepCol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchSepCol.COMMa
	# All values (4x):
	COMMa | SEMicolon | SPACe | TABulator

DexchSepDec
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DexchSepDec.COMMa
	# All values (2x):
	COMMa | DOT

DispKeybLockMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DispKeybLockMode.DISabled
	# All values (5x):
	DISabled | DONLy | ENABled | TOFF | VNConly

DmFilterA
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DmFilterA.APCO25
	# Last value:
	value = enums.DmFilterA.SPHase
	# All values (18x):
	APCO25 | C2K3x | COEQualizer | COF705 | COFequalizer | CONE | COSine | DIRac
	ENPShape | EWPShape | GAUSs | LGAuss | LPASs | LPASSEVM | PGAuss | RCOSine
	RECTangle | SPHase

DmTrigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DmTrigMode.AAUTo
	# All values (5x):
	AAUTo | ARETrigger | AUTO | RETRigger | SINGle

DpdPowRef
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DpdPowRef.ADPD
	# All values (3x):
	ADPD | BDPD | SDPD

DpdShapeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DpdShapeMode.NORMalized
	# All values (3x):
	NORMalized | POLYnomial | TABLe

DrmCodingChannelBw
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingChannelBw.INV
	# All values (8x):
	INV | K045 | K05 | K09 | K10 | K100 | K18 | K20

DrmCodingCoderate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DrmCodingCoderate.INV
	# Last value:
	value = enums.DrmCodingCoderate.R078
	# All values (17x):
	INV | R025 | R033 | R040 | R041 | R045 | R048 | R050
	R055 | R057 | R058 | R060 | R062 | R066 | R071 | R072
	R078

DrmCodingConstelMsc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingConstelMsc.INV
	# All values (6x):
	INV | Q16 | Q4 | Q64I | Q64N | Q64Q

DrmCodingConstelSdc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingConstelSdc.INV
	# All values (3x):
	INV | Q16 | Q4

DrmCodingInterleaver
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingInterleaver.INV
	# All values (4x):
	INV | MS4 | MS6 | S2

DrmCodingProtectionLevelMsc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingProtectionLevelMsc._0
	# All values (5x):
	_0 | _1 | _2 | _3 | INV

DrmCodingProtectionLevelSdc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingProtectionLevelSdc._0
	# All values (3x):
	_0 | _1 | INV

DrmCodingProtectionProfileMsc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingProtectionProfileMsc.HPP
	# All values (4x):
	HPP | INV | LPP | VSPP

DrmCodingProtectionProfileSdc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingProtectionProfileSdc.EEP
	# All values (2x):
	EEP | INV

DrmCodingRobustness
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmCodingRobustness.A
	# All values (6x):
	A | B | C | D | E | INV

DrmInputSignalLayerType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmInputSignalLayerType.BASE
	# All values (3x):
	BASE | ENHancement | INV

DrmInputSignalServices
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmInputSignalServices._0
	# All values (6x):
	_0 | _1 | _2 | _3 | _4 | INV

DrmInputSignalSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DrmInputSignalSource.EXTernal
	# All values (2x):
	EXTernal | FILE

DtmbCodingCoderate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtmbCodingCoderate.R04
	# All values (3x):
	R04 | R06 | R08

DtmbCodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtmbCodingConstel.D16
	# All values (5x):
	D16 | D32 | D4 | D4NR | D64

DtmbCodingGipN
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtmbCodingGipN.CONSt
	# All values (2x):
	CONSt | VAR

DtmbCodingGuardInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtmbCodingGuardInterval.G420
	# All values (3x):
	G420 | G595 | G945

DtmbCodingInputSignalPacketLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtmbCodingInputSignalPacketLength.INV
	# All values (2x):
	INV | P188

DtmbCodingTimeInterleaver
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DtmbCodingTimeInterleaver.I240
	# All values (3x):
	I240 | I720 | OFF

DvbcCodingDvbcCodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbcCodingDvbcCodingConstel.C128
	# All values (5x):
	C128 | C16 | C256 | C32 | C64

DvbcCodingDvbcCodingRolloff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbcCodingDvbcCodingRolloff._0_dot_13
	# All values (2x):
	_0_dot_13 | _0_dot_15

DvbcCodingDvbcInputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbcCodingDvbcInputSignalTestSignal.PBDE
	# All values (3x):
	PBDE | PBEM | TTSP

Dvbs2CodingCoderateSfe
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Dvbs2CodingCoderateSfe.R1_2
	# Last value:
	value = enums.Dvbs2CodingCoderateSfe.UNDef
	# All values (12x):
	R1_2 | R1_3 | R1_4 | R2_3 | R2_5 | R3_4 | R3_5 | R4_5
	R5_6 | R8_9 | R9_10 | UNDef

Dvbs2CodingConstelSfe
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbs2CodingConstelSfe.A16
	# All values (5x):
	A16 | A32 | S4 | S8 | UNDef

Dvbs2CodingFecFrame
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbs2CodingFecFrame.MEDium
	# All values (3x):
	MEDium | NORMal | SHORt

Dvbs2CodingModCod
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Dvbs2CodingModCod._0
	# Last value:
	value = enums.Dvbs2CodingModCod._99
	# All values (107x):
	_0 | _1 | _10 | _100 | _101 | _102 | _103 | _104
	_105 | _106 | _11 | _12 | _13 | _14 | _15 | _16
	_17 | _18 | _19 | _2 | _20 | _21 | _22 | _23
	_24 | _25 | _26 | _27 | _28 | _29 | _3 | _30
	_31 | _32 | _33 | _34 | _35 | _36 | _37 | _38
	_39 | _4 | _40 | _41 | _42 | _43 | _44 | _45
	_46 | _47 | _48 | _49 | _5 | _50 | _51 | _52
	_53 | _54 | _55 | _56 | _57 | _58 | _59 | _6
	_60 | _61 | _62 | _63 | _64 | _65 | _66 | _67
	_68 | _69 | _7 | _70 | _71 | _72 | _73 | _74
	_75 | _76 | _77 | _78 | _79 | _8 | _80 | _81
	_82 | _83 | _84 | _85 | _86 | _87 | _88 | _89
	_9 | _90 | _91 | _92 | _93 | _94 | _95 | _96
	_97 | _98 | _99

Dvbs2CodingRolloff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbs2CodingRolloff._0_dot_05
	# All values (6x):
	_0_dot_05 | _0_dot_10 | _0_dot_15 | _0_dot_20 | _0_dot_25 | _0_dot_35

Dvbs2InputSignalCmMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbs2InputSignalCmMode.ACM
	# All values (3x):
	ACM | CCM | VCM

Dvbs2InputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbs2InputSignalTestSignal.TGSP
	# All values (2x):
	TGSP | TTSP

DvbsCodingDvbsCodingCoderate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbsCodingDvbsCodingCoderate.R1_2
	# All values (6x):
	R1_2 | R2_3 | R3_4 | R5_6 | R7_8 | R8_9

DvbsCodingDvbsCodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbsCodingDvbsCodingConstel.S16
	# All values (3x):
	S16 | S4 | S8

DvbsCodingDvbsCodingRolloff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbsCodingDvbsCodingRolloff._0_dot_20
	# All values (3x):
	_0_dot_20 | _0_dot_25 | _0_dot_35

DvbsCodingDvbsInputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbsCodingDvbsInputSignalTestSignal.PBEC
	# All values (2x):
	PBEC | TTSP

Dvbt2BicmCoderate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2BicmCoderate.R1_2
	# All values (8x):
	R1_2 | R1_3 | R2_3 | R2_5 | R3_4 | R3_5 | R4_5 | R5_6

Dvbt2BicmConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2BicmConstel.T16
	# All values (4x):
	T16 | T256 | T4 | T64

Dvbt2FramingChannelBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2FramingChannelBandwidth.BW_2
	# All values (5x):
	BW_2 | BW_5 | BW_6 | BW_7 | BW_8

Dvbt2FramingFftSize
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Dvbt2FramingFftSize.M16E
	# Last value:
	value = enums.Dvbt2FramingFftSize.M8K
	# All values (9x):
	M16E | M16K | M1K | M2K | M32E | M32K | M4K | M8E
	M8K

Dvbt2FramingGuardInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2FramingGuardInterval.G1_16
	# All values (7x):
	G1_16 | G1_32 | G1_4 | G1_8 | G1128 | G19128 | G19256

Dvbt2FramingPilotPattern
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2FramingPilotPattern.PP1
	# All values (8x):
	PP1 | PP2 | PP3 | PP4 | PP5 | PP6 | PP7 | PP8

Dvbt2InputIssy
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2InputIssy.LONG
	# All values (3x):
	LONG | OFF | SHORt

Dvbt2InputSignalCm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2InputSignalCm.ACM
	# All values (2x):
	ACM | CCM

Dvbt2InputSignalMeasurementMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2InputSignalMeasurementMode.ABSOLUTE
	# All values (2x):
	ABSOLUTE | DELTA

Dvbt2ModeStreamAdapterPlpType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2ModeStreamAdapterPlpType.COMMon
	# All values (3x):
	COMMon | DT1 | DT2

Dvbt2PlpInputFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2PlpInputFormat.GCS
	# All values (4x):
	GCS | GFPS | GSE | TS

Dvbt2T2SystemFefPayload
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2T2SystemFefPayload.NOISe
	# All values (2x):
	NOISe | NULL

Dvbt2T2SystemL1PostModulation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2T2SystemL1PostModulation.T16
	# All values (4x):
	T16 | T2 | T4 | T64

Dvbt2T2SystemL1T2Version
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2T2SystemL1T2Version.V111
	# All values (3x):
	V111 | V121 | V131

Dvbt2T2SystemMisoGroupScpi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2T2SystemMisoGroupScpi.G1
	# All values (2x):
	G1 | G2

Dvbt2T2SystemProfileMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2T2SystemProfileMode.MULTI
	# All values (2x):
	MULTI | SINGLE

Dvbt2Transmission
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Dvbt2Transmission.MISO
	# All values (5x):
	MISO | NONT2 | SISO | T2LM | T2LS

DvbtCodingChannelBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbtCodingChannelBandwidth.BW_5
	# All values (5x):
	BW_5 | BW_6 | BW_7 | BW_8 | BW_Var

DvbtCodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbtCodingConstel.T16
	# All values (3x):
	T16 | T4 | T64

DvbtCodingDvbhSymbolInterleaver
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbtCodingDvbhSymbolInterleaver.INDepth
	# All values (2x):
	INDepth | NATive

DvbtCodingFftMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbtCodingFftMode.M2K
	# All values (3x):
	M2K | M4K | M8K

DvbtCodingGuardInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbtCodingGuardInterval.G1
	# All values (5x):
	G1 | G1_16 | G1_32 | G1_4 | G1_8

DvbtCodingHierarchy
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbtCodingHierarchy.A1
	# All values (4x):
	A1 | A2 | A4 | NONHier

DvbxCodingInputSignalPacketLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DvbxCodingInputSignalPacketLength.INValid
	# All values (3x):
	INValid | P188 | P204

EmulSgtBbSystemConfiguration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EmulSgtBbSystemConfiguration.AFETracking
	# All values (2x):
	AFETracking | STANdard

EmulSgtPowLevBehaviour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EmulSgtPowLevBehaviour.AUTO
	# All values (6x):
	AUTO | CVSWr | CWSWr | MONotone | UNINterrupted | USER

EmulSgtRefLoOutput
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EmulSgtRefLoOutput.LO
	# All values (3x):
	LO | OFF | REF

EmulSgtRoscOutputFreq
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EmulSgtRoscOutputFreq._1000MHZ
	# All values (4x):
	_1000MHZ | _100MHZ | _10MHZ | _13MHZ

EnetworkMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnetworkMode.MFN
	# All values (2x):
	MFN | SFN

ErFpowSensMapping
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.ErFpowSensMapping.SENS1
	# Last value:
	value = enums.ErFpowSensMapping.UNMapped
	# All values (9x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4
	UNMapped

FilterWidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilterWidth.NARRow
	# All values (2x):
	NARRow | WIDE

FormData
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormData.ASCii
	# All values (2x):
	ASCii | PACKed

FormStatReg
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FormStatReg.ASCii
	# All values (4x):
	ASCii | BINary | HEXadecimal | OCTal

FreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqMode.COMBined
	# All values (5x):
	COMBined | CW | FIXed | LIST | SWEep

FreqStepMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FreqStepMode.DECimal
	# All values (2x):
	DECimal | USER

HardCopyImageFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyImageFormat.BMP
	# All values (4x):
	BMP | JPG | PNG | XPM

HardCopyRegion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HardCopyRegion.ALL
	# All values (2x):
	ALL | DIALog

IecTermMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IecTermMode.EOI
	# All values (2x):
	EOI | STANdard

ImpG50G1KcoerceG10K
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ImpG50G1KcoerceG10K.G1K
	# All values (2x):
	G1K | G50

InclExcl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InclExcl.EXCLude
	# All values (2x):
	EXCLude | INCLude

InpOutpConnGlbMapSignb
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.InpOutpConnGlbMapSignb.CLOCK1
	# Last value:
	value = enums.InpOutpConnGlbMapSignb.TS
	# All values (11x):
	CLOCK1 | ETI | INSTtrigger | MARKA1 | NONE | NSEGM1 | PPS | SDIF
	SYNCIN | TRIG1 | TS

InputImpRf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputImpRf.G10K
	# All values (3x):
	G10K | G1K | G50

InputSignalPacketLength
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputSignalPacketLength.INValid
	# All values (4x):
	INValid | P188 | P204 | P208

InputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.InputSignalTestSignal.PAFC
	# All values (3x):
	PAFC | PBEC | TTSP

IqGain
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqGain.DB0
	# All values (7x):
	DB0 | DB2 | DB4 | DB6 | DB8 | DBM2 | DBM4

IqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqMode.ANALog
	# All values (2x):
	ANALog | BASeband

IqOutDispViaType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutDispViaType.LEVel
	# All values (2x):
	LEVel | PEP

IqOutEnvAdaption
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvAdaption.AUTO
	# All values (3x):
	AUTO | MANual | POWer

IqOutEnvDetrFunc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvDetrFunc.F1
	# All values (3x):
	F1 | F2 | F3

IqOutEnvEtRak
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvEtRak.ET1V2
	# All values (4x):
	ET1V2 | ET1V5 | ET2V0 | USER

IqOutEnvInterp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvInterp.LINear
	# All values (3x):
	LINear | OFF | POWer

IqOutEnvScale
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvScale.POWer
	# All values (2x):
	POWer | VOLTage

IqOutEnvShapeMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvShapeMode.DETRoughing
	# All values (6x):
	DETRoughing | LINear | OFF | POLYnomial | POWer | TABLe

IqOutEnvTerm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvTerm.GROund
	# All values (2x):
	GROund | WIRE

IqOutEnvVrEf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutEnvVrEf.VCC
	# All values (2x):
	VCC | VOUT

IqOutMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutMode.FIXed
	# All values (3x):
	FIXed | VARiable | VATTenuated

IqOutType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IqOutType.DIFFerential
	# All values (2x):
	DIFFerential | SINGle

IsdbtCodingSystem
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IsdbtCodingSystem.T
	# All values (3x):
	T | TSB1 | TSB3

IsdbtEewInfoType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IsdbtEewInfoType.CANCeled
	# All values (2x):
	CANCeled | ISSued

IsdbtEewSignalType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IsdbtEewSignalType.TWA
	# All values (4x):
	TWA | TWOA | WWA | WWOA

IsdbtSpecialTmcc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IsdbtSpecialTmcc.CURRent
	# All values (2x):
	CURRent | UNUSed

IsdbtSpecialTxParam
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.IsdbtSpecialTxParam.N1
	# Last value:
	value = enums.IsdbtSpecialTxParam.NORMal
	# All values (15x):
	N1 | N10 | N11 | N12 | N13 | N14 | N15 | N2
	N4 | N5 | N6 | N7 | N8 | N9 | NORMal

J83BcodingJ83BcodingConstel
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.J83BcodingJ83BcodingConstel.J1024
	# All values (3x):
	J1024 | J256 | J64

J83BcodingJ83BcodingRolloff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.J83BcodingJ83BcodingRolloff._0_dot_12
	# All values (2x):
	_0_dot_12 | _0_dot_18

J83BcodingJ83BinputSignalTestSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.J83BcodingJ83BinputSignalTestSignal.PBEM
	# All values (3x):
	PBEM | PBTR | TTSP

KbLayout
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.KbLayout.CHINese
	# Last value:
	value = enums.KbLayout.SWEDish
	# All values (20x):
	CHINese | DANish | DUTBe | DUTCh | ENGLish | ENGUK | ENGUS | FINNish
	FREBe | FRECa | FRENch | GERMan | ITALian | JAPanese | KORean | NORWegian
	PORTuguese | RUSSian | SPANish | SWEDish

LfFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LfFreqMode.CW
	# All values (3x):
	CW | FIXed | SWEep

LmodRunMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LmodRunMode.LEARned
	# All values (2x):
	LEARned | LIVE

LoRaBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.LoRaBw.BW10
	# Last value:
	value = enums.LoRaBw.BW7
	# All values (10x):
	BW10 | BW125 | BW15 | BW20 | BW250 | BW31 | BW41 | BW500
	BW62 | BW7

LoRaCodRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoRaCodRate.CR0
	# All values (5x):
	CR0 | CR1 | CR2 | CR3 | CR4

LoRaFreqDfTp
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoRaFreqDfTp.LINear
	# All values (2x):
	LINear | SINE

LoRaSf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoRaSf.SF10
	# All values (7x):
	SF10 | SF11 | SF12 | SF6 | SF7 | SF8 | SF9

LoRaSyncMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LoRaSyncMode.PRIVate
	# All values (2x):
	PRIVate | PUBLic

MappingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MappingType.A
	# All values (2x):
	A | B

MarkModeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MarkModeA.FRAMe
	# All values (6x):
	FRAMe | PATTern | PULSe | RATio | RESTart | TRIGger

MultInstMsMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MultInstMsMode.PRIMary
	# All values (2x):
	PRIMary | SECondary

NetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetMode.AUTO
	# All values (2x):
	AUTO | STATic

NetProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetProtocol.TCP
	# All values (2x):
	TCP | UDP

NetworkMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetworkMode.MFN
	# All values (1x):
	MFN

NoisAwgnDispMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoisAwgnDispMode.IQOUT1
	# All values (2x):
	IQOUT1 | RFA

NoisAwgnFseState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoisAwgnFseState.ADD
	# All values (3x):
	ADD | OFF | ONLY

NoisAwgnMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoisAwgnMode.ADD
	# All values (3x):
	ADD | CW | ONLY

NoisAwgnPowMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoisAwgnPowMode.CN
	# All values (3x):
	CN | EN | SN

NoisAwgnPowRefMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NoisAwgnPowRefMode.CARRier
	# All values (2x):
	CARRier | NOISe

NormalInverted
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NormalInverted.INVerted
	# All values (2x):
	INVerted | NORMal

NumberA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NumberA._1
	# All values (4x):
	_1 | _2 | _3 | _4

OutpConnGlbSignalb
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.OutpConnGlbSignalb.MARKA1
	# All values (2x):
	MARKA1 | NONE

ParameterSetMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ParameterSetMode.GLOBal
	# All values (2x):
	GLOBal | LIST

Parity
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Parity.EVEN
	# All values (3x):
	EVEN | NONE | ODD

PathUniCodBbin
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathUniCodBbin.A
	# All values (3x):
	A | AB | B

PathUniCodBbinA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PathUniCodBbinA.A
	# All values (1x):
	A

PayloadTestStuff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PayloadTestStuff.H00
	# All values (3x):
	H00 | HFF | PRBS

PidTestPacket
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PidTestPacket.NULL
	# All values (2x):
	NULL | VARiable

PixelTestPredefined
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PixelTestPredefined.AUTO
	# Last value:
	value = enums.PixelTestPredefined.WHITe
	# All values (9x):
	AUTO | BLACk | BLUE | GR25 | GR50 | GR75 | GREen | RED
	WHITe

PowAlcDetSensitivityEmulSgt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAlcDetSensitivityEmulSgt.AUTO
	# All values (6x):
	AUTO | FIXed | HIGH | LOW | MEDium | OFF

PowAlcStateEmulSgt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAlcStateEmulSgt._0
	# All values (7x):
	_0 | _1 | AUTO | OFF | OFFTable | ON | ONTable

PowAttRfOffMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowAttRfOffMode.FATTenuation
	# All values (2x):
	FATTenuation | UNCHanged

PowCntrlSelect
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowCntrlSelect.SENS1
	# All values (8x):
	SENS1 | SENS2 | SENS3 | SENS4 | SENSor1 | SENSor2 | SENSor3 | SENSor4

PowerAttMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerAttMode.AUTO
	# All values (5x):
	AUTO | FIXed | HPOWer | MANual | NORMal

PowLevBehaviour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowLevBehaviour.AUTO
	# All values (2x):
	AUTO | UNINterrupted

PowSensDisplayPriority
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensDisplayPriority.AVERage
	# All values (2x):
	AVERage | PEAK

PowSensFiltType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensFiltType.AUTO
	# All values (3x):
	AUTO | NSRatio | USER

PowSensSource
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowSensSource.A
	# All values (4x):
	A | B | RF | USER

PulseSoure
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulseSoure.CODer
	# All values (4x):
	CODer | EXTernal | INTernal | RANDom

PulsTrigMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulsTrigMode.AUTO
	# All values (3x):
	AUTO | EGATe | EXTernal

RecScpiCmdMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RecScpiCmdMode.AUTO
	# All values (4x):
	AUTO | DAUTo | MANual | OFF

RoscFreqExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscFreqExt._10MHZ
	# All values (3x):
	_10MHZ | _13MHZ | _5MHZ

RoscOutpFreqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscOutpFreqMode.DER10M
	# All values (3x):
	DER10M | LOOPthrough | OFF

RoscSourSetup
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RoscSourSetup.ELOop
	# All values (3x):
	ELOop | EXTernal | INTernal

Rs232BdRate
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Rs232BdRate._115200
	# All values (7x):
	_115200 | _19200 | _2400 | _38400 | _4800 | _57600 | _9600

SampRateFifoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SampRateFifoStatus.OFLow
	# All values (3x):
	OFLow | OK | URUN

SelftLev
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLev.CUSTomer
	# All values (3x):
	CUSTomer | PRODuction | SERVice

SelftLevWrite
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SelftLevWrite.CUSTomer
	# All values (4x):
	CUSTomer | NONE | PRODuction | SERVice

SensorModeAll
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SensorModeAll.AUTO
	# All values (3x):
	AUTO | EXTSingle | SINGle

SettingsPrbs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SettingsPrbs.P15_1
	# All values (2x):
	P15_1 | P23_1

SettingsTestTsPacket
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SettingsTestTsPacket.H184
	# All values (2x):
	H184 | S187

SfnMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SfnMode.ABSolute
	# All values (2x):
	ABSolute | RELative

SgtUserPlug
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SgtUserPlug.CIN
	# Last value:
	value = enums.SgtUserPlug.TRIGger
	# All values (18x):
	CIN | COUT | HIGH | LOW | MARRived | MKR1 | MKR2 | MLATency
	NEXT | PEMSource | PETRigger | PVOut | SIN | SNValid | SOUT | SVALid
	TOUT | TRIGger

SingExtAuto
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SingExtAuto.AUTO
	# All values (8x):
	AUTO | BUS | DHOP | EAUTo | EXTernal | HOP | IMMediate | SINGle

SlopeType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SlopeType.NEGative
	# All values (2x):
	NEGative | POSitive

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

Spacing
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Spacing.LINear
	# All values (3x):
	LINear | LOGarithmic | RAMP

SpecialAcData
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpecialAcData.ALL1
	# All values (2x):
	ALL1 | PRBS

StateExtended
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StateExtended._0
	# All values (6x):
	_0 | _1 | _2 | DEFault | OFF | ON

StateOn
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.StateOn._1
	# All values (2x):
	_1 | ON

SweCyclMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SweCyclMode.SAWTooth
	# All values (2x):
	SAWTooth | TRIangle

SystConfBbConf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SystConfBbConf.COUPled
	# All values (3x):
	COUPled | CPENtity | SEParate

SystConfHsChannels
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SystConfHsChannels.CH0
	# Last value:
	value = enums.SystConfHsChannels.CH8
	# All values (9x):
	CH0 | CH1 | CH2 | CH3 | CH4 | CH5 | CH6 | CH7
	CH8

SystConfOutpMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SystConfOutpMode.ALL
	# All values (6x):
	ALL | ANALog | DIGital | DIGMux | HSALl | HSDigital

SystemPostExtension
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SystemPostExtension.OFF
	# All values (1x):
	OFF

T2SystemPapr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.T2SystemPapr.OFF
	# All values (2x):
	OFF | TR

TdmaDataSource
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TdmaDataSource.DLISt
	# Last value:
	value = enums.TdmaDataSource.ZERO
	# All values (11x):
	DLISt | ONE | PATTern | PN11 | PN15 | PN16 | PN20 | PN21
	PN23 | PN9 | ZERO

TdmbInputSignalEtiSignal
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TdmbInputSignalEtiSignal.E537
	# All values (4x):
	E537 | E559 | ENI | INValid

TdmbInputSignalInputFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TdmbInputSignalInputFormat.ETI
	# All values (1x):
	ETI

TdmbInputSignalProtectionLevel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TdmbInputSignalProtectionLevel.EP1A
	# Last value:
	value = enums.TdmbInputSignalProtectionLevel.UP5
	# All values (14x):
	EP1A | EP1B | EP2A | EP2B | EP3A | EP3B | EP4A | EP4B
	UNDefined | UP1 | UP2 | UP3 | UP4 | UP5

TdmbInputSignalProtectionProfile
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TdmbInputSignalProtectionProfile.EEP
	# All values (2x):
	EEP | UEP

TdmbSettingsPrbs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TdmbSettingsPrbs.P15_1
	# All values (4x):
	P15_1 | P20_1 | P23_1 | ZERO

TdmbSpecialTransmissionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TdmbSpecialTransmissionMode.MANual
	# All values (2x):
	MANual | MID

Test
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Test._0
	# All values (4x):
	_0 | _1 | RUNning | STOPped

TestBbGenIqSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestBbGenIqSour.ARB
	# All values (4x):
	ARB | CONStant | SINE | TTONe

TestExtIqMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TestExtIqMode.IQIN
	# All values (2x):
	IQIN | IQOut

TimeProtocol
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TimeProtocol._0
	# All values (6x):
	_0 | _1 | NONE | NTP | OFF | ON

TranRecFftLen
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TranRecFftLen.LEN1024
	# All values (5x):
	LEN1024 | LEN2048 | LEN256 | LEN4096 | LEN512

TranRecMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TranRecMode.CCDF
	# All values (7x):
	CCDF | CONStellation | EYEI | EYEQ | IQ | PSPectrum | VECTor

TranRecSampFactMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TranRecSampFactMode.AUTO
	# All values (3x):
	AUTO | FULL | USER

TranRecSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TranRecSize.MAXimized
	# All values (2x):
	MAXimized | MINimized

TranRecSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TranRecSour.BBA
	# All values (6x):
	BBA | BBIA | DO1 | IQO1 | RFA | STRA

TranRecTrigSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TranRecTrigSour.MARKer
	# All values (2x):
	MARKer | SOFTware

TrigDelUnit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrigDelUnit.SAMPle
	# All values (2x):
	SAMPle | TIME

TriggerMarkModeA
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerMarkModeA.PATTern
	# All values (6x):
	PATTern | PULSe | RATio | RESTart | TRIGger | UNCHanged

TriggerSourceB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSourceB.BEXTernal
	# All values (4x):
	BEXTernal | EXTernal | INTernal | OBASeband

TrigRunMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrigRunMode.RUN
	# All values (2x):
	RUN | STOP

TrigSour
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrigSour.BBSY
	# All values (4x):
	BBSY | EGT1 | EXTernal | INTernal

TrigSweepSourNoHopExtAuto
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TrigSweepSourNoHopExtAuto.AUTO
	# All values (5x):
	AUTO | BUS | EXTernal | IMMediate | SINGle

TspLayerSettingsTestTsPacket
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TspLayerSettingsTestTsPacket.H184
	# All values (6x):
	H184 | H200 | H204 | S187 | S203 | S207

TspLayerStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TspLayerStatus.PAUSe
	# All values (4x):
	PAUSe | PLAY | RESet | STOP

TxAudioBcFmRdsAfBorder
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxAudioBcFmRdsAfBorder.ASC
	# All values (2x):
	ASC | DESC

TxAudioBcFmRdsEonAfMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxAudioBcFmRdsEonAfMethod.A
	# All values (2x):
	A | MAPF

TxAudioBcFmRdsMs
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TxAudioBcFmRdsMs.MUSic
	# All values (2x):
	MUSic | SPEech

UnchOff
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnchOff.OFF
	# All values (2x):
	OFF | UNCHanged

UnitAngle
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitAngle.DEGree
	# All values (3x):
	DEGree | DEGRee | RADian

UnitPower
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPower.DBM
	# All values (3x):
	DBM | DBUV | V

UnitPowSens
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitPowSens.DBM
	# All values (3x):
	DBM | DBUV | WATT

UnitSlB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitSlB.SAMPle
	# All values (2x):
	SAMPle | SEQuence

UnitSpeed
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UnitSpeed.KMH
	# All values (4x):
	KMH | MPH | MPS | NMPH

Unknown
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Unknown.DBM
	# All values (2x):
	DBM | V

UpdPolicyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.UpdPolicyMode.CONFirm
	# All values (3x):
	CONFirm | IGNore | STRict

