Min
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MIN:T1
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MIN:T2
	single: [SOURce<HW>]:BB:T2DVb:INPut:T2MI:MIN:T3

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MIN:T1
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MIN:T2
	[SOURce<HW>]:BB:T2DVb:INPut:T2MI:MIN:T3



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.InputPy.T2Mi.Min.MinCls
	:members:
	:undoc-members:
	:noindex: