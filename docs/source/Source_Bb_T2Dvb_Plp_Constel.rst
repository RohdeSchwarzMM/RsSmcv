Constel
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:CONStel

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:CONStel



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Constel.ConstelCls
	:members:
	:undoc-members:
	:noindex: