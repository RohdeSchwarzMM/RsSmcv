FormatPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:INPut:[IS<CH>]:FORMat

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:INPut:[IS<CH>]:FORMat



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.InputPy.IsPy.FormatPy.FormatPyCls
	:members:
	:undoc-members:
	:noindex: