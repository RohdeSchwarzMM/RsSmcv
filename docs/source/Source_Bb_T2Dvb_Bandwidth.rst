Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:BANDwidth:VARiation

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:BANDwidth:VARiation



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: