Setting
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:SETTing:CATalog
	single: [SOURce<HW>]:BB:DVBS2:SETTing:DELete
	single: [SOURce<HW>]:BB:DVBS2:SETTing:LOAD
	single: [SOURce<HW>]:BB:DVBS2:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:SETTing:CATalog
	[SOURce<HW>]:BB:DVBS2:SETTing:DELete
	[SOURce<HW>]:BB:DVBS2:SETTing:LOAD
	[SOURce<HW>]:BB:DVBS2:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.Setting.SettingCls
	:members:
	:undoc-members:
	:noindex: