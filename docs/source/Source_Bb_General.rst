General
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.General.GeneralCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.general.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_General_Am.rst
	Source_Bb_General_Fm.rst
	Source_Bb_General_Pm.rst
	Source_Bb_General_Pulm.rst