Preamble
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:PREamble:[STRucture]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:PREamble:[STRucture]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Preamble.PreambleCls
	:members:
	:undoc-members:
	:noindex: