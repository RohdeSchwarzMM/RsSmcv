Frequency<Index>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr64
	rc = driver.source.bb.radio.fm.rds.af.b.list3.frequency.repcap_index_get()
	driver.source.bb.radio.fm.rds.af.b.list3.frequency.repcap_index_set(repcap.Index.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST3:FREQuency<CH>

.. code-block:: python

	[SOURce<HW>]:BB:RADio:FM:RDS:AF:B:LIST3:FREQuency<CH>



.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Af.B.List3.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.af.b.list3.frequency.clone()