Ping
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:IGMP:[SOURce]:PING

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:IGMP:[SOURce]:PING



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Igmp.Source.Ping.PingCls
	:members:
	:undoc-members:
	:noindex: