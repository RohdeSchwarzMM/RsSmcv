Enable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:PROFiling:TICK:ENABle

.. code-block:: python

	SYSTem:PROFiling:TICK:ENABle



.. autoclass:: RsSmcv.Implementations.System.Profiling.Tick.Enable.EnableCls
	:members:
	:undoc-members:
	:noindex: