Duration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:DURation

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SUBFrame<CH>:DURation



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Subframe.Duration.DurationCls
	:members:
	:undoc-members:
	:noindex: