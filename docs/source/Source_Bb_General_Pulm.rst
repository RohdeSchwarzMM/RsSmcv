Pulm
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:GENeral:PULM:MODE
	single: [SOURce<HW>]:BB:GENeral:PULM:PERiod
	single: [SOURce<HW>]:BB:GENeral:PULM:WIDTh
	single: [SOURce<HW>]:BB:GENeral:PULM:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:GENeral:PULM:MODE
	[SOURce<HW>]:BB:GENeral:PULM:PERiod
	[SOURce<HW>]:BB:GENeral:PULM:WIDTh
	[SOURce<HW>]:BB:GENeral:PULM:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.General.Pulm.PulmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.general.pulm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_General_Pulm_Double.rst
	Source_Bb_General_Pulm_Transition.rst
	Source_Bb_General_Pulm_Video.rst