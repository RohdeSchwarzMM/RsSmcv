Frame
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:EXFinal
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:EXSYmbol
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:LENGth
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:MODE
	single: [SOURce<HW>]:BB:A3TSc:FRAMe:NSUBframes

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:FRAMe:EXFinal
	[SOURce<HW>]:BB:A3TSc:FRAMe:EXSYmbol
	[SOURce<HW>]:BB:A3TSc:FRAMe:LENGth
	[SOURce<HW>]:BB:A3TSc:FRAMe:MODE
	[SOURce<HW>]:BB:A3TSc:FRAMe:NSUBframes



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Frame.FrameCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.frame.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Frame_Additional.rst
	Source_Bb_A3Tsc_Frame_Time.rst