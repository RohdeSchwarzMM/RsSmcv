Delay
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:LORA:TRIGger:[EXTernal<CH>]:DELay

.. code-block:: python

	[SOURce<HW>]:BB:LORA:TRIGger:[EXTernal<CH>]:DELay



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Trigger.External.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex: