G3A
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Radio.Fm.Rds.Tmc.G3A.G3ACls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.radio.fm.rds.tmc.g3A.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Radio_Fm_Rds_Tmc_G3A_Var.rst