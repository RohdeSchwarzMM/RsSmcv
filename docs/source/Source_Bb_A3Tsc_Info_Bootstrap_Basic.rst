Basic
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:BASic:FECType

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:INFO:BOOTstrap:BASic:FECType



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Info.Bootstrap.Basic.BasicCls
	:members:
	:undoc-members:
	:noindex: