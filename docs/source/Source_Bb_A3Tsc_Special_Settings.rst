Settings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:SPECial:SETTings:[STATe]

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:SPECial:SETTings:[STATe]



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Special.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex: