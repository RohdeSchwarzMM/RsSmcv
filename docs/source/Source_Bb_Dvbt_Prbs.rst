Prbs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBT:PRBS:[SEQuence]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:PRBS:[SEQuence]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Prbs.PrbsCls
	:members:
	:undoc-members:
	:noindex: