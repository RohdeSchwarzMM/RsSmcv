Store
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:LORA:SETTing:STORe:FAST
	single: [SOURce<HW>]:BB:LORA:SETTing:STORe

.. code-block:: python

	[SOURce<HW>]:BB:LORA:SETTing:STORe:FAST
	[SOURce<HW>]:BB:LORA:SETTing:STORe



.. autoclass:: RsSmcv.Implementations.Source.Bb.Lora.Setting.Store.StoreCls
	:members:
	:undoc-members:
	:noindex: