Interleaver
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:J83B:INTerleaver:MODE

.. code-block:: python

	[SOURce<HW>]:BB:J83B:INTerleaver:MODE



.. autoclass:: RsSmcv.Implementations.Source.Bb.J83B.Interleaver.InterleaverCls
	:members:
	:undoc-members:
	:noindex: