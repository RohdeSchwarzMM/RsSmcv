Special
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.Special.SpecialCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbt.special.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbt_Special_ReedSolomon.rst
	Source_Bb_Dvbt_Special_Setting.rst