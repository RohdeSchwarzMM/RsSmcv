State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SYSTem:SECurity:NETWork:FTP:[STATe]

.. code-block:: python

	SYSTem:SECurity:NETWork:FTP:[STATe]



.. autoclass:: RsSmcv.Implementations.System.Security.Network.Ftp.State.StateCls
	:members:
	:undoc-members:
	:noindex: