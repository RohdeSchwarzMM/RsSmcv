Crotation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:CROTation

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:CROTation



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.Crotation.CrotationCls
	:members:
	:undoc-members:
	:noindex: