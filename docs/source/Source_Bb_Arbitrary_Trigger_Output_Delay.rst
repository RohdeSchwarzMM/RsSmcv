Delay
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:DELay
	single: [SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut:DELay:FIXed

.. code-block:: python

	[SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut<CH>:DELay
	[SOURce<HW>]:BB:ARBitrary:TRIGger:OUTPut:DELay:FIXed



.. autoclass:: RsSmcv.Implementations.Source.Bb.Arbitrary.Trigger.Output.Delay.DelayCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.arbitrary.trigger.output.delay.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Arbitrary_Trigger_Output_Delay_Maximum.rst
	Source_Bb_Arbitrary_Trigger_Output_Delay_Minimum.rst