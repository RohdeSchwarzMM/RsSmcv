IsPy<InputStream>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr8
	rc = driver.source.bb.dvbs2.inputPy.isPy.repcap_inputStream_get()
	driver.source.bb.dvbs2.inputPy.isPy.repcap_inputStream_set(repcap.InputStream.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:DVBS2:INPut:[IS<CH>]

.. code-block:: python

	[SOURce<HW>]:BB:DVBS2:INPut:[IS<CH>]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbs2.InputPy.IsPy.IsPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbs2.inputPy.isPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbs2_InputPy_IsPy_DataRate.rst
	Source_Bb_Dvbs2_InputPy_IsPy_FormatPy.rst
	Source_Bb_Dvbs2_InputPy_IsPy_TsChannel.rst