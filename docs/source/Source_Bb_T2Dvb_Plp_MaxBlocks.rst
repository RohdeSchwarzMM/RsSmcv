MaxBlocks
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:PLP<CH>:MAXBlocks

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:PLP<CH>:MAXBlocks



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Plp.MaxBlocks.MaxBlocksCls
	:members:
	:undoc-members:
	:noindex: