Address
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:INPut:IP<CH>:MULticast:ADDRess

.. code-block:: python

	[SOURce<HW>]:BB:INPut:IP<CH>:MULticast:ADDRess



.. autoclass:: RsSmcv.Implementations.Source.Bb.InputPy.Ip.Multicast.Address.AddressCls
	:members:
	:undoc-members:
	:noindex: