Tii
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:TDMB:TII:MAIN
	single: [SOURce<HW>]:BB:TDMB:TII:STATe
	single: [SOURce<HW>]:BB:TDMB:TII:SUB

.. code-block:: python

	[SOURce<HW>]:BB:TDMB:TII:MAIN
	[SOURce<HW>]:BB:TDMB:TII:STATe
	[SOURce<HW>]:BB:TDMB:TII:SUB



.. autoclass:: RsSmcv.Implementations.Source.Bb.Tdmb.Tii.TiiCls
	:members:
	:undoc-members:
	:noindex: