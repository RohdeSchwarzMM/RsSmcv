InputPy
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: [SOURce<HW>]:BB:DVBT:INPut:LOW
	single: [SOURce<HW>]:BB:DVBT:INPut:[HIGH]

.. code-block:: python

	[SOURce<HW>]:BB:DVBT:INPut:LOW
	[SOURce<HW>]:BB:DVBT:INPut:[HIGH]



.. autoclass:: RsSmcv.Implementations.Source.Bb.Dvbt.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.dvbt.inputPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_Dvbt_InputPy_DataRate.rst
	Source_Bb_Dvbt_InputPy_FormatPy.rst
	Source_Bb_Dvbt_InputPy_TsChannel.rst