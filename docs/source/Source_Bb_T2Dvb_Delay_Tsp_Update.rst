Update
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:T2DVb:DELay:TSP:UPDate

.. code-block:: python

	[SOURce<HW>]:BB:T2DVb:DELay:TSP:UPDate



.. autoclass:: RsSmcv.Implementations.Source.Bb.T2Dvb.Delay.Tsp.Update.UpdateCls
	:members:
	:undoc-members:
	:noindex: