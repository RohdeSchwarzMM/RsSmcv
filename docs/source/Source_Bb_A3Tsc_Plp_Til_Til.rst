Til
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:TIL

.. code-block:: python

	[SOURce<HW>]:BB:A3TSc:PLP<CH>:TIL:TIL



.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.Til.Til.TilCls
	:members:
	:undoc-members:
	:noindex: