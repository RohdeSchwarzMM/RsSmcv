Subslice
----------------------------------------





.. autoclass:: RsSmcv.Implementations.Source.Bb.A3Tsc.Plp.TypePy.Subslice.SubsliceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.source.bb.a3Tsc.plp.typePy.subslice.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Source_Bb_A3Tsc_Plp_TypePy_Subslice_Interval.rst