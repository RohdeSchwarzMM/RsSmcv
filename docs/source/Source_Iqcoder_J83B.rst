J83B
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: [SOURce]:[IQCoder]:J83B:INPut

.. code-block:: python

	[SOURce]:[IQCoder]:J83B:INPut



.. autoclass:: RsSmcv.Implementations.Source.Iqcoder.J83B.J83BCls
	:members:
	:undoc-members:
	:noindex: