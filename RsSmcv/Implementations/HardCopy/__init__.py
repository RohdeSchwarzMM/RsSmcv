from ...Internal.Core import Core
from ...Internal.CommandsGroup import CommandsGroup
from ...Internal import Conversions
from ... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class HardCopyCls:
	"""HardCopy commands group definition. 17 total commands, 4 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("hardCopy", core, parent)

	@property
	def device(self):
		"""device commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_device'):
			from .Device import DeviceCls
			self._device = DeviceCls(self._core, self._cmd_group)
		return self._device

	@property
	def file(self):
		"""file commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_file'):
			from .File import FileCls
			self._file = FileCls(self._core, self._cmd_group)
		return self._file

	@property
	def image(self):
		"""image commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_image'):
			from .Image import ImageCls
			self._image = ImageCls(self._core, self._cmd_group)
		return self._image

	@property
	def execute(self):
		"""execute commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_execute'):
			from .Execute import ExecuteCls
			self._execute = ExecuteCls(self._core, self._cmd_group)
		return self._execute

	def get_data(self) -> bytes:
		"""SCPI: HCOPy:DATA \n
		Snippet: value: bytes = driver.hardCopy.get_data() \n
		Transfers the hard copy data directly as a NByte stream to the remote client. \n
			:return: data: block data
		"""
		response = self._core.io.query_bin_block('HCOPy:DATA?')
		return response

	# noinspection PyTypeChecker
	def get_region(self) -> enums.HardCopyRegion:
		"""SCPI: HCOPy:REGion \n
		Snippet: value: enums.HardCopyRegion = driver.hardCopy.get_region() \n
		Selects the area to be copied. You can create a snapshot of the screen or an active dialog. \n
			:return: region: ALL| DIALog
		"""
		response = self._core.io.query_str('HCOPy:REGion?')
		return Conversions.str_to_scalar_enum(response, enums.HardCopyRegion)

	def set_region(self, region: enums.HardCopyRegion) -> None:
		"""SCPI: HCOPy:REGion \n
		Snippet: driver.hardCopy.set_region(region = enums.HardCopyRegion.ALL) \n
		Selects the area to be copied. You can create a snapshot of the screen or an active dialog. \n
			:param region: ALL| DIALog
		"""
		param = Conversions.enum_scalar_to_str(region, enums.HardCopyRegion)
		self._core.io.write(f'HCOPy:REGion {param}')

	def clone(self) -> 'HardCopyCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = HardCopyCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
