from ....Internal.Core import Core
from ....Internal.CommandsGroup import CommandsGroup
from ....Internal import Conversions
from .... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class AwgnCls:
	"""Awgn commands group definition. 21 total commands, 5 Subgroups, 5 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("awgn", core, parent)

	@property
	def bandwidth(self):
		"""bandwidth commands group. 1 Sub-classes, 3 commands."""
		if not hasattr(self, '_bandwidth'):
			from .Bandwidth import BandwidthCls
			self._bandwidth = BandwidthCls(self._core, self._cmd_group)
		return self._bandwidth

	@property
	def cmode(self):
		"""cmode commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_cmode'):
			from .Cmode import CmodeCls
			self._cmode = CmodeCls(self._core, self._cmd_group)
		return self._cmode

	@property
	def disp(self):
		"""disp commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_disp'):
			from .Disp import DispCls
			self._disp = DispCls(self._core, self._cmd_group)
		return self._disp

	@property
	def frequency(self):
		"""frequency commands group. 0 Sub-classes, 2 commands."""
		if not hasattr(self, '_frequency'):
			from .Frequency import FrequencyCls
			self._frequency = FrequencyCls(self._core, self._cmd_group)
		return self._frequency

	@property
	def power(self):
		"""power commands group. 2 Sub-classes, 3 commands."""
		if not hasattr(self, '_power'):
			from .Power import PowerCls
			self._power = PowerCls(self._core, self._cmd_group)
		return self._power

	def get_brate(self) -> float:
		"""SCPI: [SOURce<HW>]:AWGN:BRATe \n
		Snippet: value: float = driver.source.awgn.get_brate() \n
		Sets the bit rate used for calculation of bit energy to noise power ratio. Valid units are bps, kbps and mabps as well as
		b/s, kb/s and mab/s. \n
			:return: brate: float Range: 400 to depends on the installed options
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:AWGN:BRATe?')
		return Conversions.str_to_float(response)

	def set_brate(self, brate: float) -> None:
		"""SCPI: [SOURce<HW>]:AWGN:BRATe \n
		Snippet: driver.source.awgn.set_brate(brate = 1.0) \n
		Sets the bit rate used for calculation of bit energy to noise power ratio. Valid units are bps, kbps and mabps as well as
		b/s, kb/s and mab/s. \n
			:param brate: float Range: 400 to depends on the installed options
		"""
		param = Conversions.decimal_value_to_str(brate)
		self._core.io.write(f'SOURce<HwInstance>:AWGN:BRATe {param}')

	def get_cn_ratio(self) -> float:
		"""SCPI: [SOURce<HW>]:AWGN:CNRatio \n
		Snippet: value: float = driver.source.awgn.get_cn_ratio() \n
		Sets the carrier/interferer ratio. \n
			:return: cn_ratio: float Range: -50 to 45
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:AWGN:CNRatio?')
		return Conversions.str_to_float(response)

	def set_cn_ratio(self, cn_ratio: float) -> None:
		"""SCPI: [SOURce<HW>]:AWGN:CNRatio \n
		Snippet: driver.source.awgn.set_cn_ratio(cn_ratio = 1.0) \n
		Sets the carrier/interferer ratio. \n
			:param cn_ratio: float Range: -50 to 45
		"""
		param = Conversions.decimal_value_to_str(cn_ratio)
		self._core.io.write(f'SOURce<HwInstance>:AWGN:CNRatio {param}')

	def get_en_ratio(self) -> float:
		"""SCPI: [SOURce<HW>]:AWGN:ENRatio \n
		Snippet: value: float = driver.source.awgn.get_en_ratio() \n
		Sets the ratio of bit energy to noise power density. \n
			:return: en_ratio: float Range: -50 to depends on the installed options, Unit: dB
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:AWGN:ENRatio?')
		return Conversions.str_to_float(response)

	def set_en_ratio(self, en_ratio: float) -> None:
		"""SCPI: [SOURce<HW>]:AWGN:ENRatio \n
		Snippet: driver.source.awgn.set_en_ratio(en_ratio = 1.0) \n
		Sets the ratio of bit energy to noise power density. \n
			:param en_ratio: float Range: -50 to depends on the installed options, Unit: dB
		"""
		param = Conversions.decimal_value_to_str(en_ratio)
		self._core.io.write(f'SOURce<HwInstance>:AWGN:ENRatio {param}')

	# noinspection PyTypeChecker
	def get_mode(self) -> enums.NoisAwgnMode:
		"""SCPI: [SOURce<HW>]:AWGN:MODE \n
		Snippet: value: enums.NoisAwgnMode = driver.source.awgn.get_mode() \n
		Determines how the interfering signal is generated. \n
			:return: mode: ONLY| ADD| CW ADD The AWGN noise signal is added to the baseband signal. ONLY The pure AWGN noise signal is modulated to the carrier. The connection to the baseband is interrupted CW The sine interfering signal is added to the baseband signal.
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:AWGN:MODE?')
		return Conversions.str_to_scalar_enum(response, enums.NoisAwgnMode)

	def set_mode(self, mode: enums.NoisAwgnMode) -> None:
		"""SCPI: [SOURce<HW>]:AWGN:MODE \n
		Snippet: driver.source.awgn.set_mode(mode = enums.NoisAwgnMode.ADD) \n
		Determines how the interfering signal is generated. \n
			:param mode: ONLY| ADD| CW ADD The AWGN noise signal is added to the baseband signal. ONLY The pure AWGN noise signal is modulated to the carrier. The connection to the baseband is interrupted CW The sine interfering signal is added to the baseband signal.
		"""
		param = Conversions.enum_scalar_to_str(mode, enums.NoisAwgnMode)
		self._core.io.write(f'SOURce<HwInstance>:AWGN:MODE {param}')

	def get_state(self) -> bool:
		"""SCPI: [SOURce<HW>]:AWGN:STATe \n
		Snippet: value: bool = driver.source.awgn.get_state() \n
		Activates or deactivates the AWGN generator. \n
			:return: state: 1| ON| 0| OFF
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:AWGN:STATe?')
		return Conversions.str_to_bool(response)

	def set_state(self, state: bool) -> None:
		"""SCPI: [SOURce<HW>]:AWGN:STATe \n
		Snippet: driver.source.awgn.set_state(state = False) \n
		Activates or deactivates the AWGN generator. \n
			:param state: 1| ON| 0| OFF
		"""
		param = Conversions.bool_to_str(state)
		self._core.io.write(f'SOURce<HwInstance>:AWGN:STATe {param}')

	def clone(self) -> 'AwgnCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = AwgnCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
