from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ......Internal.Utilities import trim_str_response


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class SequenceCls:
	"""Sequence commands group definition. 1 total commands, 0 Subgroups, 1 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("sequence", core, parent)

	def get_select(self) -> str:
		"""SCPI: [SOURce<HW>]:BB:ARBitrary:WSEGment:SEQuence:SELect \n
		Snippet: value: str = driver.source.bb.arbitrary.wsegment.sequence.get_select() \n
		Selects the sequencing list (files with extension *.wvs) \n
			:return: filename: string
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:BB:ARBitrary:WSEGment:SEQuence:SELect?')
		return trim_str_response(response)

	def set_select(self, filename: str) -> None:
		"""SCPI: [SOURce<HW>]:BB:ARBitrary:WSEGment:SEQuence:SELect \n
		Snippet: driver.source.bb.arbitrary.wsegment.sequence.set_select(filename = 'abc') \n
		Selects the sequencing list (files with extension *.wvs) \n
			:param filename: string
		"""
		param = Conversions.value_to_quoted_str(filename)
		self._core.io.write(f'SOURce<HwInstance>:BB:ARBitrary:WSEGment:SEQuence:SELect {param}')
