from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions
from ....... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class TableCls:
	"""Table commands group definition. 10 total commands, 2 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("table", core, parent)

	@property
	def amam(self):
		"""amam commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_amam'):
			from .Amam import AmamCls
			self._amam = AmamCls(self._core, self._cmd_group)
		return self._amam

	@property
	def amPm(self):
		"""amPm commands group. 1 Sub-classes, 0 commands."""
		if not hasattr(self, '_amPm'):
			from .AmPm import AmPmCls
			self._amPm = AmPmCls(self._core, self._cmd_group)
		return self._amPm

	# noinspection PyTypeChecker
	def get_interp(self) -> enums.IqOutEnvInterp:
		"""SCPI: [SOURce<HW>]:IQ:DPD:SHAPing:TABLe:INTerp \n
		Snippet: value: enums.IqOutEnvInterp = driver.source.iq.dpd.shaping.table.get_interp() \n
		No command help available \n
			:return: ipart_interpolation: No help available
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:IQ:DPD:SHAPing:TABLe:INTerp?')
		return Conversions.str_to_scalar_enum(response, enums.IqOutEnvInterp)

	def set_interp(self, ipart_interpolation: enums.IqOutEnvInterp) -> None:
		"""SCPI: [SOURce<HW>]:IQ:DPD:SHAPing:TABLe:INTerp \n
		Snippet: driver.source.iq.dpd.shaping.table.set_interp(ipart_interpolation = enums.IqOutEnvInterp.LINear) \n
		No command help available \n
			:param ipart_interpolation: No help available
		"""
		param = Conversions.enum_scalar_to_str(ipart_interpolation, enums.IqOutEnvInterp)
		self._core.io.write(f'SOURce<HwInstance>:IQ:DPD:SHAPing:TABLe:INTerp {param}')

	def get_invert(self) -> bool:
		"""SCPI: [SOURce<HW>]:IQ:DPD:SHAPing:[TABLe]:INVert \n
		Snippet: value: bool = driver.source.iq.dpd.shaping.table.get_invert() \n
		No command help available \n
			:return: ipart_invert_values: No help available
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:IQ:DPD:SHAPing:TABLe:INVert?')
		return Conversions.str_to_bool(response)

	def set_invert(self, ipart_invert_values: bool) -> None:
		"""SCPI: [SOURce<HW>]:IQ:DPD:SHAPing:[TABLe]:INVert \n
		Snippet: driver.source.iq.dpd.shaping.table.set_invert(ipart_invert_values = False) \n
		No command help available \n
			:param ipart_invert_values: No help available
		"""
		param = Conversions.bool_to_str(ipart_invert_values)
		self._core.io.write(f'SOURce<HwInstance>:IQ:DPD:SHAPing:TABLe:INVert {param}')

	def clone(self) -> 'TableCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = TableCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
