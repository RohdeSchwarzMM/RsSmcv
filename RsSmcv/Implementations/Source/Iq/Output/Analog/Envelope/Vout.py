from .......Internal.Core import Core
from .......Internal.CommandsGroup import CommandsGroup
from .......Internal import Conversions


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class VoutCls:
	"""Vout commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("vout", core, parent)

	def get_max(self) -> float:
		"""SCPI: [SOURce<HW>]:IQ:OUTPut:[ANALog]:ENVelope:VOUT:MAX \n
		Snippet: value: float = driver.source.iq.output.analog.envelope.vout.get_max() \n
		No command help available \n
			:return: vout_max: No help available
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:IQ:OUTPut:ANALog:ENVelope:VOUT:MAX?')
		return Conversions.str_to_float(response)

	def set_max(self, vout_max: float) -> None:
		"""SCPI: [SOURce<HW>]:IQ:OUTPut:[ANALog]:ENVelope:VOUT:MAX \n
		Snippet: driver.source.iq.output.analog.envelope.vout.set_max(vout_max = 1.0) \n
		No command help available \n
			:param vout_max: No help available
		"""
		param = Conversions.decimal_value_to_str(vout_max)
		self._core.io.write(f'SOURce<HwInstance>:IQ:OUTPut:ANALog:ENVelope:VOUT:MAX {param}')

	def get_min(self) -> float:
		"""SCPI: [SOURce<HW>]:IQ:OUTPut:[ANALog]:ENVelope:VOUT:MIN \n
		Snippet: value: float = driver.source.iq.output.analog.envelope.vout.get_min() \n
		No command help available \n
			:return: vout_min: No help available
		"""
		response = self._core.io.query_str('SOURce<HwInstance>:IQ:OUTPut:ANALog:ENVelope:VOUT:MIN?')
		return Conversions.str_to_float(response)

	def set_min(self, vout_min: float) -> None:
		"""SCPI: [SOURce<HW>]:IQ:OUTPut:[ANALog]:ENVelope:VOUT:MIN \n
		Snippet: driver.source.iq.output.analog.envelope.vout.set_min(vout_min = 1.0) \n
		No command help available \n
			:param vout_min: No help available
		"""
		param = Conversions.decimal_value_to_str(vout_min)
		self._core.io.write(f'SOURce<HwInstance>:IQ:OUTPut:ANALog:ENVelope:VOUT:MIN {param}')
