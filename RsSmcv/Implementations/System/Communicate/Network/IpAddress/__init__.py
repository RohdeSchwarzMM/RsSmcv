from ......Internal.Core import Core
from ......Internal.CommandsGroup import CommandsGroup
from ......Internal import Conversions
from ......Internal.Utilities import trim_str_response
from ...... import enums


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class IpAddressCls:
	"""IpAddress commands group definition. 5 total commands, 1 Subgroups, 4 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("ipAddress", core, parent)

	@property
	def subnet(self):
		"""subnet commands group. 0 Sub-classes, 1 commands."""
		if not hasattr(self, '_subnet'):
			from .Subnet import SubnetCls
			self._subnet = SubnetCls(self._core, self._cmd_group)
		return self._subnet

	# noinspection PyTypeChecker
	def get_mode(self) -> enums.NetMode:
		"""SCPI: SYSTem:COMMunicate:NETWork:IPADdress:MODE \n
		Snippet: value: enums.NetMode = driver.system.communicate.network.ipAddress.get_mode() \n
		Selects manual or automatic setting of the IP address. \n
			:return: mode: AUTO| STATic
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:NETWork:IPADdress:MODE?')
		return Conversions.str_to_scalar_enum(response, enums.NetMode)

	def set_mode(self, mode: enums.NetMode) -> None:
		"""SCPI: SYSTem:COMMunicate:NETWork:IPADdress:MODE \n
		Snippet: driver.system.communicate.network.ipAddress.set_mode(mode = enums.NetMode.AUTO) \n
		Selects manual or automatic setting of the IP address. \n
			:param mode: AUTO| STATic
		"""
		param = Conversions.enum_scalar_to_str(mode, enums.NetMode)
		self._core.io.write(f'SYSTem:COMMunicate:NETWork:IPADdress:MODE {param}')

	def get_dns(self) -> str:
		"""SCPI: SYSTem:COMMunicate:NETWork:[IPADdress]:DNS \n
		Snippet: value: str = driver.system.communicate.network.ipAddress.get_dns() \n
		Determines or queries the network DNS server to resolve the name. \n
			:return: dns: string
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:NETWork:IPADdress:DNS?')
		return trim_str_response(response)

	def set_dns(self, dns: str) -> None:
		"""SCPI: SYSTem:COMMunicate:NETWork:[IPADdress]:DNS \n
		Snippet: driver.system.communicate.network.ipAddress.set_dns(dns = 'abc') \n
		Determines or queries the network DNS server to resolve the name. \n
			:param dns: string
		"""
		param = Conversions.value_to_quoted_str(dns)
		self._core.io.write(f'SYSTem:COMMunicate:NETWork:IPADdress:DNS {param}')

	def get_gateway(self) -> str:
		"""SCPI: SYSTem:COMMunicate:NETWork:[IPADdress]:GATeway \n
		Snippet: value: str = driver.system.communicate.network.ipAddress.get_gateway() \n
		Sets the IP address of the default gateway. \n
			:return: gateway: string Range: 0.0.0.0 to ff.ff.ff.ff
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:NETWork:IPADdress:GATeway?')
		return trim_str_response(response)

	def set_gateway(self, gateway: str) -> None:
		"""SCPI: SYSTem:COMMunicate:NETWork:[IPADdress]:GATeway \n
		Snippet: driver.system.communicate.network.ipAddress.set_gateway(gateway = 'abc') \n
		Sets the IP address of the default gateway. \n
			:param gateway: string Range: 0.0.0.0 to ff.ff.ff.ff
		"""
		param = Conversions.value_to_quoted_str(gateway)
		self._core.io.write(f'SYSTem:COMMunicate:NETWork:IPADdress:GATeway {param}')

	def get_value(self) -> str:
		"""SCPI: SYSTem:COMMunicate:NETWork:IPADdress \n
		Snippet: value: str = driver.system.communicate.network.ipAddress.get_value() \n
		Sets the IP address. \n
			:return: ip_address: string Range: 0.0.0.0. to ff.ff.ff.ff
		"""
		response = self._core.io.query_str('SYSTem:COMMunicate:NETWork:IPADdress?')
		return trim_str_response(response)

	def set_value(self, ip_address: str) -> None:
		"""SCPI: SYSTem:COMMunicate:NETWork:IPADdress \n
		Snippet: driver.system.communicate.network.ipAddress.set_value(ip_address = 'abc') \n
		Sets the IP address. \n
			:param ip_address: string Range: 0.0.0.0. to ff.ff.ff.ff
		"""
		param = Conversions.value_to_quoted_str(ip_address)
		self._core.io.write(f'SYSTem:COMMunicate:NETWork:IPADdress {param}')

	def clone(self) -> 'IpAddressCls':
		"""Clones the group by creating new object from it and its whole existing subgroups
		Also copies all the existing default Repeated Capabilities setting,
		which you can change independently without affecting the original group"""
		new_group = IpAddressCls(self._core, self._cmd_group.parent)
		self._cmd_group.synchronize_repcaps(new_group)
		return new_group
